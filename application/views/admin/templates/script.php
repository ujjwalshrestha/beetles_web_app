<script>'use strict';</script>


<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/jquery-ui/jquery-ui.min.js') ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
<!-- ChartJS -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/chart.js/Chart.min.js') ?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/sparklines/sparkline.js') ?>"></script>
<!-- JQVMap -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/jqvmap/jquery.vmap.min.js') ?>"></script>
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/jqvmap/maps/jquery.vmap.world.js') ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/jquery-knob/jquery.knob.min.js') ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/moment/moment.min.js') ?>"></script>
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/daterangepicker/daterangepicker.js') ?>"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') ?>"></script>
<!-- Summernote -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/summernote/summernote-bs4.min.js') ?>"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/fastclick/fastclick.js') ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(ADMIN_ASSETS.'dist/js/adminlte.js') ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url(ADMIN_ASSETS.'dist/js/pages/dashboard.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(ADMIN_ASSETS.'dist/js/demo.js') ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/select2/js/select2.full.min.js') ?>"></script>


<script>

    $(document).ready(function() {
        // Alert message div will disappear itself after probably 8 sec.
        $(".alert-success").fadeTo(8000, 800).slideUp(800, function(){
            $(".alert-success").slideUp(800);
        });

        // Alert message div will disappear itself after probably 8 sec.
        $(".alert-danger").fadeTo(8000, 800).slideUp(800, function(){
            $(".alert-danger").slideUp(800);
        });

        // Alert message div will disappear itself after probably 8 sec.
        $(".alert-info").fadeTo(8000, 800).slideUp(800, function(){
            $(".alert-info").slideUp(800);
        });

        // Alert message div will disappear itself after probably 8 sec.
        $(".alert-warning").fadeTo(8000, 800).slideUp(800, function(){
            $(".alert-warning").slideUp(800);
        });

        // Boostrap tooltip
        $('[data-toggle="tooltip"]').tooltip();

        // DataTable
        $('#data_table_active').DataTable({
            "responsive" : true,
            "lengthMenu": [ [10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "All"] ]
        });
        
        //Initialize Select2 Elements
        $('.select2').select2()
    });


    // For generating slug automatically. Here, the slug with be generated on the field named 'slugged' from the field named 'name' and 'title'.
    $(document).off('keyup', 'input[name="name"], input[name="title"]').on('keyup', 'input[name="name"], input[name="title"]', function(e){
        e.preventDefault();
        var obj = $(this);
        var str = obj.val();
        var trims = $.trim(str);
        var slug = trims.replace(/[^a-z0-9]/gi, '-').replace(/^-|-$/g, '');
        $('input[name="slug"]').val(slug.toLowerCase())
    });

    // Hides validation error messages
    $.fn.hideError = function(obj) {
        obj.find('.form-msg').html('').hide();
    }

    // Displays validation error messages
    $.fn.displayError = function(elemId, msg) {
        $("#"+elemId).siblings('.form-msg').html(msg).show();
    }

    // realtime date time
    tday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    tmonth = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    function GetClock(){
        let d = new Date();
        let nday = d.getDay(), 
            nmonth = d.getMonth(), 
            ndate = d.getDate(), 
            nyear = d.getYear(), 
            nhour = d.getHours(), 
            nmin = d.getMinutes(), 
            nsec = d.getSeconds(), 
            ap;

        if (nhour == 0) {
            ap = " AM";
            nhour = 12;
        } else if (nhour < 12) {
            ap = " AM";
        } else if (nhour >= 12) {
            ap = " PM";
            nhour-=12;
        }

        if (nyear < 1000) nyear += 1900;
        if (nmin <= 9) nmin = "0" + nmin;
        if (nsec <= 9) nsec = "0" + nsec;

        document.getElementById('realtime_date_time').innerHTML = "" + tday[nday] + ", " + tmonth[nmonth] + " " + ndate + ", " + nyear + " " + nhour + ":" + nmin + ":" + nsec + ap + "";
    }

    window.onload = function() {
        GetClock();
        setInterval(GetClock,1000);
    }
    // realtime date time ends here


    /** add active class and stay opened when selected */
    var url = window.location;

    // for sidebar menu entirely but not cover treeview
    $('ul.nav-sidebar a').filter(function() {
        return this.href == url;
    }).addClass('active');

    // for treeview
    $('ul.nav-treeview a').filter(function() {
        return this.href == url;
    }).parentsUntil(".nav-sidebar > .nav-treeview").addClass('menu-open').prev('a').addClass('active');
    /** // add active class and stay opened when selected ends here */

    // prevents form from submitting when the enter is hitted, while the cursor is on input fields  
    $(document).on("keypress", "input", function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            return false;
        }
    });

</script>        