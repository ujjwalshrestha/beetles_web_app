<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Logout icon -->
        <li class="nav-item">
            <a href="<?php echo base_url('auth/logout') ?>" class="nav-link" title="Logout">
                <i class="fas fa-sign-out-alt"></i>
            </a>
        </li>

        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fas fa-user"></i>&nbsp; <?php echo $username ?>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <!-- check if it is customer's group_id i.e. 8 or not  -->
                <?php if (session_data('group_id') == 8): ?>
                    <a href="<?php echo base_url('customer/profile') ?>" class="dropdown-item">
                        <i class="fas fa-user mr-2"></i> User Profile
                    </a>
                <?php else: ?>
                    <a href="<?php echo base_url('auth/userProfile') ?>" class="dropdown-item">
                        <i class="fas fa-user mr-2"></i> User Profile
                    </a>
                <?php endif; ?>
                <div class="dropdown-divider"></div>

                <a href="<?php echo base_url('auth/changePassword') ?>" class="dropdown-item">
                    <i class="fas fa-lock mr-2"></i> Change Password
                </a>
                <div class="dropdown-divider"></div>
            </div>
        </li>
    </ul>
</nav>
<!-- /.navbar -->