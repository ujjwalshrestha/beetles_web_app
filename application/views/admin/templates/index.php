<!-- top part of file: linked files for css, js, etc. -->
<?php $this->load->view('admin/templates/header'); ?>

<!-- top navbar -->
<?php $this->load->view('admin/templates/navbar'); ?>

<!-- menu navigation sidebar -->
<?php $this->load->view('admin/templates/sidebar'); ?>

<!-- top breadcrumb section (part of main content section) -->
<?php $this->load->view('admin/templates/breadcrumb'); ?>

<!-- main content section -->
<?php $this->load->view($page); ?> 

<!-- footer section -->
<?php $this->load->view('admin/templates/footer'); ?>

<!-- script section -->
<?php $this->load->view('admin/templates/script'); ?>