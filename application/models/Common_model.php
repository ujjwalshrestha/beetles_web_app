<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * A common universal class in which functions are applicable and common in all the modules.
 */
class Common_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

    public function getData($tableName)
	{
		$where = array(
			'delete_status' => 0
		);

		$result = $this->db->get_where($tableName, $where);

		if ($result->num_rows() > 0) {
			return $result->result();
		} else {
			return false;
		}
	}

	public function getDataById($id, $tableName)
	{
		$where = array(
			'id' => $id,
			'delete_status' => 0
		);

		$result = $this->db->get_where($tableName, $where);

		if ($result->num_rows() == 1) {
			return $result->row();
		} else {
			return false;
		}
	}

	public function insert($insertData, $tableName)
	{
		return $this->db->insert($tableName, $insertData);
	}

	public function update($id, $updateData, $tableName)
	{
		$where = array(
			'id' => $id
		);

		$this->db->where($where);
		return $this->db->update($tableName, $updateData);
	}

	public function softDelete($id, $tableName)
	{
		$softDeleteData = array(
			'delete_status' => 1,
			'deleted_by' => session_data('user_id'),
			'deleted_at' => date('Y-m-d H:i:s'),
		);

		$where = array(
			'id' => $id
		);
		
		$this->db->where($where);
		return $this->db->update($tableName, $softDeleteData);
	}
}