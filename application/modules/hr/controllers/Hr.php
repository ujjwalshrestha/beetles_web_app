<?php 

if( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * All the functionality related CMS, User Management and other backend functions are implemented in this class named 'Admin'.
 */
class Hr extends MX_Controller
{
    /*
	 *	variable to hold data for view
	 */
	private static $viewData = array();
	
	public function __construct()
	{
		parent::__construct();

		// To show the username in admin dashboard of the logged in user
		if (isLoggedin()) { // uri_segment(2) && uri_segment(2) != 'login'
			self::$viewData['username'] = $this->auth_model->getAuthUser()->username;
		}

		// Dashboard side menu
		self::$viewData['dashboardSideMenu'] = $this->auth_model->getDashboardSideMenu();

		// Checks if the auth active status, auth temp_status and user group login_status is 0 or not. If one of them is 0, then user is forced to logout of the system.
		if (isLoggedin() && (($this->auth_model->getAuthUser()->active_status == 0) || ($this->auth_model->getAuthUser()->temp_status == 0) || ($this->auth_model->getUserGroupById(session_data('group_id'))->login_status == 0)) ) 
		{
			$this->logout(); // force logout
		}
	}

	public function index()
	{
		show_404();
	}

	/**
	 * List of daily attendance.
	 * Attendance list is shown of current date only.
	 *
	 * @return void
	 */
	public function dailyAttendance()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Daily Attendance";
		self::$viewData['breadcrumb'] = "Daily Attendance"; 
		self::$viewData['heading'] = "Daily Attendance List";
		// Getting all Daily Attendance one list
		self::$viewData['attendanceList'] = $this->hr_model->getTodaysAttendance();
		// Check Daily Attendance (todays)
		self::$viewData['checkAttendance'] = $this->hr_model->checkTodayAttendance();
		self::$viewData['page'] = "hr/attendance/daily_attendance_list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * Daily attendance form.
	 * If user has already registered their attendance, the form wont display.
	 *
	 * @return void
	 */
	public function dailyAttendanceForm()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}
		
		// Check Daily Attendance by user_id session (todays)
		$checkAttendance = $this->hr_model->checkTodayAttendance();
		if ($checkAttendance) {
			redirect('hr/dailyAttendance');
			exit;
		}

		self::$viewData['title'] = "Add Daily Attendance";
		self::$viewData['breadcrumb'] = "Add Daily Attendance"; 
		self::$viewData['heading'] = "Daily Attendance Form"; 
		// Getting all attendance status list (for select field)
		self::$viewData['attendanceStatus'] = $this->common_model->getData($tableName="attendance_status");
		// Getting attendance status list by time limit (for select field)
		self::$viewData['attendanceStatusByLimit'] = $this->hr_model->getAttendanceStatusByLimit();
		// Getting current logged in user detail
		self::$viewData['userDetail'] = $this->auth_model->getAuthUserDetail();
		self::$viewData['page'] = "hr/attendance/daily_attendance_form";

		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * This function is invoked through ajax request.
	 * First, form validation is carried out, followed by inserting of data into db table.
	 *
	 * @return void
	 */
	public function ajaxAddDailyAttendance()
	{
		try {
			if ($this->input->is_ajax_request()) {
                $csrf_name = $this->security->get_csrf_token_name(); // get csrf name
                $csrf_value = $this->security->get_csrf_hash(); // get new csrf value

				//getting all the post data from form
				$postData = $this->input->post();

				$this->form_validation->set_rules('attendance_status', ' ', 'required|trim');
				if ($postData['attendance_status'] == "2" || $postData['attendance_status'] == "3" ) {
					$this->form_validation->set_rules('reason', ' ', 'required|trim');
				}

				// validation check 
				if ($this->form_validation->run() == false) {
    				$formErrors = array();

					foreach ($_POST as $key => $value) {
						$errMsg = form_error($key);
						if(!empty($errMsg)) {
							$formErrors[] = array(
								'id' => $key,
								'message' => $errMsg
							);
						}
					}

					$response = array (
						'status' => 'error',
						'data' => $formErrors,
						'message' => 'form_error',
                        "csrf_name" => $csrf_name,
                        "csrf_value" => $csrf_value
					); 

					header("Content-type: application/json");
					echo json_encode($response);exit;

    			} else {
					$insertData = array(
						'user_id' => session_data("user_id"),
						'attendance_status_id' => $postData['attendance_status'],
						'reason' => ($postData['reason']) ? $postData['reason'] : null
					);

					$insert = $this->common_model->insert($insertData, $tableName="attendance");
					if ($insert) {
						$response = array(
							'status' => 'success',
							'message' => 'Your daily attendance has been registered.',
							'url' => base_url('hr/dailyAttendance'),
							"csrf_name" => $csrf_name,
							"csrf_value" => $csrf_value
						);
						$message = 'Your daily attendance has been registered.';
						$this->session->set_flashdata('success_msg', $message);

					} else {
						$response = array(
							'status' => 'error',
							'message' => 'insert_error',
							'url' => base_url('hr/dailyAttendanceForm'),
							"csrf_name" => $csrf_name,
							"csrf_value" => $csrf_value
						);
						$message = 'Failed to register daily attendance.';
						$this->session->set_flashdata('error_msg', $message);
					}
				}
				header("Content-type: application/json");
				echo json_encode($response);

			} else {
				exit('No direct script access allowed');
			}
		} catch (Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}	
	}

	/**
	 * Attendance report is generated within this function, where report view page is loaded.
	 *
	 * @return void
	 */
	public function attendanceReport()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Attendance Report";
		self::$viewData['breadcrumb'] = "Attendance Report"; 
		self::$viewData['heading'] = "Attendance Report"; 
		// Getting attendance status list (for select field)
		self::$viewData['attendanceStatus'] = $this->common_model->getData($tableName="attendance_status");
		// Getting current logged in user detail
		self::$viewData['userList'] = $this->auth_model->getUsersList();
		self::$viewData['page'] = "hr/attendance/attendance_report";

		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * This function is invoked through ajax request.
	 * Attendance report is generated, according to sorting data fetched through filtering parameters.
	 *
	 * @return void
	 */
	public function ajaxGetAttendanceReport()
	{
		try {
			if ($this->input->is_ajax_request()) {
                $csrf_name = $this->security->get_csrf_token_name(); // get csrf name
                $csrf_value = $this->security->get_csrf_hash(); // get new csrf value

				$this->form_validation->set_rules('user', ' ', 'required|trim');
				$this->form_validation->set_rules('date_from', ' ', 'required|trim');
				$this->form_validation->set_rules('date_to', ' ', 'required|trim');

				// validation check 
				if ($this->form_validation->run() == false) {
    				$formErrors = array();

					foreach ($_POST as $key => $value) {
						$errMsg = form_error($key);
						if(!empty($errMsg)) {
							$formErrors[] = array(
								'id' => $key,
								'message' => $errMsg
							);
						}
					}

					$response = array (
						'status' => 'error',
						'data' => $formErrors,
						'error_type' => 'form_error',
                        "csrf_name" => $csrf_name,
                        "csrf_value" => $csrf_value
					); 

					header("Content-type: application/json");
					echo json_encode($response);exit;
					
    			} else {
					$postData = $this->input->post();
					if ($postData['date_from'] > $postData['date_to']) {
						$response = array (
							'status' => 'error',
							'message' => 'Date From should not be greater than Date To.',
							'error_type' => 'date_error',
							"csrf_name" => $csrf_name,
							"csrf_value" => $csrf_value
						); 
					}

					self::$viewData['attendanceReport'] = $attendanceReport = $this->hr_model->getAttendanceReport($postData['user'], $postData['date_from'], $postData['date_to']);

					$viewPage = $this->load->view('hr/attendance/ajax_view/attendance_report_table', self::$viewData, TRUE);
					
					$response = array(
						'status' => 'success',
						'message' => 'Attendance Report Table',
						'data' => $viewPage,
						"csrf_name" => $csrf_name,
						"csrf_value" => $csrf_value
					);
				}
				header("Content-type: application/json");
				echo json_encode($response);exit;

			} else {
				exit('No direct script access allowed');
			}
		} catch (Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}	
	}

	/**
	 * All the list of user bank detail is displayed.
	 * Management of user bank detail is done in this page.
	 *
	 * @return void
	 */
	public function userBankDetail()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 23;
		self::$viewData['title'] = "User Bank Detail";
		self::$viewData['breadcrumb'] = "User Bank Detail"; 
		self::$viewData['heading'] = "User Bank Detail List";
		self::$viewData['bankDetailList'] = $this->hr_model->getUserBankDetailList(); // join query
		self::$viewData['page'] = "hr/user_bank_detail/list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * Form for adding user bank detail is loaded.
	 * Validation and insert functionality is also carried out within this function.
	 *
	 * @return void
	 */
	public function addUserBankDetail()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Add User Bank Detail";
		self::$viewData['breadcrumb'] = "Add User Bank Detail"; 
		self::$viewData['heading'] = "User Bank Detail Form"; 
		// Getting list of user
		self::$viewData['userList'] = $this->auth_model->getUsersList();
		self::$viewData['page'] = "hr/user_bank_detail/add";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('user', ' ', 'required|trim');
			$this->form_validation->set_rules('bank_name', ' ', 'required|trim');
			$this->form_validation->set_rules('account_name', ' ', 'required|trim');
			$this->form_validation->set_rules('account_number', ' ', 'required|trim');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);
			} else {
				$post_data = $this->input->post();

				$insertData = array(
					'user_id' => $post_data['user'],
					'bank_name' => $post_data['bank_name'],
					'account_name' => $post_data['account_name'],
					'account_number' => $post_data['account_number'],
					'created_by' => session_data('user_id')
				);

				$insert = $this->common_model->insert($insertData, $tableName="user_bank_detail");
				$redirectUrl = 'hr/userBankDetail'; //redirect url

				if ($insert) {
					$message = 'User bank detail has been added.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to add user bank detail.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}

			}
		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Form for updating existing user bank detail is loaded.
	 * Validation and update functionality is also carried out within this function.
	 *
	 * @param int $bankDetailId
	 * @return void
	 */
	public function editUserBankDetail($bankDetailId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Edit User Bank Detail";
		self::$viewData['breadcrumb'] = "Edit User Bank Detail"; 
		self::$viewData['heading'] = "User Bank Detail Form";  
		// Getting list of user
		self::$viewData['userList'] = $this->auth_model->getUsersList();
		// Getting user bank detail by Id
		self::$viewData['detail'] = $this->common_model->getDataById($bankDetailId, $tableName = "user_bank_detail");
		self::$viewData['page'] = "hr/user_bank_detail/edit";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('user', ' ', 'required|trim');
			$this->form_validation->set_rules('bank_name', ' ', 'required|trim');
			$this->form_validation->set_rules('account_name', ' ', 'required|trim');
			$this->form_validation->set_rules('account_number', ' ', 'required|trim');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);
			} else {
				$post_data = $this->input->post();

				$updateData = array(
					'user_id' => $post_data['user'],
					'bank_name' => $post_data['bank_name'],
					'account_name' => $post_data['account_name'],
					'account_number' => $post_data['account_number'],
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s')
				);

				$update = $this->common_model->update($bankDetailId, $updateData, $tableName="user_bank_detail");
				$redirectUrl = 'hr/editUserBankDetail/'.$bankDetailId; //redirect url

				if ($update) {
					$message = 'User bank detail has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to update user bank detail.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
			}
		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Delete status is updated from 0 to 1.
	 * Soft delete concept is implemented.
	 *
	 * @param int $bankDetailId
	 * @return void
	 */
	public function deleteUserBankDetail($bankDetailId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		$softDelete = $this->common_model->softDelete($bankDetailId, $tableName='user_bank_detail');
		$redirectUrl = 'hr/userBankDetail'; //redirect url

		if ($softDelete) {
			$message = 'User bank detail has been deleted.';
			$this->session->set_flashdata('success_msg', $message);
			redirect($redirectUrl); 

		} else {
			$message = 'Failed to delete user bank detail.';
			$this->session->set_flashdata('error_msg', $message);
			redirect($redirectUrl); 
		}
	}
}