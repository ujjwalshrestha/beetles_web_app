<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Hr_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Attendance list is fetched of todays date.
	 *
	 * @return void
	 */
    public function getTodaysAttendance()
	{
		$todayDate = date('Y-m-d');
		$where = "DATE(a.attendance_datetime)='".$todayDate."'";

		$query = $this->db->select("a.*, as.*, ud.firstname, ud.lastname")
						   ->from("attendance as a")
						   ->join("attendance_status as as", "a.attendance_status_id = as.id")
						   ->join("user_detail as ud", "a.user_id = ud.user_id")
						   ->where($where)
						   ->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	/**
	 * Checks whether attendance record of logged in user exist or not.
	 *
	 * @return void
	 */
	public function checkTodayAttendance()
	{
		$todayDate = date('Y-m-d');
		$where = "DATE(attendance_datetime)='".$todayDate."' AND user_id=".session_data('user_id');

		$query = $this->db->get_where("attendance", $where);

		if ($query->num_rows() == 1) {
			return $query->row();
		} else {
			return false;
		}
	}

	/**
	 * Attendance data is fetched according to values fetched through parameter.
	 *
	 * @param int $userId
	 * @param string $dateFrom
	 * @param string $dateTo
	 * @return void
	 */
	public function getAttendanceReport($userId, $dateFrom, $dateTo)
	{
		if ($userId == 'all') {
			$where = "DATE(a.attendance_datetime) BETWEEN '$dateFrom' AND '$dateTo'";
		} else {
			$where = "a.user_id=".$userId." AND DATE(a.attendance_datetime) BETWEEN '$dateFrom' AND '$dateTo'";
		}

		$query = $this->db->select("a.*, as.*, ud.firstname, ud.lastname")
						   ->from("attendance as a")
						   ->join("attendance_status as as", "a.attendance_status_id = as.id")
						   ->join("user_detail as ud", "a.user_id = ud.user_id")
						   ->where($where)
						   ->order_by('a.attendance_datetime', 'asc')
						   ->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	/**
	 * User bank detail list is fetched.
	 *
	 * @return void
	 */
	public function getUserBankDetailList()
	{
		$where = array(
			'ubd.delete_status' => 0,
			'au.delete_status' => 0
		);
		
		$query = $this->db->select('ubd.*, ud.firstname, ud.lastname')
						   ->from('user_bank_detail as ubd')
						   ->join('auth_user as au', 'ubd.user_id = au.id')
						   ->join('user_detail as ud', 'ud.user_id = au.id')
						   ->where($where)
						   ->order_by('ud.firstname')
						   ->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	/**
	 * Fetch only attendance status with no time limit.
	 *
	 * @return array
	 */
	public function getAttendanceStatusByLimit()
	{
		$where = array(
			'time_limit' => 0,
			'delete_status' => 0
		);

		$query = $this->db->get_where('attendance_status', $where);

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}		
	}
}