<hr>
<table class="table table-hover table-bordered" id="data_table_active">
    <thead>
        <tr>
            <th>S.N</th>
            <th>Name</th>
            <th>Status</th>
            <th>Reason</th>
            <th>Entry Date</th>
            <th>Entry Time</th>
        </tr>                                        
    </thead>
    <tbody>
    <?php if ($attendanceReport): ?>
        <?php foreach ($attendanceReport as $key => $report):  ?>
            <tr class="<?php echo $report->bg_color_class ?>">
                <td nowrap><?php echo ++$key.'.' ?></td>
                <td nowrap><?php echo $report->firstname.' '.$report->lastname ?></td>
                <td class="text-center" nowrap><?php echo $report->status_name ?></td>
                <td><?php echo ($report->reason) ? $report->reason : "-" ?></td>
                <td class="text-center" nowrap>
                    <?php 
                        $timestamp = strtotime($report->attendance_datetime);
                        echo date('Y-m-d', $timestamp);
                    ?>
                </td>
                <td class="text-center" nowrap>
                    <?php 
                        $timestamp = strtotime($report->attendance_datetime);
                        echo date('H:i:s', $timestamp);
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
</table>