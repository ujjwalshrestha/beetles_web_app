<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">
        
        <div class="col-md-12 card-body" style="overflow-x: scroll;">

            <h4><?php echo $heading ?> <?php if ($checkAttendance == false): ?><a href="<?php echo base_url('hr/dailyAttendanceForm') ?>" class="btn btn-success btn-sm float-right"><span class="fa fa-plus"></span> Register</a><?php endif; ?></h4><hr>

            <?php if($this->session->flashdata('error_msg')): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                </div>
            <?php endif; ?>

            <?php if($this->session->flashdata('success_msg')): ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                </div>
            <?php endif; ?>

            <table class="table table-hover table-bordered" id="data_table_active">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Reason</th>
                        <th>Entry Time</th>
                    </tr>                                        
                </thead>
                <tbody>
                <?php if ($attendanceList): ?>
                    <?php foreach ($attendanceList as $key => $list):  ?>
                        <tr class="<?php echo $list->bg_color_class ?>">
                            <td nowrap><?php echo ++$key.'.' ?></td>
                            <td nowrap><?php echo $list->firstname.' '.$list->lastname ?></td>
                            <td class="text-center" nowrap><?php echo $list->status_name ?></td>
                            <td><?php echo ($list->reason) ? $list->reason : "-" ?></td>
                            <td nowrap>
                                <?php 
                                    $timestamp = strtotime($list->attendance_datetime);
                                    echo date('H:i:s', $timestamp);
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.card -->
</div>