<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">

        <?php
            $action = base_url('hr/ajaxAddDailyAttendance');
            $attributes = array(
                "id" => "add_daily_attendance", 
                "name" => "add_daily_attendance",
                "method" => "POST"
            );

            echo form_open($action, $attributes); 
        ?>
        
        <div class="col-md-12 card-body">

            <h4><?php echo $heading ?> <a href="<?php echo base_url('hr/dailyAttendance') ?>" class="btn btn-success btn-sm float-right" data-toggle="tooltip" data-placement="top" title="Back to List"><span class="fa fa-arrow-left"></span></a></h4><hr>

            <div class="col-md-7">
                <?php if($this->session->flashdata('error_msg')): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                    </div>
                <?php endif; ?>

                <?php if($this->session->flashdata('success_msg')): ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group row">
                <div class="col-md-5">
                    <strong id="realtime_date_time"></strong>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="max_stock_level">Employee Name</label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" value="<?php echo $userDetail->firstname.' '.$userDetail->lastname ?>" disabled>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="attendance_status">Select Attendance Status <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <select class="form-control select2" name="attendance_status" id="attendance_status">
                        <option value="">--- Select Attendance Status ---</option>
                        <!-- if time is between 12am and 10am, show all attendance status in select list -->
                        <?php if(date('H:i:s') >= '00:00:00' && date('H:i:s') <= '10:00:00'): ?>
                            <?php if($attendanceStatus): ?>
                                <?php foreach($attendanceStatus as $key => $status): ?>
                                    <option value="<?php echo $status->id ?>" data-reason-status="<?php echo $status->reason_status ?>"><?php echo ucfirst($status->status_name) ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        <!-- else show only late and leave options  -->
                        <?php else: ?>
                            <?php if($attendanceStatusByLimit): ?>
                                <?php foreach($attendanceStatusByLimit as $key => $status): ?>
                                    <option value="<?php echo $status->id ?>" data-reason-status="<?php echo $status->reason_status ?>"><?php echo ucfirst($status->status_name) ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </select>
                    <span class="form-msg"></span>
                </div>
            </div>

            <div class="form-group row d-none" id="reason_section">
                <div class="col-md-3">
                    <label for="reason">Reason <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <textarea type="text" class="form-control" id="reason" name="reason" placeholder="Please provide your reason if you are on leave or late for work." rows="4" cols="50"></textarea>
                    <span class="form-msg"></span>
                </div>
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-success" id="add_btn">Submit</button>

            <button class="btn btn-success" type="button" id="loading_btn" disabled>
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                Loading...
            </button>
        </div>

        <?php echo form_close() ?>
    </div>
    <!-- /.card -->

</div>

<script>
    $(document).ready(function() {
        $(document).off("change", "#attendance_status").on("change", "#attendance_status", function(e) {
            let obj = $(this),
                reasonStatus = obj.find(':selected').data('reason-status');

            if (reasonStatus == 0) {
                $("#reason_section").addClass("d-none");
            } else if (reasonStatus == 1) {
                $("#reason_section").removeClass("d-none");
            }
        });

        $(document).off("submit", "#add_daily_attendance").on("submit", "#add_daily_attendance", function(e) {
            e.preventDefault();

            let obj = $(this),
                url = obj.attr('action'),
                data = obj.serialize();

            $.ajax({
                type : "POST",
                url  : url,
                data : data,
                beforeSend: function(){
                    $('#add_btn').css('display', 'none');
                    $('#loading_btn').css('display', 'block');
                },
                complete: function(){
                    $('#loading_btn').css('display', 'none');
                    $('#add_btn').css('display', 'block');
                },
                success: function(resp) {
                    $("input[name="+resp.csrf_name+"]").val(resp.csrf_value); // replaces the csrf value from the hidden input field with the new one.

                    $.fn.hideError(obj); // hides the validation error

                    if (resp.status === "error") {
                        if (resp.message === 'form_error') {
                            $.each(resp.data, function(index, element) {
                                $.fn.displayError(element.id, element.message);
                            });

                        }  else if (resp.message === "insert_error") {
                            window.location = resp.url;
                        }

                    } else if (resp.status === 'success') {
                        window.location = resp.url;
                    }
                },
                error: function() {
                    alert('Internal Server Error!');
                }
            });
        });
    });
</script>