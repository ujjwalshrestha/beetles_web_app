<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">
        
        <div class="col-md-12 card-body" style="overflow-x: scroll;">

            <h4><?php echo $heading ?></h4><hr>

            
            <div class="col-md-10">
                <?php if($this->session->flashdata('error_msg')): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                    </div>
                <?php endif; ?>

                <?php if($this->session->flashdata('success_msg')): ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                    </div>
                <?php endif; ?>
            </div>

            <?php
                $action = base_url('hr/ajaxGetAttendanceReport');
                $attributes = array(
                    "id" => "attendance_report", 
                    "name" => "attendance_report",
                    "method" => "POST"
                );

                echo form_open($action, $attributes); 
            ?>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="user">Select User <span class="red-asterisk">*</span></label>
                    <select class="form-control select2" name="user" id="user">
                        <option value="">--- Select User ---</option>
                        <option value="all">All</option>
                        <?php if($userList): ?>
                            <?php foreach($userList as $key => $list): ?>
                                <option value="<?php echo $list->user_id ?>"><?php echo ucfirst($list->firstname).' '.ucfirst($list->lastname) ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <span class="form-msg"></span>
                </div>

                <div class="col-md-3">
                    <label for="max_stock_level">Date From <span class="red-asterisk">*</span></label>
                    <input type="date" class="form-control" name="date_from" id="date_from">
                    <span class="form-msg"></span>
                </div>
                
                <div class="col-md-3">
                    <label for="max_stock_level">Date To <span class="red-asterisk">*</span></label>
                    <input type="date" class="form-control" name="date_to" id="date_to">
                    <span class="form-msg"></span>
                </div>

                <div class="col-md-2">
                    <button type="submit" class="btn btn-success" id="get_report_btn">Get Report</button>

                    <button class="btn btn-success" type="button" id="loading_btn" disabled style="margin-top: 28px;">
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>
            </div>

            <?php echo form_close() ?>

            <div id="attendance_report_list">
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>

<script>
    $(document).ready(function() {
        $(document).off("submit", "#attendance_report").on("submit", "#attendance_report", function(e) {
            e.preventDefault();

            let obj = $(this),
                url = obj.attr('action'),
                data = obj.serialize();

            $.ajax({
                type : "POST",
                url  : url,
                data : data,
                beforeSend: function(){
                    $('#get_report_btn').css('display', 'none');
                    $('#loading_btn').css('display', 'block');
                },
                complete: function(){
                    $('#loading_btn').css('display', 'none');
                    $('#get_report_btn').css('display', 'block');
                },
                success: function(resp) {
                    $("input[name="+resp.csrf_name+"]").val(resp.csrf_value); // replaces the csrf value from the hidden input field with the new one.

                    $.fn.hideError(obj); // hides the validation error

                    if (resp.status === "error") {
                        if (resp.error_type === 'form_error') {
                            $.each(resp.data, function(index, element) {
                                $.fn.displayError(element.id, element.message);
                            });

                        } else if (resp.error_type === "date_error") {
                            $('#date_from').siblings('.form-msg').html(resp.message).show();

                        } 

                    } else if (resp.status === 'success') {
                        $('#attendance_report_list').html('');
                        $('#attendance_report_list').html(resp.data);

                        // DataTable
                        $('#data_table_active').DataTable({
                            "responsive" : true,
                            "lengthMenu": [ [10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "All"] ]
                        });
                    }
                },
                error: function() {
                    alert('Internal Server Error!');
                }
            });
        });
    });
</script>