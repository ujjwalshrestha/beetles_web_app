<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">

        <?php
            $action = base_url('hr/editUserBankDetail/'.$detail->id);
            $attributes = array(
                "id" => "user_bank_detail_form", 
                "name" => "user_bank_detail_form",
                "method" => "POST"
            );

            echo form_open($action, $attributes); 
        ?>
        
        <div class="col-md-12 card-body">

            <h4><?php echo $heading ?> <a href="<?php echo base_url('hr/userBankDetail') ?>" class="btn btn-success btn-sm float-right" data-toggle="tooltip" data-placement="top" title="Back to List"><span class="fa fa-arrow-left"></span></a></h4><hr>

            <div class="col-md-6">
                <?php if($this->session->flashdata('error_msg')): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                    </div>
                <?php endif; ?>

                <?php if($this->session->flashdata('success_msg')): ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="user">Select User <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <select class="form-control select2" name="user" id="user">
                        <option value="">--- Select User ---</option>
                        <?php if($userList): ?>
                            <?php foreach($userList as $key => $list): ?>
                                <option value="<?php echo $list->user_id ?>" <?php echo ($list->user_id === $detail->user_id) ? 'selected' : ''; ?>><?php echo ucfirst($list->firstname).' '.ucfirst($list->lastname) ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <?php echo form_error('user'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="bank_name">Bank Name <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Enter Bank Name" value="<?php echo $detail->bank_name ?>">
                    <?php echo form_error('bank_name'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="account_name">Account Name <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="account_name" name="account_name" placeholder="Enter Account Name" value="<?php echo $detail->account_name ?>">
                    <?php echo form_error('account_name'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="account_number">Account Number <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="account_number" name="account_number" placeholder="Enter Account Number" value="<?php echo $detail->account_number ?>">
                    <?php echo form_error('account_number'); ?>
                </div>
            </div>
            
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-success">Add</button>
        </div>

        <?php echo form_close() ?>
    </div>
    <!-- /.card -->

</div>