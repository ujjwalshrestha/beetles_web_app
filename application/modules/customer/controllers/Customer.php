<?php 

if( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * All the functionality related CMS, User Management and other backend functions are implemented in this class named 'Admin'.
 */
class Customer extends MX_Controller
{
    /*
	 *	variable to hold data for view
	 */
	private static $viewData = array();
	var $customerGroupId;

	public function __construct()
	{
		parent::__construct();

		// To show the username in admin dashboard of the logged in user
		if (isLoggedin()) { // uri_segment(2) && uri_segment(2) != 'login'
			self::$viewData['username'] = $this->auth_model->getAuthUser()->username;
		}

		// Dashboard side menu
		self::$viewData['dashboardSideMenu'] = $this->auth_model->getDashboardSideMenu();

		// Checks if the auth active status, auth temp_status and user group login_status is 0 or not. If one of them is 0, then user is forced to logout of the system.
		if (isLoggedin() && (($this->auth_model->getAuthUser()->active_status == 0) || ($this->auth_model->getAuthUser()->temp_status == 0) || ($this->auth_model->getUserGroupById(session_data('group_id'))->login_status == 0)) ) 
		{
			$this->logout(); // force logout
		}

		$this->customerGroupId = 8;
	}

	public function index()
	{
		show_404();
	}

	/**
	 * All the list of wholesale customer is displayed.
	 * Management of wholesale customer is done in this page.
	 *
	 * @return void
	 */
	public function wholesaleCustomer()
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 25;
		self::$viewData['title'] = "Manage Wholesale Customers";
		self::$viewData['breadcrumb'] = "Manage Wholesale Customers"; 
		self::$viewData['heading'] = "Wholesale Customers List";
		self::$viewData['customerList'] = $this->customer_model->getWholesaleCustomerList(); // join query
		self::$viewData['page'] = "customer/wholesale/list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * First, form validation is carried out.
	 * Then, auth data of customer is inserted, followed by inserting of details of customer.
	 * Lastly, user group mapping is done.
	 *
	 * @return void
	 */
	public function addWholesaleCustomer()
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Add Wholesale Customer";
		self::$viewData['breadcrumb'] = "Add Wholesale Customer"; 
		self::$viewData['heading'] = "Add Wholesale Customer Form";
		self::$viewData['page'] = "customer/wholesale/add";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('customer_name', ' ', 'required|trim');
			$this->form_validation->set_rules('address', ' ', 'required|trim');
			$this->form_validation->set_rules('pan_vat', ' ', 'required|trim');
			$this->form_validation->set_rules('contact', ' ', 'required|trim');
			$this->form_validation->set_rules('email', ' ', 'required|trim|valid_email|is_unique[auth_user.email]',
				array('is_unique' => 'This email already exists.')
			);
			$this->form_validation->set_rules('username', ' ', 'required|trim|is_unique[auth_user.username]',
				array('is_unique' => 'This username already exists.')
			);
			$this->form_validation->set_rules('password', ' ', 'required|trim');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				$customer_name = $this->input->post('customer_name');
				$address = $this->input->post('address');
				$pan_vat = $this->input->post('pan_vat');
				$contact = $this->input->post('contact');
				$email = $this->input->post('email');
				$username = $this->input->post('username');
				$password = $this->input->post('password');

				// This data will be inserted in table 'auth_user'
				$insertCustomerAuthData = array(
					'email' => $email,
					'username' => $username,
					'password' => encrypt_password($password),
					'created_by' => session_data('user_id'),
				);

				$this->db->trans_begin();

				// Here last inserted user_id is returned.
				$insertCustomerAuth = $this->customer_model->insertCustomerAuth($insertCustomerAuthData); 
				$userId = $insertCustomerAuth;

				// This data will be inserted in table 'customer'
				$insertCustomerDetailData = array(
					'user_id' => $userId,
					'name' => $customer_name,
					'address' => $address,
					'pan_vat' => $pan_vat,
					'contact' => $contact,
					'created_by' => session_data('user_id'),
				);

				$insertCustomerDetail = $this->common_model->insert($insertCustomerDetailData, $tableName="wholesale_customer");

				// This data will be inserted in table 'auth_user_group_mapping'
				$insertUserGroupMapData = array(
					'user_id' => $userId,
					'group_id' => $this->customerGroupId,
					'created_by' => session_data('user_id'),
				);

				$insertUserGroupMapping = $this->common_model->insert($insertUserGroupMapData, $tableName="auth_user_group_mapping");

				if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}

				//redirect url
				$redirectUrl = 'customer/wholesaleCustomer'; 

				if ($insertCustomerAuth && $insertCustomerDetail && $insertUserGroupMapping) {
					$message = 'Customer has been added.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to add customer.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * First, form validation is carried out.
	 * Then, auth data of customer is updated, followed by updating of details of customer.
	 *
	 * @param int $customerId
	 * @return void
	 */
	public function editWholesaleCustomer($customerId)
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Manage Wholesale Customer";
		self::$viewData['breadcrumb'] = "Manage Wholesale Customer"; 
		self::$viewData['heading'] = "Edit Wholesale Customer Form";
		self::$viewData['detail'] = $this->customer_model->getCustomerDetailById($customerId); // join query
		// print_r(self::$viewData['detail']); exit;
		self::$viewData['page'] = "customer/wholesale/edit";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('customer_name', ' ', 'required|trim');
			$this->form_validation->set_rules('address', ' ', 'required|trim');
			$this->form_validation->set_rules('pan_vat', ' ', 'required|trim');
			$this->form_validation->set_rules('contact', ' ', 'required|trim');
			$this->form_validation->set_rules('email', ' ', 'required|trim|valid_email');
			$this->form_validation->set_rules('username', ' ', 'required|trim');
			$this->form_validation->set_rules('password', ' ', 'required|trim');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				$customer_name = $this->input->post('customer_name');
				$address = $this->input->post('address');
				$pan_vat = $this->input->post('pan_vat');
				$contact = $this->input->post('contact');
				$email = $this->input->post('email');
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$status = $this->input->post('status');

				//redirect url
				$redirectUrl = 'customer/editWholesaleCustomer/'.$customerId; 

				// Checking if email exists or not. If it does, provide an error message.
				$checkEmail = $this->auth_model->checkEmail($email, $customerId);
				if ($checkEmail) {
					$message = 'Email already exists.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}

				// Checking if username exists or not. If it does, provide an error message.
				$checkUsername = $this->auth_model->checkUsername($username, $customerId);
				if ($checkUsername) {
					$message = 'Username already exists.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}

				// This data will be updated in table 'auth_user'
				$updateCustomerAuthData = array(
					'email' => $email,
					'username' => $username,
					'password' => encrypt_password($password),
					'active_status' => $status,
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s'),
				);
				
				// This data will be updated in table 'wholesale_customer'
				$updateCustomerDetailData = array(
					'name' => $customer_name,
					'address' => $address,
					'pan_vat' => $pan_vat,
					'contact' => $contact,
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s'),
				);

				// $this->db->trans_begin();

				// updates customer's auth data in table 'auth_user'
				$updateCustomerAuth = $this->common_model->update($customerId, $updateCustomerAuthData, $tableName="auth_user");

				// updates customer's detail data in table 'wholesale_customer'
				$updateCustomerDetail = $this->customer_model->updateCustomerDetail($updateCustomerDetailData, $customerId);

				// checks if customer auth and detail data is updated or not
				if ($updateCustomerAuth && $updateCustomerDetail) {
					$message = 'Customer detail has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl);

				} else {
					$message = 'Failed to update customer detail.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}

				// if ($this->db->trans_status() === FALSE) {
				// 	$this->db->trans_rollback();
				// } else {
				// 	$this->db->trans_commit();
				// }
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Delete status is updated from 0 to 1.
	 * Soft delete concept is implemented.
	 *
	 * @param int $customerId
	 * @return void
	 */
	public function deleteWholesaleCustomer($customerId)
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		$softDelete = $this->common_model->softDelete($customerId, $tableName='auth_user');
		$redirectUrl = 'customer/wholesaleCustomer'; //redirect url

		if ($softDelete) {
			$message = 'Customer has been deleted.';
			$this->session->set_flashdata('success_msg', $message);
			redirect($redirectUrl); 

		} else {
			$message = 'Failed to delete customer.';
			$this->session->set_flashdata('error_msg', $message);
			redirect($redirectUrl); 
		}
	}

	/**
	 * Through this function, user profile page is displayed.
	 *
	 * @return void
	 */
	public function profile()
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "User Profile";
		self::$viewData['breadcrumb'] = "User Profile"; 
		// gets both customer auth and details
		self::$viewData['detail'] = $this->customer_model->getCustomerDetailById(session_data('user_id')); 
		self::$viewData['page'] = "customer/customer_profile";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * All the list of retail customer is displayed.
	 * Management of retail customer is done in this page.
	 *
	 * @return void
	 */
	public function retailCustomer()
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 32;
		self::$viewData['title'] = "Manage Retail Customers";
		self::$viewData['breadcrumb'] = "Manage Retail Customers"; 
		self::$viewData['heading'] = "Retail Customers List";
		self::$viewData['customerList'] = $this->common_model->getData($tableName="retail_customer"); // join query
		self::$viewData['page'] = "customer/retail/list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * First, form validation is carried out.
	 * Then, detail of retail customer is inserted.
	 *
	 * @return void
	 */
	public function addRetailCustomer()
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Add Retail Customer";
		self::$viewData['breadcrumb'] = "Add Retail Customer"; 
		self::$viewData['heading'] = "Add Retail Customer Form";
		self::$viewData['page'] = "customer/retail/add";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('customer_name', ' ', 'required|trim');
			$this->form_validation->set_rules('address', ' ', 'required|trim');
			$this->form_validation->set_rules('contact', ' ', 'required|trim');
			$this->form_validation->set_rules('email', ' ', 'trim|valid_email|is_unique[retail_customer.email]',
				array('is_unique' => 'This email already exists.')
			);
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				$customer_name = $this->input->post('customer_name');
				$address = $this->input->post('address');
				$contact = $this->input->post('contact');
				$email = $this->input->post('email');

				// This data will be inserted in table 'customer'
				$insertData = array(
					'name' => $customer_name,
					'address' => $address,
					'email' => $email,
					'contact' => $contact,
					'created_by' => session_data('user_id'),
				);

				$insert = $this->common_model->insert($insertData, $tableName="retail_customer");

				//redirect url
				$redirectUrl = 'customer/retailCustomer'; 

				if ($insert) {
					$message = 'Customer has been added.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to add customer.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * First, form validation is carried out.
	 * Then, of details of retail customer is updated.
	 *
	 * @param int $customerId
	 * @return void
	 */
	public function editRetailCustomer($customerId)
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Manage Retail Customer";
		self::$viewData['breadcrumb'] = "Manage Retail Customer"; 
		self::$viewData['heading'] = "Edit Retail Customer Form";
		self::$viewData['detail'] = $this->common_model->getDataById($customerId, $table_name="retail_customer"); // join query
		self::$viewData['page'] = "customer/retail/edit";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('customer_name', ' ', 'required|trim');
			$this->form_validation->set_rules('address', ' ', 'required|trim');
			$this->form_validation->set_rules('contact', ' ', 'required|trim');
			$this->form_validation->set_rules('email', ' ', 'trim|valid_email');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				$customer_name = $this->input->post('customer_name');
				$address = $this->input->post('address');
				$contact = $this->input->post('contact');
				$email = $this->input->post('email');

				//redirect url
				$redirectUrl = 'customer/editRetailCustomer/'.$customerId; 

				// Checking if email exists or not. If it does, provide an error message.
				$checkEmail = $this->customer_model->checkRetailCustomerEmail($email, $customerId);
				if ($checkEmail) {
					$message = 'Email already exists.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
				
				// This data will be updated in table 'wholesale_customer'
				$updateData = array(
					'name' => $customer_name,
					'address' => $address,
					'email' => $email,
					'contact' => $contact,
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s'),
				);

				// updates customer's data in table 'retail_customer'
				$update = $this->common_model->update($customerId, $updateData, $tableName="retail_customer");

				if ($update) {
					$message = 'Customer detail has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl);

				} else {
					$message = 'Failed to update customer detail.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Delete status is updated from 0 to 1.
	 * Soft delete concept is implemented.
	 *
	 * @param int $customerId
	 * @return void
	 */
	public function deleteRetailCustomer($customerId)
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		$softDelete = $this->common_model->softDelete($customerId, $tableName='retail_customer');
		$redirectUrl = 'customer/retailCustomer'; //redirect url

		if ($softDelete) {
			$message = 'Customer has been deleted.';
			$this->session->set_flashdata('success_msg', $message);
			redirect($redirectUrl); 

		} else {
			$message = 'Failed to delete customer.';
			$this->session->set_flashdata('error_msg', $message);
			redirect($redirectUrl); 
		}
	}
}