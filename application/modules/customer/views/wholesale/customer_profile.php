<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">
        <div class="col-md-12 card-body">
            <table class="table table-responsive-lg" id="user_profile_table">
                <tr>
                    <th class="top-borderless">Name</th>
                    <td class="top-borderless"><?php echo ucfirst($detail->name); ?></td>
                </tr>

                <tr>
                    <th>Address</th>
                    <td><?php echo ($detail->address) ? $detail->address : '-'; ?></td>
                </tr>

                <tr>
                    <th>PAN/VAT No.</th>
                    <td><?php echo ($detail->pan_vat) ? $detail->pan_vat : '-'; ?></td>
                </tr>

                <tr>
                    <th>Contact</th>
                    <td><?php echo ($detail->contact) ? $detail->contact : '-'; ?></td>
                </tr>

                <tr>
                    <th>Email</th>
                    <td><?php echo $detail->email ?></td>
                </tr>

                <tr>
                    <th>Username</th>
                    <td><?php echo $detail->username ?></td>
                </tr>

                <tr>
                    <th>Account Status</th>
                    <td><span class="badge badge-success">ACTIVE</span></td>
                </tr>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>