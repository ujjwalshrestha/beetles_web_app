<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">
        
        <div class="col-md-12 card-body" style="overflow-x: scroll;">

            <h4><?php echo $heading ?> <?php if (session_data('is_super_admin') == 1 || addAccess($dashboardMenuId) == 1): ?><a href="<?php echo base_url('customer/addWholesaleCustomer') ?>" class="btn btn-success btn-sm float-right"><span class="fa fa-plus"></span> Add New</a><?php endif; ?></h4><hr>

            <?php if($this->session->flashdata('error_msg')): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                </div>
            <?php endif; ?>

            <?php if($this->session->flashdata('success_msg')): ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                </div>
            <?php endif; ?>

            <table class="table table-hover table-bordered" id="data_table_active">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th class="text-center">Action</th>
                        <th>Customer Name</th>
                        <th>Address</th>
                        <th>PAN/VAT No.</th>
                        <th>Contact</th>
                        <th>Email</th>
                        <th>Active Status</th>
                    </tr>                                        
                </thead>
                <tbody>
                    <?php if ($customerList): ?>
                        <?php foreach ($customerList as $key => $list):  ?>
                            <tr>
                                <td class="text-center" nowrap><?php echo ++$key.'.' ?></td>
                                <td class="text-center" nowrap>
                                    <?php if (session_data('is_super_admin') == 1 || editAccess($dashboardMenuId) == 1): ?>
                                        <a href="<?php echo base_url('customer/editWholesaleCustomer/'.$list->user_id) ?>" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Edit"><span class="fa fa-edit" style="color: white"></span></a>&nbsp;
                                    <?php endif; ?> 

                                    <?php if (session_data('is_super_admin') == 1 || deleteAccess($dashboardMenuId) == 1): ?>
                                        <a href="<?php echo base_url('customer/deleteWholesaleCustomer/'.$list->user_id) ?>" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete" onClick="return confirm('Are you sure to delete this data?')" ><span class="fa fa-trash" style="color: white"></span></a>
                                    <?php endif; ?>

                                    <?php if (session_data('is_super_admin') == 0 && (deleteAccess($dashboardMenuId) == 0 && editAccess($dashboardMenuId) == 0)): ?>
                                        <span>-</span>
                                    <?php endif; ?>
                                </td>
                                <td nowrap><?php echo $list->name ?></td>
                                <td nowrap><?php echo $list->address ?></td>
                                <td nowrap><?php echo $list->pan_vat ?></td>
                                <td nowrap><?php echo $list->contact ?></td>
                                <td nowrap><?php echo $list->email ?></td>
                                <td nowrap class="text-center"><?php echo ($list->active_status == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>' ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>