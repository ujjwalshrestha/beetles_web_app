<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">

        <?php
            $action = base_url('customer/addWholesaleCustomer');
            $attributes = array(
                "id" => "add_customer_form", 
                "name" => "add_customer_form",
                "method" => "POST"
            );

            echo form_open($action, $attributes); 
        ?>
        
        <div class="col-md-12 card-body">

            <h4><?php echo $heading ?> <a href="<?php echo base_url('customer/wholesaleCustomer') ?>" class="btn btn-success btn-sm float-right" data-toggle="tooltip" data-placement="top" title="Back to List"><span class="fa fa-arrow-left"></span></a></h4><hr>

            <?php if($this->session->flashdata('error_msg')): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                </div>
            <?php endif; ?>

            <?php if($this->session->flashdata('success_msg')): ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                </div>
            <?php endif; ?>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="customer_name">Customer Name <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="customer_name" name="customer_name" placeholder="Enter Customer Name" value="<?php echo set_value('customer_name') ?>">
                    <?php echo form_error('customer_name'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="address">Address <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address" value="<?php echo set_value('address') ?>" >
                    <?php echo form_error('address'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="pan_vat">PAN/VAT No. <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="pan_vat" name="pan_vat" placeholder="Enter PAN/VAT No." value="<?php echo set_value('pan_vat') ?>" >
                    <?php echo form_error('pan_vat'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="contact">Contact <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="contact" name="contact" placeholder="Enter Contact" value="<?php echo set_value('contact') ?>" >
                    <?php echo form_error('contact'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="email">Email <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" value="<?php echo set_value('email') ?>" >
                    <?php echo form_error('email'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="username">Username <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Enter Username" value="<?php echo set_value('username') ?>" >
                    <?php echo form_error('username'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="password">Password <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" >
                    <?php echo form_error('password'); ?>
                </div>
            </div>
            
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-success">Add</button>
        </div>

        <?php echo form_close(); ?>
    </div>
    <!-- /.card -->

</div>