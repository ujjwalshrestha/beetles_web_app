<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * All the wholesale customer list is fetched.
	 *
	 * @return array
	 */
	public function getWholesaleCustomerList()
	{
		$where = array(
			'au.delete_status' => 0
		);

		$query = $this->db->select('wc.*, au.username, au.email, au.active_status')
						   ->from('auth_user as au')
						   ->join('wholesale_customer as wc', 'au.id = wc.user_id')
						   ->where($where)
						   ->order_by('wc.name')
						   ->get();

		if ($query->num_rows() > 0) {
			return $query->result();

		} else {
			return false;
		}				
	}

	/**
	 * Wholesale Customer auth data is inserted.
	 * Last insert id from auth_user table is returned.
	 *
	 * @param array $insert_auth_user
	 * @return int
	 */
	public function insertCustomerAuth($insertAuthUserData)
	{
		$this->db->insert('auth_user', $insertAuthUserData);
		$insert_id = $this->db->insert_id();

   		return $insert_id;
	}

	/**
	 * Both auth and detail data of wholesale customer is fetched by customer's user id.
	 *
	 * @param int $customerId
	 * @return array
	 */
	public function getCustomerDetailById($customerId)
	{
		$where = array(
			'au.id' => $customerId
		);

		$result = $this->db->select('wc.*, au.username, au.email, au.password, au.active_status')
						   ->from('auth_user as au')
						   ->join('wholesale_customer as wc', 'au.id = wc.user_id')
						   ->where($where)
						   ->get();

		if ($result->num_rows() == 1) {
			return $result->row();

		} else {
			return false;
		}	
	}

	/**
	 * Customer detail is updated.
	 *
	 * @param array $updateData
	 * @param int $customerId
	 * @return boolean
	 */
	public function updateCustomerDetail($updateData, $customerId)
	{
		$where = array(
			'user_id' => $customerId
		);

		$this->db->where($where);
		return $this->db->update('wholesale_customer', $updateData);
	}

	/**
	 * Email Id is checked to determine if it exists or not.
	 *
	 * @param string $email
	 * @param int $user_id
	 * @return array
	 */
	public function checkRetailCustomerEmail($email, $customer_id)
	{
		$where = array(
			'email' => $email
		);

		$this->db->where_not_in('id', $customer_id);
		$result = $this->db->get_where('retail_customer', $where);

		if ($result->num_rows() == 1) {
			return $result->row();

		} else {
			return false;
		}	
	}
}