<?php 

if( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * All the functionality related CMS, User Management and other backend functions are implemented in this class named 'Admin'.
 */
class Inventory extends MX_Controller
{
    /*
	 *	variable to hold data for view
	 */
	private static $viewData = array();
	
	public function __construct()
	{
		parent::__construct();

		// To show the username in admin dashboard of the logged in user
		if (isLoggedin()) { 
			self::$viewData['username'] = $this->auth_model->getAuthUser()->username;
		}

		// Dashboard side menu
		self::$viewData['dashboardSideMenu'] = $this->auth_model->getDashboardSideMenu();

		// Checks if the auth active status, auth temp_status and user group login_status is 0 or not. If one of them is 0, then user is forced to logout of the system.
		if (isLoggedin() && (($this->auth_model->getAuthUser()->active_status == 0) || ($this->auth_model->getAuthUser()->temp_status == 0) || ($this->auth_model->getUserGroupById(session_data('group_id'))->login_status == 0)) ) 
		{
			$this->logout(); // force logout
		}
	}

	public function index()
	{
		show_404();
	}

	/**
	 * All the list of product category one is displayed.
	 * Management of product category one is done in this page.
	 *
	 * @return void
	 */
	public function productCategoryOne()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 13;
		self::$viewData['title'] = "Product Category";
		self::$viewData['breadcrumb'] = "Product Category"; 
		self::$viewData['heading'] = "Product Category One List";
		// Getting all product category one list
		self::$viewData['categoryList'] = $this->common_model->getData($tableName = "product_category_one");
		self::$viewData['page'] = "inventory/product_category/one/list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * Form for adding new product category one is loaded.
	 * Validation and insert functionality is also carried out within this function.
	 *
	 * @return void
	 */
	public function addProductCategoryOne()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Add Product Category";
		self::$viewData['breadcrumb'] = "Add Product Category"; 
		self::$viewData['heading'] = "Add Product Category One"; 
		self::$viewData['page'] = "inventory/product_category/one/add";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('category_name', ' ', 'required|trim');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);
			} else {
				$category_name = $this->input->post('category_name');

				$insertData = array(
					'category_name' => $category_name,
					'created_by' => session_data('user_id')
				);

				$tableName = "product_category_one"; // db table name
				$insertProductCategoryOne = $this->common_model->insert($insertData, $tableName);
				$redirectUrl = 'inventory/productCategoryOne'; //redirect url

				if ($insertProductCategoryOne) {
					$message = 'Product category one has been added.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to add product category one.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}

			}
		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Form for updating existing product category one is loaded.
	 * Validation and update functionality is also carried out within this function.
	 *
	 * @param int $categoryId
	 * @return void
	 */
	public function editProductCategoryOne($categoryId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Edit Product Category";
		self::$viewData['breadcrumb'] = "Edit Product Category"; 
		self::$viewData['heading'] = "Edit Product Category One";  
		// Getting product category one detail by Id
		self::$viewData['detail'] = $this->common_model->getDataById($categoryId, $tableName = "product_category_one");
		self::$viewData['page'] = "inventory/product_category/one/edit";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('category_name', ' ', 'required|trim');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);
			} else {
				$category_name = $this->input->post('category_name');

				$updateData = array(
					'category_name' => $category_name,
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s')
				);

				$updateProductCategoryOne = $this->common_model->update($categoryId, $updateData, $tableName = "product_category_one");
				$redirectUrl = 'inventory/editProductCategoryOne/'.$categoryId; //redirect url

				if ($updateProductCategoryOne) {
					$message = 'Product category one has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to update product category one.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
			}
		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Delete status is updated from 0 to 1.
	 * Soft delete concept is implemented.
	 *
	 * @param int $category_id
	 * @return void
	 */
	public function deleteProductCategoryOne($categoryId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		$tableName = "product_category_one"; // db table name
		$softDelete = $this->common_model->softDelete($categoryId, $tableName);
		$redirectUrl = 'inventory/productCategoryOne'; //redirect url

		if ($softDelete) {
			$message = 'Product category one has been deleted.';
			$this->session->set_flashdata('success_msg', $message);
			redirect($redirectUrl); 

		} else {
			$message = 'Failed to delete product category one.';
			$this->session->set_flashdata('error_msg', $message);
			redirect($redirectUrl); 
		}
	}

	/**
	 * All the list of product category two is displayed.
	 * Management of product category two is done in this page.
	 *
	 * @return void
	 */
	public function productCategoryTwo()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 14;
		self::$viewData['title'] = "Product Category";
		self::$viewData['breadcrumb'] = "Product Category"; 
		self::$viewData['heading'] = "Product Category Two List";
		self::$viewData['categoryList'] = $this->inventory_model->getProductCategoryTwo(); // join query
		self::$viewData['page'] = "inventory/product_category/two/list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * Form for adding new product category two is loaded.
	 * Validation and insert functionality is also carried out within this function.
	 *
	 * @return void
	 */
	public function addProductCategoryTwo()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Add Product Category";
		self::$viewData['breadcrumb'] = "Add Product Category"; 
		self::$viewData['heading'] = "Add Product Category Two"; 
		// Getting all product category one list (dependency concept: select field)
		self::$viewData['productCategoryOne'] = $this->common_model->getData($tableName = "product_category_one");
		self::$viewData['page'] = "inventory/product_category/two/add";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('prod_category_one', ' ', 'required|trim');
			$this->form_validation->set_rules('category_name', ' ', 'required|trim');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);
			} else {
				$prod_cat_one_id = $this->input->post('prod_category_one');
				$category_name = $this->input->post('category_name');
				$box_no = $this->input->post('box_no');

				$insertData = array(
					'prod_cat_one_id' => $prod_cat_one_id,
					'category_name' => $category_name,
					'box_no' => $box_no,
					'created_by' => session_data('user_id')
				);

				$tableName = "product_category_two"; // db table name
				$insertProductCategoryTwo = $this->common_model->insert($insertData, $tableName);
				$redirectUrl = 'inventory/productCategoryTwo'; //redirect url

				if ($insertProductCategoryTwo) {
					$message = 'Product category two has been added.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to add product category two.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}

			}
		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Form for updating existing product category two is loaded.
	 * Validation and update functionality is also carried out within this function.
	 *
	 * @param int $categoryId
	 * @return void
	 */
	public function editProductCategoryTwo($categoryId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Edit Product Category";
		self::$viewData['breadcrumb'] = "Edit Product Category"; 
		self::$viewData['heading'] = "Edit Product Category Three";  
		// Getting all product category one list (dependency concept: select field)
		self::$viewData['productCategoryOne'] = $this->common_model->getData($tableName = "product_category_one"); 
		// Getting product category detail two by Id
		self::$viewData['detail'] = $this->common_model->getDataById($categoryId, $tableName = "product_category_two");
		self::$viewData['page'] = "inventory/product_category/two/edit";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('prod_category_one', ' ', 'required|trim');
			$this->form_validation->set_rules('category_name', ' ', 'required|trim');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);
			} else {
				$prod_cat_one_id = $this->input->post('prod_category_one');
				$category_name = $this->input->post('category_name');
				$box_no = $this->input->post('box_no');

				$updateData = array(
					'prod_cat_one_id' => $prod_cat_one_id,
					'category_name' => $category_name,
					'box_no' => $box_no,
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s')
				);

				$tableName = "product_category_two"; // db table name
				$updateProductCategoryTwo = $this->common_model->update($categoryId, $updateData, $tableName);
				$redirectUrl = 'inventory/editProductCategoryTwo/'.$categoryId; //redirect url

				if ($updateProductCategoryTwo) {
					$message = 'Product category two has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to update product category two.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
			}
		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Delete status is updated from 0 to 1.
	 * Soft delete concept is implemented.
	 *
	 * @param int $category_id
	 * @return void
	 */
	public function deleteProductCategoryTwo($categoryId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		$tableName = "product_category_two"; // db table name
		$softDelete = $this->common_model->softDelete($categoryId, $tableName);
		$redirectUrl = 'inventory/productCategoryTwo'; //redirect url

		if ($softDelete) {
			$message = 'Product category two has been deleted.';
			$this->session->set_flashdata('success_msg', $message);
			redirect($redirectUrl); 

		} else {
			$message = 'Failed to delete product category two.';
			$this->session->set_flashdata('error_msg', $message);
			redirect($redirectUrl); 
		}
	}

	/**
	 * All the list of product category three is displayed.
	 * Management of product category three is done in this page.
	 *
	 * @return void
	 */
	public function productCategoryThree()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 15;
		self::$viewData['title'] = "Product Category";
		self::$viewData['breadcrumb'] = "Product Category"; 
		self::$viewData['heading'] = "Product Category Three List";
		self::$viewData['categoryList'] = $this->inventory_model->getProductCategoryThree(); // join query
		self::$viewData['page'] = "inventory/product_category/three/list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * Form for adding new product category three is loaded.
	 * Validation and insert functionality is also carried out within this function.
	 *
	 * @return void
	 */
	public function addProductCategoryThree()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Add Product Category";
		self::$viewData['breadcrumb'] = "Add Product Category"; 
		self::$viewData['heading'] = "Add Product Category Three"; 
		// Getting all product category two list (dependency concept: select field)
		self::$viewData['productCategoryTwo'] = $this->common_model->getData($tableName = "product_category_two");
		self::$viewData['page'] = "inventory/product_category/three/add";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('prod_category_two', ' ', 'required|trim');
			$this->form_validation->set_rules('category_name', ' ', 'required|trim');
			$this->form_validation->set_rules('reorder_level', ' ', 'integer|trim');
			$this->form_validation->set_rules('max_stock_level', ' ', 'integer|trim');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);
			} else {
				$prod_cat_two_id = $this->input->post('prod_category_two');
				$category_name = $this->input->post('category_name');
				$reorder_level = $this->input->post('reorder_level');
				$max_stock_level = $this->input->post('max_stock_level');

				$insertData = array(
					'prod_cat_two_id' => $prod_cat_two_id,
					'category_name' => $category_name,
					'reorder_level' => $reorder_level,
					'max_stock_level' => $max_stock_level,
					'created_by' => session_data('user_id')
				);

				$tableName = "product_category_three"; // db table name
				$insertProductCategoryThree = $this->common_model->insert($insertData, $tableName);
				$redirectUrl = 'inventory/productCategoryThree'; //redirect url

				if ($insertProductCategoryThree) {
					$message = 'Product category three has been added.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to add product category three.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}

			}
		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Form for updating existing product category three is loaded.
	 * Validation and update functionality is also carried out within this function.

	 *
	 * @param int $categoryId
	 * @return void
	 */
	public function editProductCategoryThree($categoryId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Edit Product Category";
		self::$viewData['breadcrumb'] = "Edit Product Category"; 
		self::$viewData['heading'] = "Edit Product Category One";  
		// Getting all product category two list (dependency concept: select field)
		self::$viewData['productCategoryTwo'] = $this->common_model->getData($tableName = "product_category_two"); 
		// Getting product category three detail by Id
		self::$viewData['detail'] = $this->common_model->getDataById($categoryId, $tableName = "product_category_three");
		self::$viewData['page'] = "inventory/product_category/three/edit";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('prod_category_two', ' ', 'required|trim');
			$this->form_validation->set_rules('category_name', ' ', 'required|trim');
			$this->form_validation->set_rules('reorder_level', ' ', 'integer|trim');
			$this->form_validation->set_rules('max_stock_level', ' ', 'integer|trim');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);
			} else {
				$prod_cat_two_id = $this->input->post('prod_category_two');
				$category_name = $this->input->post('category_name');
				$reorder_level = $this->input->post('reorder_level');
				$max_stock_level = $this->input->post('max_stock_level');

				$updateData = array(
					'prod_cat_two_id' => $prod_cat_two_id,
					'category_name' => $category_name,
					'reorder_level' => $reorder_level,
					'max_stock_level' => $max_stock_level,
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s')
				);

				$tableName = "product_category_three"; // db table name
				$updateProductCategoryThree = $this->common_model->update($categoryId, $updateData, $tableName);
				$redirectUrl = 'inventory/editProductCategoryThree/'.$categoryId; //redirect url

				if ($updateProductCategoryThree) {
					$message = 'Product category three has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to update product category three.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
			}
		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Delete status is updated from 0 to 1.
	 * Soft delete concept is implemented.
	 *
	 * @param int $category_id
	 * @return void
	 */
	public function deleteProductCategoryThree($categoryId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		$tableName = "product_category_three"; // db table name
		$softDelete = $this->common_model->softDelete($categoryId, $tableName);
		$redirectUrl = 'inventory/productCategoryThree'; //redirect url

		if ($softDelete) {
			$message = 'Product category three has been deleted.';
			$this->session->set_flashdata('success_msg', $message);
			redirect($redirectUrl); 

		} else {
			$message = 'Failed to delete product category three.';
			$this->session->set_flashdata('error_msg', $message);
			redirect($redirectUrl); 
		}
	}

	/**
	 * Company detail is displayed and updated within this function.
	 *
	 * @return void
	 */
	public function companyDetail()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Company Detail";
		self::$viewData['breadcrumb'] = "Company Detail"; 
		self::$viewData['heading'] = "Manage Company Detail";
		self::$viewData['detail'] = $this->inventory_model->getCompanyDetail();
		self::$viewData['page'] = "inventory/company_detail";
		
		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('company_name', ' ', 'required|trim');
			$this->form_validation->set_rules('pan_vat', ' ', 'required|trim');
			$this->form_validation->set_rules('address', ' ', 'required|trim');
			$this->form_validation->set_rules('contact', ' ', 'required|trim');
			$this->form_validation->set_rules('email', ' ', 'required|trim|valid_email');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);
			} else {

				$updateData = array(
					'company_name' => $this->input->post('company_name'),
					'pan_vat' => $this->input->post('pan_vat'),
					'address' => $this->input->post('address'),
					'contact' => $this->input->post('contact'),
					'email' => $this->input->post('email'),
					'service_charge' => $this->input->post('service_charge'),
					'vat_charge' => $this->input->post('vat_charge'),
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s')
				);

				$tableName = "company_detail"; // db table name
				$updateCompanyDetail = $this->common_model->update($id = 1, $updateData, $tableName);
				$redirectUrl = "inventory/companyDetail"; //redirect url

				if ($updateCompanyDetail) {
					$message = 'Company detail has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to update company detail.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
			}
		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * All the list of supplier is displayed.
	 * Management of supplier is done in this page.
	 *
	 * @return void
	 */
	public function supplier()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 18;
		self::$viewData['title'] = "Supplier";
		self::$viewData['breadcrumb'] = "Supplier"; 
		self::$viewData['heading'] = "Supplier List";
		// getting supplier list from supplier table
		self::$viewData['supplierList'] = $this->common_model->getData($table="supplier"); 
		self::$viewData['page'] = "inventory/supplier/list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * Form for adding new supplier is loaded.
	 * Validation and insert functionality is also carried out within this function.
	 *
	 * @return void
	 */
	public function addSupplier()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Add Supplier";
		self::$viewData['breadcrumb'] = "Add Supplier"; 
		self::$viewData['heading'] = "Add Supplier Form"; 
		self::$viewData['page'] = "inventory/supplier/add";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('supplier_name', ' ', 'required|trim');
			$this->form_validation->set_rules('address', ' ', 'required|trim');
			$this->form_validation->set_rules('contact', ' ', 'required|trim');
			$this->form_validation->set_rules('email', ' ', 'trim|valid_email|is_unique[supplier.email]',
				array('is_unique' => 'This email already exists.')
			);
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);
			} else {
				$supplier_name = $this->input->post('supplier_name');
				$address = $this->input->post('address');
				$pan_vat = $this->input->post('pan_vat');
				$contact = $this->input->post('contact');
				$email = $this->input->post('email');

				$insertData = array(
					'name' => $supplier_name,
					'address' => $address,
					'pan_vat' => $pan_vat,
					'contact' => $contact,
					'email' => $email,
					'created_by' => session_data('user_id')
				);

				$tableName = "supplier"; // db table name
				$insertProductCategoryOne = $this->common_model->insert($insertData, $tableName);
				$redirectUrl = 'inventory/supplier'; //redirect url

				if ($insertProductCategoryOne) {
					$message = 'Supplier has been added.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to add supplier.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}

			}
		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Form for updating existing supplier is loaded.
	 * Validation and update functionality is also carried out within this function.
	 *
	 * @param int $categoryId
	 * @return void
	 */
	public function editSupplier($supplierId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Edit Supplier";
		self::$viewData['breadcrumb'] = "Edit Supplier"; 
		self::$viewData['heading'] = "Edit Supplier Form";  
		// Getting supplier detail by Id
		self::$viewData['detail'] = $this->common_model->getDataById($supplierId, $tableName = "supplier");
		self::$viewData['page'] = "inventory/supplier/edit";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('supplier_name', ' ', 'required|trim');
			$this->form_validation->set_rules('address', ' ', 'required|trim');
			$this->form_validation->set_rules('contact', ' ', 'required|trim');
			$this->form_validation->set_rules('email', ' ', 'trim|valid_email');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);
			} else {
				$supplier_name = $this->input->post('supplier_name');
				$address = $this->input->post('address');
				$pan_vat = $this->input->post('pan_vat');
				$contact = $this->input->post('contact');
				$email = $this->input->post('email');

				$updateData = array(
					'name' => $supplier_name,
					'address' => $address,
					'pan_vat' => $pan_vat,
					'contact' => $contact,
					'email' => $email,
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s')
				);

				$tableName = "supplier"; // db table name
				$updateSupplier = $this->common_model->update($supplierId, $updateData, $tableName);
				$redirectUrl = 'inventory/editSupplier/'.$supplierId; //redirect url

				if ($updateSupplier) {
					$message = 'Supplier has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to update supplier.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
			}
		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Delete status is updated from 0 to 1.
	 * Soft delete concept is implemented.
	 *
	 * @param int $category_id
	 * @return void
	 */
	public function deleteSupplier($supplierId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		$tableName = "supplier"; // db table name
		$softDelete = $this->common_model->softDelete($supplierId, $tableName);
		$redirectUrl = 'inventory/supplier'; //redirect url

		if ($softDelete) {
			$message = 'Supplier has been deleted.';
			$this->session->set_flashdata('success_msg', $message);
			redirect($redirectUrl); 

		} else {
			$message = 'Failed to delete supplier.';
			$this->session->set_flashdata('error_msg', $message);
			redirect($redirectUrl); 
		}
	}

	/**
	 * All the list of product is displayed.
	 * Management of product is done in this page.
	 *
	 * @return void
	 */
	public function product_old_no_datatables()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 16;
		self::$viewData['title'] = "Product";
		self::$viewData['breadcrumb'] = "Product"; 
		self::$viewData['heading'] = "Product List";
		// getting product list from product table
		self::$viewData['productList'] = $this->inventory_model->getProduct(); // join query 
		self::$viewData['page'] = "inventory/product/list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * All the list of product is displayed.
	 * Management of product is done in this page.
	 * Here, server-side datatables is implemented for displaying product list.
	 *
	 * @return void
	 */
	public function product()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 16;
		self::$viewData['title'] = "Product";
		self::$viewData['breadcrumb'] = "Product"; 
		self::$viewData['heading'] = "Product List";
		self::$viewData['page'] = "inventory/product/list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * This function is called through ajax request from product/list view page.
	 * All the product data is fetched, and the required data for server-side datatable is sent as response through $output json data.
	 *
	 * @return void
	 */
	public function fetchProducts()
	{
		try {
			if ($this->input->is_ajax_request()) {	
				// product data fetched
				$fetchData = $this->inventory_model->makeProductDatatable();
				$data = array();

				// preparing data to send as response and display in the table list
				if ($fetchData) {
					foreach ($fetchData as $key => $row) {
						$subArray = array();
						$subArray[] = '';
						$subArray[] = "<button type='button' data-href='".base_url('inventory/ajaxViewProductDetail/'.$row->id)."' class='btn btn-info btn-sm' id='view_product' data-rel='tooltip' data-placement='top' title='View Product Detail' data-toggle='modal' data-target='#formModal'><span class='fas fa-eye' style='color: white'></span></button>&nbsp;".(((session_data('is_super_admin') == 1 || editAccess($dashboardMenuId=16) == 1)) ? "<a href='".base_url('inventory/editProduct/'.$row->id)."' class='btn btn-warning btn-sm' data-toggle='tooltip' data-placement='top' title='Edit'><span class='fa fa-edit' style='color: white'></span></a>&nbsp" : ''). (((session_data('is_super_admin') == 1 || editAccess($dashboardMenuId=16) == 1)) ? "<a href='".base_url('inventory/deleteProduct/'.$row->id)."' class='btn btn-danger btn-sm' data-toggle='tooltip' data-placement='top' title='Delete' onClick='return confirm_delete()' ><span class='fa fa-trash' style='color: white'></span></a>": '');
						$subArray[] = $row->supplier_name;
						$subArray[] = $row->category_name;
						$subArray[] = $row->sku;
						$subArray[] = $row->unit_price;
						$subArray[] = $row->stock_quantity;
						$subArray[] = $row->arrival_date;
						$data[] = $subArray;
					}
				}

				// json data sent as response
				$output = array(
					"draw" => intval($_POST["draw"]),
					"recordsTotal" => $this->inventory_model->countAllProductData(),
					"recordsFiltered" => $this->inventory_model->countProductFilteredData(),
					"data" => $data  
				);
				echo json_encode($output);

			} else {
				exit('No direct script access allowed');
			}
		} catch (Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	/**
	 * Form for adding new product is loaded.
	 * Validation and insert functionality is also carried out within this function.
	 *
	 * @return void
	 */
	public function addProduct()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Add Product";
		self::$viewData['breadcrumb'] = "Add Product"; 
		self::$viewData['heading'] = "Add Product Form"; 
		self::$viewData['page'] = "inventory/product/add";

		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * This function is invoked through ajax request from product/add.php.
	 * Form is displayed according to entry_medium selected.
	 * The form view pages are located in product/ajax_view/$.
	 *
	 * @return void
	 */
	public function ajaxGetAddProductForm()
	{
		try {
			if ($this->input->is_ajax_request()) {	
				// getting supplier list from supplier table
				self::$viewData['supplierList'] = $this->common_model->getData($table="supplier"); 
				// getting product category list from product_category_three table
				self::$viewData['productCategoryList'] = $this->common_model->getData($table="product_category_three"); 

				$entry_medium = $this->input->post('entry_medium');

				if ($entry_medium === "excel") {	
					$viewPage = $this->load->view('inventory/product/ajax_view/add_excel', self::$viewData, TRUE);
				} elseif ($entry_medium === "manual") {	
					$viewPage = $this->load->view('inventory/product/ajax_view/add_manual', self::$viewData, TRUE);
				}

				$response = array(
					'status' => 'success',
					'message' => 'Add product form.',
					'data' => $viewPage
				);

				header("Content-type: application/json");
				echo json_encode($response);

			} else {
				exit('No direct script access allowed');
			}
		} catch (Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	/**
	 * This function is invoked through ajax request while uploading product data through excel file.	
	 * First, form validation is carried out, followed by inserting of data into db table.
	 *
	 * @return void
	 */
	public function addProductByExcel()
	{
		try {
			if ($this->input->is_ajax_request()) {
                $csrf_name = $this->security->get_csrf_token_name(); // get csrf name
                $csrf_value = $this->security->get_csrf_hash(); // get new csrf value

				$this->form_validation->set_rules('supplier', ' ', 'required|trim');
				$this->form_validation->set_rules('product_category', ' ', 'required|trim');
				$this->form_validation->set_rules('arrival_date', ' ', 'required|trim');

				if (empty($_FILES['excel_file']['name'])) {
					$this->form_validation->set_rules('excel_file', ' ', 'required');
				}

				// validation check
				if ($this->form_validation->run() == false) {
    				$formErrors = array();

					foreach ($_POST as $key => $value) {
						$errMsg = form_error($key);
						if(!empty($errMsg)) {
							$formErrors[] = array(
								'id' => $key,
								'message' => $errMsg
							);
						}
					}

					$response = array (
						'status' => 'error',
						'data' => $formErrors,
						'message' => 'form_error',
                        "csrf_name" => $csrf_name,
                        "csrf_value" => $csrf_value
					); 
					
					header("Content-type: application/json");
					echo json_encode($response);exit;
    			} else {
					//getting all the post data from form
					$postData = $this->input->post();

					//EXCEL IMPORT STARTS HERE
					include APPPATH.'/libraries/PHPExcel/Classes/PHPExcel.php';

					// checking file extension 
					$ext = pathinfo($_FILES['excel_file']['name'], PATHINFO_EXTENSION);
					$allowed_ext = array('xls', 'xlsx', 'xlsb', 'xlsm', 'csv');
					
					if( !in_array($ext, $allowed_ext) ) {
						$response = array(
							'status' => 'error',
							'message' => 'file_error',
							'field_id' => 'excel_file',
							'error_msg' => "Extensions allowed: 'csv', 'xls', 'xlsx', 'xlsb', 'xlsm'.",
							"csrf_name" => $csrf_name,
							"csrf_value" => $csrf_value
						);

						header("Content-type: application/json");
						echo json_encode($response); exit;
					}

					// excel file configuration
					$inputFileName = $_FILES['excel_file']['tmp_name'];
					$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
					$objPHPExcelReader = PHPExcel_IOFactory::createReader($inputFileType);
					$objPHPExcelReader->setReadDataOnly(true);
					$objPHPExcel = $objPHPExcelReader->load($inputFileName);
					$objWorksheet = $objPHPExcel->getActiveSheet();

					$highestRow = $objWorksheet->getHighestRow();
					$highestColumn = $objWorksheet->getHighestColumn();
					$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
					$rows = array();

					// database column names 
					$db_col = array('sku', 'product_name', 'product_image', 'size', 'color', 'unit_price', 'stock_quantity', 'packaging_type');

					// preparing data for insert 
					for ($row = 1; $row <= $highestRow; ++$row) {
						for ($col = 0; $col <= $highestColumnIndex-1; ++$col) {
							$row_count = $row;

							if ($row != 1) {
								$row_count--;
							}
							
							$rows[$row_count]['supplier_id'] = $postData['supplier'];
							$rows[$row_count]['product_category_id'] = $postData['product_category'];
							$rows[$row_count]['arrival_date'] = $postData['arrival_date'];
							$rows[$row_count]['entry_medium'] = $postData['entry_medium'];
							$rows[$row_count]['created_by'] = session_data('user_id');
							$rows[$row_count][$db_col[$col]] = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
						}
					}

					if (count($rows) > 1){
						// inserting data 
						$insert_batch = $this->db->insert_batch('product', $rows);

						if ($insert_batch) {
							$response = array(
								'status' => 'success',
								'message' => 'Product added successfully.',
								'url' => base_url('inventory/product'),
								"csrf_name" => $csrf_name,
								"csrf_value" => $csrf_value
							);

							$message = 'Product added successfully.';
							$this->session->set_flashdata('success_msg', $message);

						} else {
							$response = array(
								'status' => 'error',
								'message' => 'insert_error',
								'url' => base_url('inventory/addProduct'),
								"csrf_name" => $csrf_name,
								"csrf_value" => $csrf_value
							);
	
							$message = 'Failed to add product.';
							$this->session->set_flashdata('error_msg', $message);
						}

					} else {
						$response = array(
							'status' => 'error',
							'message' => 'insert_error',
							'url' => base_url('inventory/addProduct'),
							"csrf_name" => $csrf_name,
							"csrf_value" => $csrf_value
						);

						$message = 'Failed to add product.';
						$this->session->set_flashdata('error_msg', $message);
					}
					//EXCEL IMPORT ENDS HERE HERE
				}
				header("Content-type: application/json");
				echo json_encode($response);

			} else {
				exit('No direct script access allowed');
			}
		} catch (Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}	
	}

	/**
	 * This function is invoked through ajax request while uploading product data manually.	
	 * First, form validation is carried out, followed by inserting of data into db table.
	 *
	 * @return void
	 */
	public function addProductByManual()
	{
		try {
			if ($this->input->is_ajax_request()) {
                $csrf_name = $this->security->get_csrf_token_name(); // get csrf name
                $csrf_value = $this->security->get_csrf_hash(); // get new csrf value

				$this->form_validation->set_rules('supplier', ' ', 'required|trim');
				$this->form_validation->set_rules('product_category', ' ', 'required|trim');
				$this->form_validation->set_rules('sku', ' ', 'required|trim|is_unique[product.sku]',
					array('is_unique' => 'The entered SKU already exists.')
				);
				$this->form_validation->set_rules('unit_price', ' ', 'required|trim');
				$this->form_validation->set_rules('quantity', ' ', 'required|trim');
				$this->form_validation->set_rules('arrival_date', ' ', 'required|trim');

				// validation check 
				if ($this->form_validation->run() == false) {
    				$formErrors = array();

					foreach ($_POST as $key => $value) {
						$errMsg = form_error($key);
						if(!empty($errMsg)) {
							$formErrors[] = array(
								'id' => $key,
								'message' => $errMsg
							);
						}
					}

					$response = array (
						'status' => 'error',
						'data' => $formErrors,
						'message' => 'form_error',
                        "csrf_name" => $csrf_name,
                        "csrf_value" => $csrf_value
					); 

					header("Content-type: application/json");
					echo json_encode($response);exit;

    			} else {
					//getting all the post data from form
					$postData = $this->input->post();

					//Image upload starts here
					if ($_FILES['product_image']['name']) {
						$imagePath = IMG_UPL_PATH."products"; // image path defined
						if (!is_dir($imagePath)) {
							@mkdir($imagePath, 0777, TRUE);
						} 

						$config['upload_path'] = $imagePath;
						$config['allowed_types'] = 'jpeg|jpg|png';
						// $config['overwrite'] = TRUE;
						$config['max_size'] = 10000000;
						$config['max_width'] = null;
						$config['max_height'] = null;
						$config['file_ext_tolower'] = true;

						$this->load->library('upload', $config);
						$this->upload->initialize($config); 

						if ( !$this->upload->do_upload('product_image') ) {
							$imgError = strip_tags($this->upload->display_errors());
							
							$response = array(
								'status' => 'error',
								'message' => 'file_error',
								'field_id' => 'product_image',
								'error_msg' => $imgError,
								"csrf_name" => $csrf_name,
								"csrf_value" => $csrf_value
							);
							header("Content-type: application/json");
							echo json_encode($response);exit;
				
						} else {
							$uploadData = $this->upload->data();
							$imageFinalPath = $imagePath.'/'.$uploadData['file_name'];
						}
					} else {
						$imageFinalPath = null;
					}
					//Image upload ends here
			
					$insertData = array(
						'supplier_id' => $postData['supplier'],
						'product_category_id' => $postData['product_category'],
						'sku' => $postData['sku'],
						'product_name' => $postData['product_name'],
						'product_image' => $imageFinalPath,
						'size' => $postData['size'],
						'color' => $postData['color'],
						'unit_price' => $postData['unit_price'],
						'stock_quantity' => $postData['quantity'],
						'packaging_type' => $postData['packaging_type'],
						'arrival_date' => $postData['arrival_date'],
						'entry_medium' => $postData['entry_medium'],
						'created_by' => session_data('user_id')
					);

					$insertProduct = $this->common_model->insert($insertData, $tableName="product");
					if ($insertProduct) {
						$response = array(
							'status' => 'success',
							'message' => 'Product added successfully.',
							'url' => base_url('inventory/product'),
							"csrf_name" => $csrf_name,
							"csrf_value" => $csrf_value
						);
						$message = 'Product added successfully.';
						$this->session->set_flashdata('success_msg', $message);

					} else {
						$response = array(
							'status' => 'error',
							'message' => 'insert_error',
							'url' => base_url('inventory/addProduct'),
							"csrf_name" => $csrf_name,
							"csrf_value" => $csrf_value
						);
						$message = 'Failed to add product.';
						$this->session->set_flashdata('error_msg', $message);
					}
					
				}
				header("Content-type: application/json");
				echo json_encode($response);

			} else {
				exit('No direct script access allowed');
			}
		} catch (Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}	
	}

	/**
	 * Existing product is edited.
	 *
	 * @param int $productId
	 * @return void
	 */
	public function editProduct($productId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Edit Product";
		self::$viewData['breadcrumb'] = "Edit Product"; 
		self::$viewData['heading'] = "Edit Product Form";  
		// Getting product detail by Id
		self::$viewData['detail'] = $this->common_model->getDataById($productId, $tableName = "product");
		// getting supplier list from supplier table
		self::$viewData['supplierList'] = $this->common_model->getData($table="supplier"); 
		// getting product category list from product_category_three table
		self::$viewData['productCategoryList'] = $this->common_model->getData($table="product_category_three"); 
		self::$viewData['page'] = "inventory/product/edit";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			// callback function validation
			$this->form_validation->CI =& $this; 
			$this->form_validation->set_rules('supplier', ' ', 'required|trim');
			$this->form_validation->set_rules('product_category', ' ', 'required|trim');
			$this->form_validation->set_rules('sku', ' ', 'required|trim|callback_productsku_check');
			$this->form_validation->set_rules('unit_price', ' ', 'required|trim');
			$this->form_validation->set_rules('quantity', ' ', 'required|trim');
			$this->form_validation->set_rules('arrival_date', ' ', 'required|trim');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			// validation check 
			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);
			} else {
				//getting all the post data from form
				$postData = $this->input->post();
				$redirectUrl = "inventory/editProduct/".$productId; // redirect url

				//Image upload starts here
				if ($_FILES['product_image']['name']) {
					$imagePath = IMG_UPL_PATH."products"; // image path defined
					if (!is_dir($imagePath)) {
						@mkdir($imagePath, 0777, TRUE);
					} 

					$config['upload_path'] = $imagePath;
					$config['allowed_types'] = 'jpeg|jpg|png';
					// $config['overwrite'] = TRUE;
					$config['max_size'] = 10000000;
					$config['max_width'] = null;
					$config['max_height'] = null;
					$config['file_ext_tolower'] = true;

					$this->load->library('upload', $config);
					$this->upload->initialize($config); 

					if ( !$this->upload->do_upload('product_image') ) {
						$imgError = strip_tags($this->upload->display_errors());
						$this->session->set_flashdata('image_error', $imgError);
						redirect($redirectUrl); 
			
					} else {
						if ($postData['product_image']) {
							@unlink($postData['product_image']);
						}
						$uploadData = $this->upload->data();
						$imageFinalPath = $imagePath.'/'.$uploadData['file_name'];
					}
				} else {
					$imageFinalPath = $postData['product_image'];
				}
				//Image upload ends here

				$updateData = array(
					'supplier_id' => $postData['supplier'],
					'product_category_id' => $postData['product_category'],
					'sku' => $postData['sku'],
					'product_name' => $postData['product_name'],
					'product_image' => ($imageFinalPath) ? $imageFinalPath : null,
					'size' => $postData['size'],
					'color' => $postData['color'],
					'unit_price' => $postData['unit_price'],
					'stock_quantity' => $postData['quantity'],
					'packaging_type' => $postData['packaging_type'],
					'arrival_date' => $postData['arrival_date'],
					'entry_medium' => $postData['entry_medium'],
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s')
				);

				$updateProduct = $this->common_model->update($productId, $updateData, $tableName="product");

				if ($updateProduct) {
					$message = 'Product has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to update product.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
			}
		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * This is callback function for checking if entered product sku already exists or not.
	 *
	 * @return void
	 */
	public function productsku_check()
	{
		$sku = $this->input->post('sku');
		$productId = $this->input->post('product_id');

		// If sku is empty
		if (!$sku) {
			$this->form_validation->set_message('productsku_check', 'The field is required.');
		   	return FALSE;
		}

		$checkProductSku = $this->inventory_model->checkProductSku($productId, $sku);
	 
		// check if product sku exists
		if ($checkProductSku) {
		   $this->form_validation->set_message('productsku_check', 'The entered SKU already exists.');
		   return FALSE;
		   
		} else {
			return TRUE;
		}
	}
	
	/**
	 * Delete status is updated from 0 to 1.
	 * Soft delete concept is implemented.
	 *
	 * @param int $productId
	 * @return void
	 */
	public function deleteProduct($productId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		$softDelete = $this->common_model->softDelete($productId, $tableName="product");
		$redirectUrl = 'inventory/product'; //redirect url

		if ($softDelete) {
			$message = 'Product has been deleted.';
			$this->session->set_flashdata('success_msg', $message);
			redirect($redirectUrl); 

		} else {
			$message = 'Failed to delete product.';
			$this->session->set_flashdata('error_msg', $message);
			redirect($redirectUrl); 
		}
	}

	/**
	 * This function is invoked through ajax request.
	 * Detail of specific existing product is displayed.
	 *
	 * @param int $productId
	 * @return void
	 */
	public function ajaxViewProductDetail($productId)
	{
		try {
			if ($this->input->is_ajax_request()) {

				self::$viewData['detail'] = $this->inventory_model->getProductById($productId); 
				$viewPage = $this->load->view('inventory/product/ajax_view/product_detail', self::$viewData, TRUE);

				$response = array(
					'status' => 'success',
					'message' => 'Product Detail.',
					'title' => 'Product Detail',
					'data' => $viewPage
				);

				header("Content-type: application/json");
				echo json_encode($response);

			} else {
				exit('No direct script access allowed');
			}

		} catch (Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}	
	}

	/**
	 * List of exisiting orders is displayed.
	 *
	 * @return void
	 */
	public function wholesaleOrders()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 26;
		self::$viewData['title'] = "Manage Wholesale Orders";
		self::$viewData['breadcrumb'] = "Manage Wholesale Orders"; 
		self::$viewData['heading'] = "Wholesale Orders List";
		// get all order list 
		self::$viewData['orderList'] = $this->inventory_model->getWholesaleOrderDetails(); // join query
		self::$viewData['page'] = "inventory/orders/wholesale/list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * First the form validation is done, followed by inserting of orders and order items detail.
	 * The overall process of insert is carried in model function i.e. 'createOrder'.
	 *
	 * @return void
	 */
	public function addWholesaleOrder()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Add Wholesale Order";
		self::$viewData['breadcrumb'] = "Add Wholesale Order"; 
		self::$viewData['heading'] = "Add Wholesale Order Form";
		// Getting all wholesale customer list
		self::$viewData['customerList'] = $this->inventory_model->getAllWholesaleCustomerList();
		// Getting all product list
		self::$viewData['productList'] = $this->inventory_model->getAllProductList();
		// Getting company detail
		self::$viewData['companyDetail'] = $this->inventory_model->getCompanyDetail();
		// Getting last order id
		self::$viewData['lastOrderNo'] = $this->inventory_model->getLastWholesaleOrderNo();
		self::$viewData['page'] = "inventory/orders/wholesale/add";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('customer', ' ', 'required|trim');
			$this->form_validation->set_rules('order_date', ' ', 'required|trim');
			$this->form_validation->set_rules('order_time', ' ', 'required|trim');
			$this->form_validation->set_rules('delivery_date', ' ', 'required|trim');
			$this->form_validation->set_rules('product[]', 'Product', 'trim|required');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			// validation check 
			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);
			} else {
				$orderId = $this->inventory_model->createWholesaleOrder();
        	
				if ($orderId) {
					$message = 'Order has been created.';
					$this->session->set_flashdata('success_msg', $message);
					redirect('inventory/editWholesaleOrder/'.$orderId, 'refresh');
				}
				else {
					$message = 'Failed to create order.';
					$this->session->set_flashdata('error_msg', $message);
					redirect('inventory/addWholesaleOrder', 'refresh');
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}
	
	/**
	 * First the form validation is done, followed by updating of orders and order items detail.
	 * The overall process of update is carried out in model function i.e. 'updateWholesaleOrder'.
	 *
	 * @return void
	 */
	public function editWholesaleOrder($orderId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Edit Wholesale Order";
		self::$viewData['breadcrumb'] = "Edit Wholesale Order"; 
		self::$viewData['heading'] = "Edit Wholesale Order Form";
		// Getting all wholesale customer list
		self::$viewData['customerList'] = $this->inventory_model->getAllWholesaleCustomerList();
		// Getting all product list
		self::$viewData['productList'] = $this->inventory_model->getAllProductList();
		// Getting company detail
		self::$viewData['companyDetail'] = $this->inventory_model->getCompanyDetail();
		// Getting order detail
		self::$viewData['orderDetail'] = $this->common_model->getDataById($orderId, $tableName = "wholesale_order");
		// Getting order items detail
		self::$viewData['orderItemsDetail'] = $this->inventory_model->getWholesaleOrderItemsDetail($orderId);
		self::$viewData['page'] = "inventory/orders/wholesale/edit";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('customer', ' ', 'required|trim');
			$this->form_validation->set_rules('order_date', ' ', 'required|trim');
			$this->form_validation->set_rules('order_time', ' ', 'required|trim');
			$this->form_validation->set_rules('delivery_date', ' ', 'required|trim');
			$this->form_validation->set_rules('product[]', 'Product', 'trim|required');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			// validation check 
			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);
			} else {
				$redirectUrl = 'inventory/editWholesaleOrder/'.$orderId;
				$updateOrder = $this->inventory_model->updateWholesaleOrder($orderId);

				if ($updateOrder) {
					$message = 'Order has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl, 'refresh');
				}
				else {
					$message = 'Failed to update order.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl, 'refresh');
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Delete status is updated from 0 to 1.
	 * Soft delete concept is implemented.
	 *
	 * @param int $orderId
	 * @return void
	 */
	public function deleteWholesaleOrder($orderId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		$softDelete = $this->common_model->softDelete($orderId, $tableName = "orders");
		$redirectUrl = 'inventory/wholesaleOrders'; //redirect url

		if ($softDelete) {
			$message = 'Order has been deleted.';
			$this->session->set_flashdata('success_msg', $message);
			redirect($redirectUrl); 

		} else {
			$message = 'Failed to delete order.';
			$this->session->set_flashdata('error_msg', $message);
			redirect($redirectUrl); 
		}
	}

	/**
	 * List of exisiting orders is displayed.
	 *
	 * @return void
	 */
	public function retailOrders()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 34;
		self::$viewData['title'] = "Manage Retail Orders";
		self::$viewData['breadcrumb'] = "Manage Retail Orders"; 
		self::$viewData['heading'] = "Retail Orders List";
		// get all order list 
		self::$viewData['orderList'] = $this->inventory_model->getRetailOrderDetails(); // join query
		self::$viewData['page'] = "inventory/orders/retail/list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * First the form validation is done, followed by inserting of orders and order items detail.
	 * The overall process of insert is carried in model function i.e. 'createOrder'.
	 *
	 * @return void
	 */
	public function addRetailOrder()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Add Retail Order";
		self::$viewData['breadcrumb'] = "Add Retail Order"; 
		self::$viewData['heading'] = "Add Retail Order Form";
		// Getting all retail customer list
		self::$viewData['customerList'] = $this->common_model->getData($tableName="retail_customer");
		// Getting all product list
		self::$viewData['productList'] = $this->inventory_model->getAllProductList();
		// Getting company detail
		self::$viewData['companyDetail'] = $this->inventory_model->getCompanyDetail();
		// Getting last order id
		self::$viewData['lastOrderNo'] = $this->inventory_model->getLastRetailOrderNo();
		self::$viewData['page'] = "inventory/orders/retail/add";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('customer', ' ', 'required|trim');
			$this->form_validation->set_rules('order_date', ' ', 'required|trim');
			$this->form_validation->set_rules('order_time', ' ', 'required|trim');
			$this->form_validation->set_rules('delivery_date', ' ', 'required|trim');
			$this->form_validation->set_rules('product[]', 'Product', 'trim|required');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			// validation check 
			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);
			} else {
				$orderId = $this->inventory_model->createRetailOrder();
        	
				if ($orderId) {
					$message = 'Order has been created.';
					$this->session->set_flashdata('success_msg', $message);
					redirect('inventory/editRetailOrder/'.$orderId, 'refresh');
				}
				else {
					$message = 'Failed to create order.';
					$this->session->set_flashdata('error_msg', $message);
					redirect('inventory/addRetailOrder', 'refresh');
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * First the form validation is done, followed by updating of orders and order items detail.
	 * The overall process of update is carried out in model function i.e. 'updateRetailOrder'.
	 *
	 * @return void
	 */
	public function editRetailOrder($orderId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Edit Retail Order";
		self::$viewData['breadcrumb'] = "Edit Retail Order"; 
		self::$viewData['heading'] = "Edit Retail Order Form";
		// Getting all retail customer list
		self::$viewData['customerList'] = $this->common_model->getData($tableName="retail_customer");
		// Getting all product list
		self::$viewData['productList'] = $this->inventory_model->getAllProductList();
		// Getting company detail
		self::$viewData['companyDetail'] = $this->inventory_model->getCompanyDetail();
		// Getting order detail
		self::$viewData['orderDetail'] = $this->common_model->getDataById($orderId, $tableName="retail_order");
		// Getting order items detail
		self::$viewData['orderItemsDetail'] = $this->inventory_model->getRetailOrderItemsDetail($orderId);
		self::$viewData['page'] = "inventory/orders/retail/edit";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('customer', ' ', 'required|trim');
			$this->form_validation->set_rules('order_date', ' ', 'required|trim');
			$this->form_validation->set_rules('order_time', ' ', 'required|trim');
			$this->form_validation->set_rules('delivery_date', ' ', 'required|trim');
			$this->form_validation->set_rules('product[]', 'Product', 'trim|required');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			// validation check 
			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);
			} else {
				$redirectUrl = 'inventory/editRetailOrder/'.$orderId;
				$updateOrder = $this->inventory_model->updateRetailOrder($orderId);

				if ($updateOrder) {
					$message = 'Order has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl, 'refresh');
				}
				else {
					$message = 'Failed to update order.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl, 'refresh');
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Delete status is updated from 0 to 1.
	 * Soft delete concept is implemented.
	 *
	 * @param int $orderId
	 * @return void
	 */
	public function deleteRetailOrder($orderId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		$softDelete = $this->common_model->softDelete($orderId, $tableName = "retail_order");
		$redirectUrl = 'inventory/retailOrders'; //redirect url

		if ($softDelete) {
			$message = 'Order has been deleted.';
			$this->session->set_flashdata('success_msg', $message);
			redirect($redirectUrl); 

		} else {
			$message = 'Failed to delete order.';
			$this->session->set_flashdata('error_msg', $message);
			redirect($redirectUrl); 
		}
	}

	/**
	 * It gets the all the active product information from the product table 
	 * This function is used in the order page, for the product selection in the table
	 * The response is return on the json format.
	 *
	 * @return void
	 */
	public function ajaxGetTableProductRow()
	{
		$products = $this->inventory_model->getAllProductList();
		echo json_encode($products);
	}

	/*
	* It gets the product id passed from the ajax method.
	* It checks retrieves the particular product data from the product id 
	* and return the data into the json format.
	*/
	public function ajaxGetProductById($productId)
	{
		if($productId) {
			$productData = $this->inventory_model->getProductById($productId);
			echo json_encode($productData);
		}
	}

	/**
	 * This function is called through ajax request.
	 * Stock quantity of product through its productId is returned as response.
	 *
	 * @return void
	 */
	public function ajaxGetProductStockQty()
	{
		$productId = $this->input->post('product_id');
		$getStockQty = $this->common_model->getDataById($productId, $tableName = "product");
		$stockQty = array(
			'stock_quantity' => $getStockQty->stock_quantity
		);
		echo json_encode($stockQty);
	}
}