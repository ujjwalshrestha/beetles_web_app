<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Product category two data is fetched.
	 * Delete status of both joined tables should be 0 i.e. not deleted.
	 *
	 * @return array
	 */
	public function getProductCategoryTwo()
	{
		$where = array(
			'pct.delete_status' => 0,
			'pco.delete_status' => 0
		);

		$result = $this->db->select('pct.*, pco.category_name as prod_cat_one')
						   ->from('product_category_two as pct')
						   ->join('product_category_one as pco', 'pct.prod_cat_one_id = pco.id')
						   ->where($where)
						   ->get();

		if ($result->num_rows() > 0) {
			return $result->result();
		} else {
			return false;
		}
	}

	/**
	 * Product category three data is fetched.
	 * Delete status of all three joined tables should be 0 i.e. not deleted.
	 *
	 * @return void
	 */
	public function getProductCategoryThree()
	{
		$where = array(
			'pcth.delete_status' => 0,
			'pct.delete_status' => 0,
			'pco.delete_status' => 0,
		);

		$result = $this->db->select('pcth.*, pct.category_name as prod_cat_two')
						   ->from('product_category_three as pcth')
						   ->join('product_category_two as pct', 'pcth.prod_cat_two_id = pct.id')
						   ->join('product_category_one as pco', 'pct.prod_cat_one_id = pco.id')
						   ->where($where)
						   ->get();

		if ($result->num_rows() > 0) {
			return $result->result();
		} else {
			return false;
		}
	}

	/**
	 * Company detail is fetched.
	 *
	 * @param int $id
	 * @return void
	 */
	public function getCompanyDetail()
	{
		$where = array(
			'id' => 1
		);

		$result = $this->db->get_where('company_detail', $where);
		if ($result->num_rows() == 1) {
			return $result->row();
		} else {
			return false;
		}
	}

	/**
	 * All the product data with delete status 0 is fetched.
	 * Supplier and Product_category_three table is joined.
	 *
	 * @return array
	 */
	public function getProduct()
	{
		$where = array(
			'p.delete_status' => 0
		);

		$result = $this->db->select('p.*, s.name as supplier_name, c.category_name')
						   ->from('product as p')
						   ->join('supplier as s', 'p.supplier_id = s.id')
						   ->join('product_category_three as c', 'p.product_category_id = c.id')
						   ->where($where)
						   ->order_by('p.created_at', 'desc')
						   ->get();

		if ($result->num_rows() > 0) {
			return $result->result();
		} else {
			return false;
		}
	}

	/**
	 * Product detail is fetched through product id.
	 * Supplier and Product_category_three table is joined.
	 *
	 * @param int $productId
	 * @return array
	 */
	public function getProductById($productId)
	{
		$where = array(
			'p.id' => $productId
		);

		$result = $this->db->select('p.*, s.name as supplier_name, c.category_name')
						   ->from('product as p')
						   ->join('supplier as s', 'p.supplier_id = s.id')
						   ->join('product_category_three as c', 'p.product_category_id = c.id')
						   ->where($where)
						   ->get();

		if ($result->num_rows() === 1) {
			return $result->row();
		} else {
			return false;
		}
	}

	/**
	 * Query for displaying product data is being prepared in this function.
	 * This is used for server-side datatable.
	 *
	 * @return void
	 */
	public function makeProductQuery()
	{
		// columns in which order functionality is not required is set to null. A null is compulsory at the end.
		$order_columns = array(null, null, 's.name', 'c.category_name', 'p.sku', 'p.unit_price', 'p.stock_quantity', 'p.arrival_date', null);
		
		$where = array(
			'p.delete_status' => 0
		);

		$this->db->select('p.*, s.name as supplier_name, c.category_name')
				 ->from('product as p')
				 ->join('supplier as s', 'p.supplier_id = s.id')
				 ->join('product_category_three as c', 'p.product_category_id = c.id')
				 ->where($where);

		if (isset($_POST['search']['value'])) {
			// group_start and group_end method is required while using 'where' and 'like' condition the same time 
            $this->db->group_start();
			$this->db->like('s.name', $_POST['search']['value'])
					 ->or_like('c.category_name', $_POST['search']['value'])
					 ->or_like('p.sku', $_POST['search']['value'])
					 ->or_like('p.unit_price', $_POST['search']['value'])
					 ->or_like('p.stock_quantity', $_POST['search']['value'])
					 ->or_like('p.arrival_date', $_POST['search']['value']);
			$this->db->group_end();
		}

		if (isset($_POST['order'])) {
			$this->db->order_by($order_columns[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else {
			$this->db->order_by('p.id', 'desc');
		}
	}

	/**
	 * Fetching final data of product.
	 *
	 * @return array
	 */
	public function makeProductDatatable()
	{
		// calling function product query
		$this->makeProductQuery();

		// setting limit 
		if ($_POST["length"] != -1) {
			$this->db->limit($_POST["length"], $_POST["start"]);
		}

		$result = $this->db->get();
		return $result->result();
	}

	/**
	 * Fetch overall no. of rows from product table
	 *
	 * @return int
	 */
	public function countProductFilteredData()
	{
		// calling function product query
		$this->makeProductQuery();
		$result = $this->db->get();
		return $result->num_rows();
	}

	/**
	 * Fetch overall no. of results from product table
	 *
	 * @return int
	 */
	public function countAllProductData()
	{
		$where = array(
			'delete_status' => 0
		);

		$this->db->select('*')
				 ->from('product')
				 ->where('delete_status', 0);
		return $this->db->count_all_results();
	}

	/**
	 * Get all the product list where delete_status is 0 and stock_quantity is not 0.
	 *
	 * @return array
	 */
	public function getAllProductList()
	{
		$where = array(
			'delete_status' => 0 
		);

		$this->db->where_not_in('stock_quantity', 0);
		$query = $this->db->select('id, sku, product_name, unit_price, stock_quantity')
						  ->from('product')
						  ->where($where)
						  ->order_by('id', 'desc')
						  ->get();
				
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	/**
	 * Checks existence of product sku.
	 *
	 * @param int $productId
	 * @param string $sku
	 * @return array
	 */
	public function checkProductSku($productId, $sku)
	{
		$where = array(
			'sku' => $sku
		);

		$this->db->where_not_in('id', $productId);
		$query = $this->db->get_where('product', $where);

		if ($query->num_rows() == 1) {
			return $query->row();
		} else {
			return false;
		}	
	}

	/**
	 * Gets all customer list where delete_status in auth_user table is 0.
	 *
	 * @return void
	 */
	public function getAllWholesaleCustomerList()
	{
		$where = array(
			'au.delete_status' => 0 
		);

		$query = $this->db->select('c.*')
						  ->from('wholesale_customer as c')
						  ->join('auth_user as au', 'c.user_id = au.id')
						  ->where($where)
						  ->get();
				
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	/**
	 * Gets last order id from 'wholesale_order' table.
	 *
	 * @return int
	 */
	public function getLastWholesaleOrderNo()
	{
		$query = $this->db->order_by('id', 'desc')
						  ->limit(1)
						  ->get('wholesale_order');

		if ($query->num_rows() == 1) {
			return $query->row()->order_no;
		} else {
			return false;
		}
	}

	/**
	 * First the order detail is inserted.
	 * Then, the order items (products) are inserted using loop.
	 * The ID acquired from inserted order detail is used as foreign key for wholesale_order_item.
	 * After inserting order items, no of stock quantity from product is reduced with the quantity no of products ordered.  
	 *
	 * @return int
	 */
	public function createWholesaleOrder()
	{
    	$data = array(
			'order_no' => $this->input->post('order_no'),
    		'customer_id' => $this->input->post('customer'),
    		'order_datetime' => $this->input->post('order_date').' '.$this->input->post('order_time'),
    		'delivery_date' => $this->input->post('delivery_date'),
    		'special_instruction' => $this->input->post('special_instruction'),
    		'gross_amount' => $this->input->post('gross_amount'),
    		'service_charge_rate' => $this->input->post('service_charge_rate'),
    		'service_charge' => ($this->input->post('service_charge') > 0) ? $this->input->post('service_charge') : 0,
    		'vat_charge_rate' => $this->input->post('vat_charge_rate'),
    		'vat_charge' => ($this->input->post('vat_charge') > 0) ? $this->input->post('vat_charge') : 0,
    		'net_amount' => $this->input->post('net_amount'),
    		'discount_rate' => $this->input->post('discount_rate'),
    		'discount' => $this->input->post('discount'),
    		'order_status' => 0,
    		'created_by' => session_data('user_id')
    	);

		$insert = $this->db->insert('wholesale_order', $data);
		$orderId = $this->db->insert_id(); // getting last inserted ID from orders table
		$productId = $this->input->post('product'); // arrays of product data
		$quantity = $this->input->post('qty'); // arrays of quantity data

		$countProduct = count($productId);
    	for($i = 0; $i < $countProduct; $i++) {
    		$items = array(
    			'order_id' => $orderId,
    			'product_id' => $productId[$i],
    			'quantity' => $quantity[$i],
    			'rate' => $this->input->post('rate')[$i],
    			'total_amount' => $this->input->post('amount')[$i]
    		);

    		$this->db->insert('wholesale_order_item', $items);

    		// now decrease the stock from the product
    		$productData = $this->common_model->getDataById($productId[$i], $tableName = 'product');
    		$stockQuantity = (int) $productData->stock_quantity - (int) $quantity[$i];

    		$updateProductData = array(
				'stock_quantity' => $stockQuantity
			);

    		$this->common_model->update($productId[$i], $updateProductData, $tableName = 'product'); 
    	}

		return ($orderId) ? $orderId : false;
	}

	/**
	 * Wholesale Order details along with customer detail is fetched.
	 *
	 * @return array
	 */
	public function getWholesaleOrderDetails()
	{
		$where = array(
			'o.delete_status' => 0
		);

		$query = $this->db->select('c.name as customer_name, c.contact as customer_contact, o.*')
				 	  	  ->from('wholesale_order as o')
						  ->join('wholesale_customer as c', 'o.customer_id = c.user_id')
						  ->where($where)
						  ->order_by('o.id', 'desc')
						  ->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	/**
	 * No. of wholesale order items within a specific order is counted.
	 *
	 * @param int $orderId
	 * @return int
	 */
	public function countWholesaleOrderItem($orderId)
	{
		if($orderId) {
			$where = array(
				'order_id' => $orderId
			);
			
			$query = $this->db->get_where('wholesale_order_item', $where);
			return $query->num_rows();
		}
	}

	/**
	 * Details of wholesale order items within a specific order is fetched.
	 *
	 * @param int $orderId
	 * @return array
	 */
	public function getWholesaleOrderItemsDetail($orderId)
	{
		$where = array(
			'order_id' => $orderId
		);

		$query = $this->db->get_where('wholesale_order_item', $where);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	/**
	 * First the wholesale order detail is updated.
	 * Then the stock quantity of products in 'product' table is added with the quantity of order_items.
	 * The, whole order_items of respective order is removed.
	 * Then, the order items (products) are inserted again using loop.
	 * After inserting order items, no of stock quantity from product is reduced with the quantity no of products ordered.  
	 *
	 * @param int $orderId
	 * @return boolean
	 */
	public function updateWholesaleOrder($orderId)
	{
		// first order data 
    	$updateOrderData = array(
    		'customer_id' => $this->input->post('customer'),
    		'order_datetime' => $this->input->post('order_date').' '.$this->input->post('order_time'),
    		'delivery_date' => $this->input->post('delivery_date'),
    		'special_instruction' => $this->input->post('special_instruction'),
    		'gross_amount' => $this->input->post('gross_amount'),
    		'service_charge_rate' => $this->input->post('service_charge_rate'),
    		'service_charge' => ($this->input->post('service_charge') > 0) ? $this->input->post('service_charge') : 0,
    		'vat_charge_rate' => $this->input->post('vat_charge_rate'),
    		'vat_charge' => ($this->input->post('vat_charge') > 0) ? $this->input->post('vat_charge') : 0,
    		'net_amount' => $this->input->post('net_amount'),
    		'discount_rate' => $this->input->post('discount_rate'),
    		'discount' => $this->input->post('discount'),
    		'remarks' => $this->input->post('remarks'),
    		'order_status' => $this->input->post('order_status'),
    		'dispatched_date' => $this->input->post('dispatched_date'),
			'updated_by' => session_data('user_id'),
			'updated_at' => date('Y-m-d H:i:s')
    	);
		// update order data 
		$this->db->where('id', $orderId);
		$this->db->update('wholesale_order', $updateOrderData);

		// now the order item 
		// first we will replace the product qty to original and subtract the qty again
		$orderItemsDetail = $this->getWholesaleOrderItemsDetail($orderId);
		foreach ($orderItemsDetail as $key => $item) {
			$productId = $item->product_id;
			$quantity = $item->quantity;
			// get product data 
			$productData = $this->common_model->getDataById($productId, $tableName = "product");
			$addQuantity = $quantity + $productData->stock_quantity;

			$updateProductQuantity = array(
				'stock_quantity' => $addQuantity
			);
			$this->common_model->update($productId, $updateProductQuantity, $tableName = "product");
		}

		// now remove the order item data 
		$this->db->where('order_id', $orderId);
		$this->db->delete('wholesale_order_item');

		// now decrease the product quantity
		$product = $this->input->post('product');
		$qty = $this->input->post('qty');
		
		$countProduct = count($product);
		for($i = 0; $i < $countProduct; $i++) {
			$insertItems = array(
    			'order_id' => $orderId,
    			'product_id' => $product[$i],
    			'quantity' => $qty[$i],
    			'rate' => $this->input->post('rate')[$i],
    			'total_amount' => $this->input->post('amount')[$i]
			);
			$this->db->insert('wholesale_order_item', $insertItems);

			// now decrease the stock from the product
    		$productData = $this->common_model->getDataById($product[$i], $tableName = 'product');
    		$stockQuantity = (int) $productData->stock_quantity - (int) $qty[$i];

    		$updateProductData = array(
				'stock_quantity' => $stockQuantity
			);

    		$this->common_model->update($product[$i], $updateProductData, $tableName = 'product'); 
		}

		return true;
	}

	/**
	 * Retail Order details along with customer detail is fetched.
	 *
	 * @return array
	 */
	public function getRetailOrderDetails()
	{
		$where = array(
			'o.delete_status' => 0
		);

		$query = $this->db->select('c.name as customer_name, c.contact as customer_contact, o.*')
				 	  	  ->from('retail_order as o')
						  ->join('retail_customer as c', 'o.customer_id = c.id')
						  ->where($where)
						  ->order_by('o.id', 'desc')
						  ->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	/**
	 * Gets last order id from 'retail_order' table.
	 *
	 * @return int
	 */
	public function getLastRetailOrderNo()
	{
		$query = $this->db->order_by('id', 'desc')
						  ->limit(1)
						  ->get('retail_order');

		if ($query->num_rows() == 1) {
			return $query->row()->order_no;
		} else {
			return false;
		}
	}

	/**
	 * First the order detail is inserted.
	 * Then, the order items (products) are inserted using loop.
	 * The ID acquired from inserted order detail is used as foreign key for retail_order_item.
	 * After inserting order items, no of stock quantity from product is reduced with the quantity no of products ordered.  
	 *
	 * @return int
	 */
	public function createRetailOrder()
	{
    	$data = array(
			'order_no' => $this->input->post('order_no'),
    		'customer_id' => $this->input->post('customer'),
    		'order_datetime' => $this->input->post('order_date').' '.$this->input->post('order_time'),
    		'delivery_date' => $this->input->post('delivery_date'),
    		'special_instruction' => $this->input->post('special_instruction'),
    		'gross_amount' => $this->input->post('gross_amount'),
    		'service_charge_rate' => $this->input->post('service_charge_rate'),
    		'service_charge' => ($this->input->post('service_charge') > 0) ? $this->input->post('service_charge') : 0,
    		'vat_charge_rate' => $this->input->post('vat_charge_rate'),
    		'vat_charge' => ($this->input->post('vat_charge') > 0) ? $this->input->post('vat_charge') : 0,
    		'net_amount' => $this->input->post('net_amount'),
    		'discount_rate' => $this->input->post('discount_rate'),
    		'discount' => $this->input->post('discount'),
    		'order_status' => 0,
    		'created_by' => session_data('user_id')
    	);

		$insert = $this->db->insert('retail_order', $data);
		$orderId = $this->db->insert_id(); // getting last inserted ID from orders table
		$productId = $this->input->post('product'); // arrays of product data
		$quantity = $this->input->post('qty'); // arrays of quantity data

		$countProduct = count($productId);
    	for($i = 0; $i < $countProduct; $i++) {
    		$items = array(
    			'order_id' => $orderId,
    			'product_id' => $productId[$i],
    			'quantity' => $quantity[$i],
    			'rate' => $this->input->post('rate')[$i],
    			'total_amount' => $this->input->post('amount')[$i]
    		);

    		$this->db->insert('retail_order_item', $items);

    		// now decrease the stock from the product
    		$productData = $this->common_model->getDataById($productId[$i], $tableName = 'product');
    		$stockQuantity = (int) $productData->stock_quantity - (int) $quantity[$i];

    		$updateProductData = array(
				'stock_quantity' => $stockQuantity
			);

    		$this->common_model->update($productId[$i], $updateProductData, $tableName = 'product'); 
    	}

		return ($orderId) ? $orderId : false;
	}

	/**
	 * No. of retail order items within a specific order is counted.
	 *
	 * @param int $orderId
	 * @return int
	 */
	public function countRetailOrderItem($orderId)
	{
		if($orderId) {
			$where = array(
				'order_id' => $orderId
			);
			
			$query = $this->db->get_where('retail_order_item', $where);
			return $query->num_rows();
		}
	}

	/**
	 * Details of retail order items within a specific order is fetched.
	 *
	 * @param int $orderId
	 * @return array
	 */
	public function getRetailOrderItemsDetail($orderId)
	{
		$where = array(
			'order_id' => $orderId
		);

		$query = $this->db->get_where('retail_order_item', $where);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	

	/**
	 * First the retail order detail is updated.
	 * Then the stock quantity of products in 'product' table is added with the quantity of order_items.
	 * The, whole order_items of respective order is removed.
	 * Then, the order items (products) are inserted again using loop.
	 * After inserting order items, no of stock quantity from product is reduced with the quantity no of products ordered.  
	 *
	 * @param int $orderId
	 * @return boolean
	 */
	public function updateRetailOrder($orderId)
	{
		// first order data 
    	$updateOrderData = array(
    		'customer_id' => $this->input->post('customer'),
    		'order_datetime' => $this->input->post('order_date').' '.$this->input->post('order_time'),
    		'delivery_date' => $this->input->post('delivery_date'),
    		'special_instruction' => $this->input->post('special_instruction'),
    		'gross_amount' => $this->input->post('gross_amount'),
    		'service_charge_rate' => $this->input->post('service_charge_rate'),
    		'service_charge' => ($this->input->post('service_charge') > 0) ? $this->input->post('service_charge') : 0,
    		'vat_charge_rate' => $this->input->post('vat_charge_rate'),
    		'vat_charge' => ($this->input->post('vat_charge') > 0) ? $this->input->post('vat_charge') : 0,
    		'net_amount' => $this->input->post('net_amount'),
    		'discount_rate' => $this->input->post('discount_rate'),
    		'discount' => $this->input->post('discount'),
    		'remarks' => $this->input->post('remarks'),
    		'order_status' => $this->input->post('order_status'),
    		'dispatched_date' => $this->input->post('dispatched_date'),
			'updated_by' => session_data('user_id'),
			'updated_at' => date('Y-m-d H:i:s')
    	);
		// update order data 
		$this->db->where('id', $orderId);
		$this->db->update('retail_order', $updateOrderData);

		// now the order item 
		// first we will replace the product qty to original and subtract the qty again
		$orderItemsDetail = $this->getRetailOrderItemsDetail($orderId);
		foreach ($orderItemsDetail as $key => $item) {
			$productId = $item->product_id;
			$quantity = $item->quantity;
			// get product data 
			$productData = $this->common_model->getDataById($productId, $tableName = "product");
			$addQuantity = $quantity + $productData->stock_quantity;

			$updateProductQuantity = array(
				'stock_quantity' => $addQuantity
			);
			$this->common_model->update($productId, $updateProductQuantity, $tableName = "product");
		}

		// now remove the order item data 
		$this->db->where('order_id', $orderId);
		$this->db->delete('retail_order_item');

		// now decrease the product quantity
		$product = $this->input->post('product');
		$qty = $this->input->post('qty');
		
		$countProduct = count($product);
		for($i = 0; $i < $countProduct; $i++) {
			$insertItems = array(
    			'order_id' => $orderId,
    			'product_id' => $product[$i],
    			'quantity' => $qty[$i],
    			'rate' => $this->input->post('rate')[$i],
    			'total_amount' => $this->input->post('amount')[$i]
			);
			$this->db->insert('retail_order_item', $insertItems);

			// now decrease the stock from the product
    		$productData = $this->common_model->getDataById($product[$i], $tableName = 'product');
    		$stockQuantity = (int) $productData->stock_quantity - (int) $qty[$i];

    		$updateProductData = array(
				'stock_quantity' => $stockQuantity
			);

    		$this->common_model->update($product[$i], $updateProductData, $tableName = 'product'); 
		}

		return true;
	}
}