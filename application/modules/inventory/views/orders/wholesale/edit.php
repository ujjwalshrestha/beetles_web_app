<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">

        <?php
            $action = base_url('inventory/editWholesaleOrder/'.$orderDetail->id);
            $attributes = array(
                "id" => "edit_order_form", 
                "name" => "edit_order_form",
                "method" => "POST"
            );

            echo form_open($action, $attributes); 
        ?>
        
        <div class="col-md-12 card-body">

            <h4><?php echo $heading ?> <a href="<?php echo base_url('inventory/wholesaleOrders') ?>" class="btn btn-success btn-sm float-right" data-toggle="tooltip" data-placement="top" title="Back to List"><span class="fa fa-arrow-left"></span></a></h4><hr>

            <?php if($this->session->flashdata('error_msg')): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                </div>
            <?php endif; ?>

            <?php if($this->session->flashdata('success_msg')): ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                </div>
            <?php endif; ?>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="order_no">Order No</label>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" id="order_no" name="order_no" value="<?php echo $orderDetail->order_no ?>" readonly>
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">
                    <label for="customer">Select Customer <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-3">
                    <select class="form-control select2" name="customer" id="customer">
                        <option value="">--- Select Customer ---</option>
                        <?php if($customerList): ?>
                            <?php foreach($customerList as $key => $list): ?>
                                <option value="<?php echo $list->user_id ?>" <?php echo ($list->user_id == $orderDetail->customer_id) ? 'selected' : ''; ?>><?php echo ucfirst($list->name) ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <span class="error-msg"></span>
                    <?php echo form_error('customer'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="order_date">Order Date <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-3">
                    <input type="date" class="form-control" id="order_date" name="order_date" value="<?php echo date('Y-m-d', strtotime($orderDetail->order_datetime)) ?>">
                    <span class="error-msg"></span>
                    <?php echo form_error('order_date'); ?>
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">
                    <label for="order_time">Order Time <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-3">
                    <input type="time" step="1" class="form-control" id="order_time" name="order_time" value="<?php echo date('H:i:s', strtotime($orderDetail->order_datetime)) ?>">
                    <span class="error-msg"></span>
                    <?php echo form_error('order_time'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="delivery_date">Delivery Date <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-3">
                    <input type="date" class="form-control" id="delivery_date" name="delivery_date" value="<?php echo $orderDetail->delivery_date ?>">
                    <span class="error-msg"></span>
                    <?php echo form_error('delivery_date'); ?>
                </div>

                <div class="col-md-1"></div>
                
                <div class="col-md-2">
                    <label for="special_instruction">Special Instruction</label>
                </div>
                <div class="col-md-3">
                    <textarea type="text" class="form-control" id="special_instruction" name="special_instruction" placeholder="Any special instruction from customer?" rows="4" cols="50"><?php echo $orderDetail->special_instruction ?></textarea>
                </div>
            </div><br><hr>


            <?php echo form_error('product[]'); ?>
            <table class="table table-bordered" id="product_info_table">
                <thead>
                    <tr>
                        <th style="width:40%">Product</th>
                        <th style="width:15%">Quantity</th>
                        <th style="width:15%">Unit Price</th>
                        <th style="width:20%">Amount</th>
                        <th class="text-center" style="width:10%"><button type="button" id="add_row" class="btn btn-success btn-sm" title="Add Row"><i class="fas fa-plus"></i></button></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($orderItemsDetail): ?>
                        <?php $x = 1; ?>
                        <?php foreach($orderItemsDetail as $key => $items): ?>
                        <tr id="row_<?php echo $x; ?>">
                            <td>
                                <select class="form-control select2 product" data-row-id="row_<?php echo $x; ?>" id="product_<?php echo $x; ?>" name="product[]" style="width:100%;" onchange="getProductData(<?php echo $x; ?>)" >
                                    <option value="">--- Select Product ---</option>
                                    <?php foreach ($productList as $key => $product): ?>
                                    <option value="<?php echo $product['id'] ?>" <?php echo ($product['id'] === $items->product_id) ? 'selected' : ''; ?> ><?php echo $product['sku'].' ('.$product['stock_quantity'].')'; ?></option>
                                    <?php endforeach ?>
                                </select>
                                <span class="error-msg"></span>
                            </td>
                            <td><input type="number" name="qty[]" id="qty_<?php echo $x; ?>" class="form-control" value="<?php echo $items->quantity ?>" onkeyup="getTotal(<?php echo $x; ?>)">
                            <span class="error-msg"></span></td>
                            <td><input type="text" name="rate[]" id="rate_<?php echo $x; ?>" class="form-control" value="<?php echo $items->rate ?>" readonly autocomplete="off"></td>
                            <td>
                            <input type="text" name="amount[]" id="amount_<?php echo $x; ?>" class="form-control" value="<?php echo $items->total_amount ?>" readonly autocomplete="off"></td>
                            <td class="text-center"><button type="button" class="btn btn-danger btn-sm" onclick="removeRow('<?php echo $x; ?>')" title="Remove Row"><i class="fas fa-times"></i></button></td>
                        </tr>
                        <?php $x++; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table><hr><br><br>

            <div class="form-group row">
                <div class="col-md-7"></div>
                <div class="col-md-2 text-right">
                    <label for="gross_amount">Gross Amount</label>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" id="gross_amount" name="gross_amount" value="<?php echo $orderDetail->gross_amount ?>" readonly autocomplete="off">
                </div>
            </div>

            <?php if ($companyDetail->service_charge > 0): ?>
            <div class="form-group row">
                <div class="col-md-7"></div>
                <div class="col-md-2 text-right">
                    <label for="service_charge">Service Charge (<?php echo $companyDetail->service_charge ?>%)</label>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" id="service_charge" name="service_charge" value="<?php echo $orderDetail->service_charge ?>" readonly autocomplete="off">
                    <input type="hidden" name="service_charge_rate" value="<?php echo $companyDetail->service_charge ?>" autocomplete="off">
                </div>
            </div>
            <?php endif; ?>

            <?php if ($companyDetail->vat_charge > 0): ?>
            <div class="form-group row">
                <div class="col-md-7"></div>
                <div class="col-md-2 text-right">
                    <label for="vat_charge">Vat Charge (<?php echo $companyDetail->vat_charge ?>%)</label>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" id="vat_charge" name="vat_charge" value="<?php echo $orderDetail->vat_charge ?>" readonly autocomplete="off">
                    <input type="hidden" name="vat_charge_rate" value="<?php echo $companyDetail->vat_charge ?>" autocomplete="off">
                </div>
            </div>
            <?php endif; ?>
            
            <!-- <div class="form-group row">
                <div class="col-md-7"></div>
                <div class="col-md-2 text-right">
                    <label for="delivery_charge">Delivery Charge</label>
                </div>
                <div class="col-md-3">
                      <input type="text" class="form-control" id="delivery_charge" name="delivery_charge" placeholder="Delivery charge" onkeyup="subAmount()" autocomplete="off">
                </div>
            </div>  -->
            
            <div class="form-group row">
                <div class="col-md-7"></div>
                <div class="col-md-2 text-right">
                    <label for="discount_rate">Promo Discount (%)</label>
                </div>
                <div class="col-md-3">
                        <input type="text" class="form-control" id="discount_rate" name="discount_rate" placeholder="Discount in %" value="<?php echo $orderDetail->discount_rate ?>" onkeyup="subAmount()" autocomplete="off">
                        <input type="hidden" name="discount" id="discount" autocomplete="off" value="<?php echo $orderDetail->discount ?>">
                </div>
            </div>  

            <div class="form-group row">
                <div class="col-md-7"></div>
                <div class="col-md-2 text-right">
                    <label for="net_amount">Net Amount</label>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" id="net_amount" name="net_amount" value="<?php echo $orderDetail->net_amount ?>" readonly autocomplete="off">
                </div>
            </div>
            <hr>

            <div class="form-group row">
                <div class="col-md-7"></div>
                <div class="col-md-2 text-right">
                    <label for="dispatched_date">Dispatched Date</label>
                </div>
                <div class="col-md-3">
                    <input type="date" class="form-control" id="dispatched_date" name="dispatched_date" value="<?php echo $orderDetail->dispatched_date ?>">
                    <?php echo form_error('dispatched_date'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-7"></div>
                <div class="col-md-2 text-right">
                    <label for="order_status">Order Status</label>
                </div>
                <div class="col-md-3">
                    <select class="form-control" name="order_status" id="order_status">
                        <option value="1" <?php echo ($orderDetail->order_status == 1) ? 'selected' : ''; ?> >Complete</option>
                        <option value="0" <?php echo ($orderDetail->order_status == 0) ? 'selected' : ''; ?> >Not Complete</option>
                    </select>
                    <?php echo form_error('order_status'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-7"></div>
                <div class="col-md-2 text-right">
                    <label for="remarks">Remarks</label>
                </div>
                <div class="col-md-3">
                    <textarea type="text" class="form-control" id="remarks" name="remarks" placeholder="Enter Remarks..." rows="4" cols="50"><?php echo $orderDetail->remarks ?></textarea>
                    <?php echo form_error('remarks'); ?>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-warning">Edit Order</button>
        </div>

        <?php echo form_close() ?>
    </div>
    <!-- /.card -->
</div>

<script>
    $(document).ready(function (){
        $("#edit_order_form").validate({
            rules: {
                'customer': 'required',
                'order_date': 'required',
                'order_time': 'required',
                'delivery_date': 'required',
                'product[]': 'required',
                'qty[]': {
                    'min': 1
                }
            },
            errorPlacement: function(error, element) {
                error.appendTo( element.siblings("span.error-msg") );
            }
        });
    })
    
    const base_url = "<?php echo base_url(); ?>";

    // Add new row in the table 
    $("#add_row").unbind('click').bind('click', function() {
        const table = $("#product_info_table");
        let count_table_tbody_tr = $("#product_info_table tbody tr").length;
        let row_id = count_table_tbody_tr + 1;

        $.ajax({
            url: base_url + 'inventory/ajaxGetTableProductRow',
            type: 'post',
            dataType: 'json',
            success:function(response) {  
                // console.log(reponse.x);
                let html = '<tr id="row_'+row_id+'">'+
                    '<td>'+ 
                        '<select class="form-control select2 product" data-row-id="'+row_id+'" id="product_'+row_id+'" name="product[]" style="width:100%;" onchange="getProductData('+row_id+')">'+
                            '<option value="">--- Select Product ---</option>';
                            $.each(response, function(index, value) {
                                html += '<option value="'+value.id+'">'+value.sku+' ('+value.stock_quantity+')'+'</option>';              
                            });
                            
                        html += '</select>'+
                        '<span class="error-msg"></span>'+
                        '</td>'+ 
                        '<td><input type="number" name="qty[]" id="qty_'+row_id+'" class="form-control" onkeyup="getTotal('+row_id+')"><span class="error-msg"></span></td>'+
                        '<td><input type="text" name="rate[]" id="rate_'+row_id+'" class="form-control" readonly>'+
                        '<td><input type="text" name="amount[]" id="amount_'+row_id+'" class="form-control" readonly>'+
                        '<td class="text-center"><button type="button" class="btn btn-danger btn-sm" onclick="removeRow(\''+row_id+'\')"><i class="fas fa-times"></i></button></td>'+
                        '</tr>';

                if(count_table_tbody_tr >= 1) {
                    $("#product_info_table tbody tr:last").after(html);  
                }
                else {
                    $("#product_info_table tbody").html(html);
                }

                $(".product").select2();
            }
        });
        return false;
    });

    // get total amount by multiplying rate (unit price) and quantity
    function getTotal(row = null) {
        if(row) {
            let product_id = $("#product_"+row).val();
            let qty = Number($("#qty_"+row).val());
            let rate = Number($("#rate_"+row).val());

            // ajax call to check entered quantity value with the stock quantity of respective product  
            $.ajax({
                url : base_url + 'inventory/ajaxGetProductStockQty',
                dataType: 'json',
                data : {'product_id':product_id},
                type : "POST",
                success: function(resp) {
                    $("#qty_"+row).siblings("span.error-msg").text('');
                    
                    // check if entered quantity is greater than respective product's stock quantity 
                    if (qty > resp.stock_quantity) {
                        $("#qty_"+row).siblings("span.error-msg").text('Product\'s stock quantity exceeded.').css('color', 'red');

                    // check if quantity field is empty 
                    } else if (qty == '') {
                        $("#qty_"+row).siblings("span.error-msg").text('This field is required.').css('color', 'red');
                    
                    } else {
                        let total = rate * qty;
                        total = total.toFixed(2);
                        $("#amount_"+row).val(total);
                        subAmount();
                    }
                },
                error: function() {
                    alert('Internal Server Error!');
                }
            });

        } else {
            alert('No row found! Please refresh the page.');
        }
    }

    // get the product information from the server
    function getProductData(row_id)
    {
        let product_id = $("#product_"+row_id).val();    
        if(product_id == "") {
            $("#rate_"+row_id).val("");
            $("#qty_"+row_id).val("");    
            $("#amount_"+row_id).val("");

        } else {
            $.ajax({
                url: base_url + 'inventory/ajaxGetProductById/' + product_id,
                type: 'get',
                dataType: 'json',
                success:function(resp) {
                    // setting the rate value into the rate input field
                    $("#rate_"+row_id).val(resp.unit_price);
                    $("#qty_"+row_id).val(1); // initial value of quantity as 1

                    let total = Number(resp.unit_price) * 1;
                    total = total.toFixed(2);
                    $("#amount_"+row_id).val(total);
                    
                    subAmount();
                } // /success
            }); // /ajax function to fetch the product data 
        }
    }

    // calculate the total amount of the order
    function subAmount() {
        let service_charge = <?php echo ($companyDetail->service_charge > 0) ? $companyDetail->service_charge : 0; ?>;
        let vat_charge = <?php echo ($companyDetail->vat_charge > 0) ? $companyDetail->vat_charge : 0; ?>;

        let tableProductLength = $("#product_info_table tbody tr").length;
        let totalSubAmount = 0;
        for (i = 0; i < tableProductLength; i++) {
            let tr = $("#product_info_table tbody tr")[i];
            let count = $(tr).attr('id');
            count = count.substring(4);

            totalSubAmount = Number(totalSubAmount) + Number($("#amount_"+count).val());
        } // /for

        totalSubAmount = totalSubAmount.toFixed(2);

        // sub total
        $("#gross_amount").val(totalSubAmount);

        // vat
        let vat = (Number($("#gross_amount").val()) / 100) * vat_charge;
        vat = vat.toFixed(2);
        $("#vat_charge").val(vat);

        // service
        let service = (Number($("#gross_amount").val()) / 100) * service_charge;
        service = service.toFixed(2);
        $("#service_charge").val(service);

        // delivery charge
        // let delivery_charge = $("#delivery_charge").val();
        
        // total amount
        let totalAmount = (Number(totalSubAmount) + Number(vat) + Number(service));
        totalAmount = totalAmount.toFixed(2);

        // discount
        let discount_rate = $("#discount_rate").val();
        let discount = (Number($("#gross_amount").val()) / 100) * discount_rate;
        discount = discount.toFixed(2);
        $("#discount").val(discount);
        if (discount_rate) {
            let grandTotal = Number(totalAmount) - Number(discount);
            grandTotal = grandTotal.toFixed(2);
            $("#net_amount").val(grandTotal);

        } else {
            $("#net_amount").val(totalAmount);
        } 
    } // /sub total amount

    function removeRow(tr_id) {
        $("#product_info_table tbody tr#row_"+tr_id).remove();
        subAmount();
    }

</script>