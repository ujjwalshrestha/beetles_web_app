<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">
        
        <div class="col-md-12 card-body" style="overflow-x: scroll;">

            <h4><?php echo $heading ?> <?php if (session_data('is_super_admin') == 1 || addAccess($dashboardMenuId) == 1): ?><a href="<?php echo base_url('inventory/addRetailOrder') ?>" class="btn btn-success btn-sm float-right"><span class="fa fa-plus"></span> Add New</a><?php endif; ?></h4><hr>

            <?php if($this->session->flashdata('error_msg')): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                </div>
            <?php endif; ?>

            <?php if($this->session->flashdata('success_msg')): ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                </div>
            <?php endif; ?>

            <table class="table table-hover table-bordered" id="data_table_active">
                <thead>
                    <tr>
                        <th class="text-center">Action</th>
                        <th>Order No</th>
                        <th>Customer Name</th>
                        <th>Customer Contact No.</th>
                        <th>Order Date-Time</th>
                        <th>Total Order Items</th>
                        <th>Total Amount</th>
                        <th>Delivery Date</th>
                        <th>Order Status</th>
                    </tr>                                        
                </thead>
                <tbody>
                <?php if ($orderList): ?>
                    <?php foreach ($orderList as $key => $list):  ?>
                        <tr>
                            <td class="text-center" nowrap>
                                <?php if (session_data('is_super_admin') == 1 || editAccess($dashboardMenuId) == 1): ?>
                                    <a href="<?php echo base_url('inventory/editRetailOrder/'.$list->id) ?>" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Edit"><span class="fa fa-edit" style="color: white"></span></a>&nbsp;   
                                <?php endif; ?>

                                <?php if (session_data('is_super_admin') == 1 || deleteAccess($dashboardMenuId) == 1): ?>
                                    <a href="<?php echo base_url('inventory/deleteRetailOrder/'.$list->id) ?>" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete" onClick="return confirm('Are you sure to delete this data?')" ><span class="fa fa-trash" style="color: white"></span></a>  
                                <?php endif; ?>

                                <?php if (session_data('is_super_admin') == 0 && (deleteAccess($dashboardMenuId) == 0 && editAccess($dashboardMenuId) == 0)): ?>
                                    <span>-</span>
                                <?php endif; ?>
                            </td>
                            <td class="text-center text-bold" nowrap><?php echo $list->order_no ?></td>
                            <td nowrap><?php echo $list->customer_name ?></td>
                            <td nowrap><?php echo $list->customer_contact ?></td>
                            <td nowrap><?php echo $list->order_datetime ?></td>
                            <td class="text-center" nowrap><?php echo $this->inventory_model->countRetailOrderItem($list->id) ?></td>
                            <td nowrap><?php echo $list->net_amount ?></td>
                            <td nowrap><?php echo $list->delivery_date ?></td>
                            <td nowrap><?php echo ($list->order_status == 1) ? '<span class="badge badge-success">Complete</span>' : '<span class="badge badge-danger">Not Complete</span>' ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.card -->
</div>