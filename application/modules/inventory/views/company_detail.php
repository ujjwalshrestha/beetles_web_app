<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">

        <?php
            $action = base_url('inventory/companyDetail');
            $attributes = array(
                "id" => "company_detail_form", 
                "name" => "company_detail_form",
                "method" => "POST"
            );

            echo form_open($action, $attributes); 
        ?>
        
        <div class="col-md-12 card-body">

            <h4><?php echo $heading ?></h4><hr>

            <div class="col-md-6">
                <?php if($this->session->flashdata('error_msg')): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                    </div>
                <?php endif; ?>

                <?php if($this->session->flashdata('success_msg')): ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="company_name">Company Name <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Enter Company Name" value="<?php echo $detail->company_name ?>">
                    <?php echo form_error('company_name'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="pan_vat">PAN/VAT No <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="pan_vat" name="pan_vat" placeholder="Enter PAN/VAT No" value="<?php echo $detail->pan_vat ?>">
                    <?php echo form_error('pan_vat'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="address">Address <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address" value="<?php echo $detail->address ?>">
                    <?php echo form_error('address'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="contact">Contact <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="contact" name="contact" placeholder="Enter Contact" value="<?php echo $detail->contact ?>">
                    <?php echo form_error('contact'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="email">Email <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" value="<?php echo $detail->email ?>">
                    <?php echo form_error('email'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="service_charge">Service Charge (%)</label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="service_charge" name="service_charge" placeholder="Enter Service Charge (%)" value="<?php echo $detail->service_charge ?>">
                    <?php echo form_error('service_charge'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="vat_charge">VAT Charge (%)</label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="vat_charge" name="vat_charge" placeholder="Enter VAT Charge (%)" value="<?php echo $detail->vat_charge ?>">
                    <?php echo form_error('vat_charge'); ?>
                </div>
            </div>
            
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-warning">Update</button>
        </div>

        <?php echo form_close() ?>
    </div>
    <!-- /.card -->

</div>