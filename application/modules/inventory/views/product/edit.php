<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">

        <?php
            $action = base_url("inventory/editProduct/".$detail->id); // form action will show on change event of entry_method
            $attributes = array(
                "id" => "edit_product_form", 
                "name" => "edit_product_form",
                "method" => "POST"
            );

            echo form_open_multipart($action, $attributes); 
        ?>
        
        <div class="col-md-12 card-body">

            <h4><?php echo $heading ?> <a href="<?php echo base_url('inventory/product') ?>" class="btn btn-success btn-sm float-right" data-toggle="tooltip" data-placement="top" title="Back to List"><span class="fa fa-arrow-left"></span></a></h4><hr>

            <div class="col-md-7">
                <?php if($this->session->flashdata('error_msg')): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                    </div>
                <?php endif; ?>

                <?php if($this->session->flashdata('success_msg')): ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="supplier">Select Supplier <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <select class="form-control select2" name="supplier" id="supplier">
                        <option value="">--- Select Supplier ---</option>
                        <?php if($supplierList): ?>
                            <?php foreach($supplierList as $key => $list): ?>
                                <option value="<?php echo $list->id ?>" <?php echo ($list->id === $detail->supplier_id) ? 'selected' : ''; ?>><?php echo ucfirst($list->name) ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <?php echo form_error('supplier'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="product_category">Select Product Category <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <select class="form-control select2" name="product_category" id="product_category">
                        <option value="">--- Select Product Category ---</option>
                        <?php if($productCategoryList): ?>
                            <?php foreach($productCategoryList as $key => $list): ?>
                                <option value="<?php echo $list->id ?>" <?php echo ($list->id === $detail->product_category_id) ? 'selected' : ''; ?>><?php echo ucfirst($list->category_name) ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <?php echo form_error('product_category'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="sku">SKU <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="sku" name="sku" placeholder="Enter SKU" value="<?php echo $detail->sku ?>">
                    <?php echo form_error('sku'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="product_name">Product Name</label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Enter Product Name" value="<?php echo $detail->product_name ?>">
                    <?php echo form_error('product_name'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="product_image">Product Image</label>
                </div>
                <div class="col-md-4">
                    <input type="file" class="form-control" id="product_image" name="product_image">
                    <input type="hidden" id="product_image" name="product_image" value="<?php echo $detail->product_image ?>">
                    <?php if($this->session->flashdata('image_error')): ?>
                        <span class="text-danger text-sm"><?php echo $this->session->flashdata('image_error'); ?></span><br>
                    <?php endif; ?>

                    <!-- Show image if available -->
                    <?php if($detail->product_image): ?>
                        <!-- If the image string has 'http', it means the links is inserted, else image path is uploaded -->
                        <a href="<?php echo (preg_match("/(http:|https:)/i", $detail->product_image)) ? $detail->product_image : base_url($detail->product_image) ?>" target="_blank">View Image</a>
                    <?php endif; ?>
                    <?php echo form_error('product_image'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="size">Size</label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="size" name="size" placeholder="Enter Size" value="<?php echo $detail->size ?>">
                    <?php echo form_error('size'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="color">Color</label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="color" name="color" placeholder="Enter Color" value="<?php echo $detail->color ?>">
                    <?php echo form_error('color'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="unit_price">Unit Price <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="unit_price" name="unit_price" placeholder="Enter Unit Price" value="<?php echo $detail->unit_price ?>">
                    <?php echo form_error('unit_price'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="quantity">Quantity <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Enter Quantity" value="<?php echo $detail->stock_quantity ?>">
                    <?php echo form_error('quantity'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="packaging_type">Packaging Type</label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="packaging_type" name="packaging_type" placeholder="Enter Packaging Type" value="<?php echo $detail->packaging_type ?>">
                    <?php echo form_error('packaging_type'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="arrival_date">Arrival Date <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="date" class="form-control" id="arrival_date" name="arrival_date" placeholder="Enter Arrival Date" value="<?php echo $detail->arrival_date ?>">
                    <?php echo form_error('arrival_date'); ?>
                </div>
            </div>
            
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <!-- this product_id is for callback function to check SKU  -->
            <input type="hidden" name="product_id" value="<?php echo $detail->id ?>">
            <button type="submit" class="btn btn-warning" id="add_btn">Update</button>
        </div>

        <?php echo form_close() ?>
    </div>
    <!-- /.card -->

</div>