<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="formModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->

<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">
        
        <div class="col-md-12 card-body" style="overflow-x: scroll;">

            <h4><?php echo $heading ?> <?php if (session_data('is_super_admin') == 1 || addAccess($dashboardMenuId) == 1): ?><a href="<?php echo base_url('inventory/addProduct') ?>" class="btn btn-success btn-sm float-right"><span class="fa fa-plus"></span> Add New</a><?php endif; ?></h4><hr>

            <?php if($this->session->flashdata('error_msg')): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                </div>
            <?php endif; ?>

            <?php if($this->session->flashdata('success_msg')): ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                </div>
            <?php endif; ?>

            <table class="table table-hover table-bordered" id="product_datatable">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th class="text-center">Action</th>
                        <th>Supplier Name</th>
                        <th>Product Category</th>
                        <th>SKU</th>
                        <th>Unit Price</th>
                        <th>Stock Quantity</th>
                        <th>Arrival Date</th>
                    </tr>                                        
                </thead>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>

<script>
    $(document).ready(function(){
        /**
         * server-side datatable for product table list
         */
        let dataTable = $("#product_datatable").DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                url: "<?php echo base_url('inventory/fetchProducts') ?>",
                type: "POST",
		        dataType: "json",
		        data:{  // csrf data (only if csrf token generation is activated)
                    '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' 
                }
            },
            "columnDefs": [
                {
                    "targets": [0, 1], // table columns from view page that are targeted. Here 0 for SN column and 1 for action column of table.
                    "orderable": false, // ordering of above mentioned columns disabled
                }
            ],
            "pageLength": 10, // initial length of page to be displayed
            "lengthMenu": [
                [10, 25, 50, 100], // -1 (last array element for displaying all data)
                [10, 25, 50, 100] // "All" (last array element for displaying all data)
            ],
        });

        /**
         * For displaying serial numbers continuously in server-side datatable
         */
        dataTable.on( 'draw.dt', function () {
            var PageInfo = $('#product_datatable').DataTable().page.info();
            dataTable.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start + '.';
            });
        });

        /**
         * Displays product detail in modal
         */
        $(document).off('click', '#view_product').on('click', '#view_product', function(e){
            e.preventDefault();
            // $('.modal-dialog').addClass('modal-lg');
            
            let obj = $(this),
                url = obj.data('href');

            $.ajax({
                url  : url,
                type : "GET",
                success: function(resp) {
                    if (resp.status == 'success') {
                        $('.modal-title').html(resp.title);
                        $('.modal-body').html(resp.data);
                    }
                },
                error: function() {
                    alert('Internal Server Error!');
                }
            });
        });

        // removes all the html data from the modal after it is closed.
        $('#formModal').on('hidden.bs.modal', function () {
            $('.modal-title').html('');
            $('.modal-body').html('');
        })
    });

    /**
     * For confirm delete. (Do this for every server-side datatable)
     *
     * @return void
     */
    function confirm_delete() {
        return confirm('Are you sure to delete this data?');
    }
</script>

