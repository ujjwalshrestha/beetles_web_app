<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="formModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->

<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">
        
        <div class="col-md-12 card-body" style="overflow-x: scroll;">

            <h4><?php echo $heading ?> <?php if (session_data('is_super_admin') == 1 || addAccess($dashboardMenuId) == 1): ?><a href="<?php echo base_url('inventory/addProduct') ?>" class="btn btn-success btn-sm float-right"><span class="fa fa-plus"></span> Add New</a><?php endif; ?></h4><hr>

            <?php if($this->session->flashdata('error_msg')): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                </div>
            <?php endif; ?>

            <?php if($this->session->flashdata('success_msg')): ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                </div>
            <?php endif; ?>

            <table class="table table-hover table-bordered" id="data_table_active">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th class="text-center">Action</th>
                        <th>Supplier Name</th>
                        <th>Product Category</th>
                        <th>SKU</th>
                        <th>Unit Price</th>
                        <th>Stock Quantity</th>
                        <th>Arrival Date</th>
                    </tr>                                        
                </thead>
                <tbody>
                    <?php if ($productList): ?>
                        <?php foreach ($productList as $key => $list):  ?>
                            <tr>
                                <td nowrap><?php echo ++$key.'.' ?></td>
                                <td class="text-center" nowrap>
                                    <button type="button" data-href="<?php echo base_url('inventory/ajaxViewProductDetail/'.$list->id) ?>" class="btn btn-info btn-sm" id="view_product" data-rel="tooltip" data-placement="top" title="View Product Detail" data-toggle="modal" data-target="#formModal"><span class="fas fa-eye" style="color: white"></span></button>&nbsp;

                                    <?php if (session_data('is_super_admin') == 1 || editAccess($dashboardMenuId) == 1): ?>
                                        <a href="<?php echo base_url('inventory/editProduct/'.$list->id) ?>" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Edit"><span class="fa fa-edit" style="color: white"></span></a>&nbsp;   
                                    <?php endif; ?>

                                    <?php if (session_data('is_super_admin') == 1 || deleteAccess($dashboardMenuId) == 1): ?>
                                        <a href="<?php echo base_url('inventory/deleteProduct/'.$list->id) ?>" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete" onClick="return confirm('Are you sure to delete this data?')" ><span class="fa fa-trash" style="color: white"></span></a>  
                                    <?php endif; ?>
                                </td>
                                <td nowrap><?php echo $list->supplier_name ?></td>
                                <td nowrap><?php echo $list->category_name ?></td>
                                <td nowrap><?php echo $list->sku ?></td>
                                <td class="text-center" nowrap><?php echo $list->unit_price ?></td>
                                <td class="text-center" nowrap><?php echo $list->stock_quantity ?></td>
                                <td nowrap><?php echo $list->arrival_date ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>

<script>
    $(document).ready(function(){
        $(document).off('click', '#view_product').on('click', '#view_product', function(e){
            e.preventDefault();
            // $('.modal-dialog').addClass('modal-lg');
            
            let obj = $(this),
                url = obj.data('href');

            $.ajax({
                url  : url,
                type : "GET",
                success: function(resp) {
                    if (resp.status == 'success') {
                        $('.modal-title').html(resp.title);
                        $('.modal-body').html(resp.data);
                    }
                },
                error: function() {
                    alert('Internal Server Error!');
                }
            });
        });

        $('#formModal').on('hidden.bs.modal', function () {
            $('.modal-title').html('');
            $('.modal-body').html('');
        })
    });
</script>