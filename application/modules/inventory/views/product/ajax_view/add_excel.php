<div class="form-group row">
    <div class="col-md-3">
        <label for="supplier">Select Supplier <span class="red-asterisk">*</span></label>
    </div>
    <div class="col-md-4">
        <select class="form-control select2" name="supplier" id="supplier">
            <option value="">--- Select Supplier ---</option>
            <?php if($supplierList): ?>
                <?php foreach($supplierList as $key => $list): ?>
                    <option value="<?php echo $list->id ?>"><?php echo ucfirst($list->name) ?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
        <span class="form-msg"></span>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3">
        <label for="product_category">Select Product Category <span class="red-asterisk">*</span></label>
    </div>
    <div class="col-md-4">
        <select class="form-control select2" name="product_category" id="product_category">
            <option value="">--- Select Product Category ---</option>
            <?php if($productCategoryList): ?>
                <?php foreach($productCategoryList as $key => $list): ?>
                    <option value="<?php echo $list->id ?>"><?php echo ucfirst($list->category_name) ?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
        <span class="form-msg"></span>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3">
        <label for="arrival_date">Arrival Date <span class="red-asterisk">*</span></label>
    </div>
    <div class="col-md-4">
        <input type="date" class="form-control" id="arrival_date" name="arrival_date" placeholder="Enter Arrival Date" >
        <span class="form-msg"></span>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3">
        <label for="excel_file">Upload Excel File <span class="red-asterisk">*</span></label>
    </div>
    <div class="col-md-4">
        <input type="file" class="form-control" id="excel_file" name="excel_file">
        <input type="hidden" name="excel_file" id ="excel_file">
        <span class="form-msg"></span>
    </div>
</div>
