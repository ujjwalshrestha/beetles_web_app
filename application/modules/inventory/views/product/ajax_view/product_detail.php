<!-- Check if product image is available -->
<?php if($detail->product_image): ?>
    <div class="row">
        <div class="col-md-12 text-center">
            <!-- If the image string has 'http', it means the links is inserted, else image path is uploaded -->
            <a href="<?php echo (preg_match("/(http:|https:)/i", $detail->product_image)) ? $detail->product_image : base_url($detail->product_image) ?>" target="_blank"><image class="img-responsive img-thumbnail" src="<?php echo (preg_match("/(http:|https:)/i", $detail->product_image)) ? $detail->product_image : base_url($detail->product_image) ?>" height="250" width="300"></image></a>
        </div>
    </div><br>
<?php endif; ?>

<div class="row">
    <div class="col-md-5">
        <label>Supplier Name</label>
    </div>
    <div class="col-md-7">
        <?php echo $detail->supplier_name ?>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <label>Product Category</label>
    </div>
    <div class="col-md-7">
        <?php echo $detail->category_name ?>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <label>SKU</label>
    </div>
    <div class="col-md-7">
        <?php echo $detail->sku ?>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <label>Product Name</label>
    </div>
    <div class="col-md-7">
        <?php echo ($detail->product_name) ? $detail->product_name : '-' ?>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <label>Size</label>
    </div>
    <div class="col-md-7">
        <?php echo ($detail->size) ? $detail->size : '-' ?>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <label>Color</label>
    </div>
    <div class="col-md-7">
        <?php echo ($detail->color) ? $detail->color : '-' ?>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <label>Unit Price</label>
    </div>
    <div class="col-md-7">
        <?php echo $detail->unit_price ?>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <label>Stock Quantity</label>
    </div>
    <div class="col-md-7">
        <?php echo $detail->stock_quantity ?>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <label>Total Valuation</label>
    </div>
    <div class="col-md-7">
        <?php echo number_format((float)$detail->unit_price * $detail->stock_quantity, 2, '.', '') ?>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <label>Packaging Type</label>
    </div>
    <div class="col-md-7">
        <?php echo ($detail->packaging_type) ? $detail->packaging_type : '-' ?>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <label>Arrival Date</label>
    </div>
    <div class="col-md-7">
        <?php echo $detail->arrival_date ?>
    </div>
</div>