<div class="form-group row">
    <div class="col-md-3">
        <label for="supplier">Select Supplier <span class="red-asterisk">*</span></label>
    </div>
    <div class="col-md-4">
        <select class="form-control select2" name="supplier" id="supplier">
            <option value="">--- Select Supplier ---</option>
            <?php if($supplierList): ?>
                <?php foreach($supplierList as $key => $list): ?>
                    <option value="<?php echo $list->id ?>"><?php echo ucfirst($list->name) ?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
        <span class="form-msg"></span>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3">
        <label for="product_category">Select Product Category <span class="red-asterisk">*</span></label>
    </div>
    <div class="col-md-4">
        <select class="form-control select2" name="product_category" id="product_category">
            <option value="">--- Select Product Category ---</option>
            <?php if($productCategoryList): ?>
                <?php foreach($productCategoryList as $key => $list): ?>
                    <option value="<?php echo $list->id ?>"><?php echo ucfirst($list->category_name) ?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
        <span class="form-msg"></span>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3">
        <label for="sku">SKU <span class="red-asterisk">*</span></label>
    </div>
    <div class="col-md-4">
        <input type="text" class="form-control" id="sku" name="sku" placeholder="Enter SKU" >
        <span class="form-msg"></span>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3">
        <label for="product_name">Product Name</label>
    </div>
    <div class="col-md-4">
        <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Enter Product Name" >
        <span class="form-msg"></span>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3">
        <label for="product_image">Product Image</label>
    </div>
    <div class="col-md-4">
        <input type="file" class="form-control" id="product_image" name="product_image">
        <input type="hidden" name="product_image" id ="product_image">
        <span class="form-msg"></span>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3">
        <label for="size">Size</label>
    </div>
    <div class="col-md-4">
        <input type="text" class="form-control" id="size" name="size" placeholder="Enter Size" >
        <span class="form-msg"></span>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3">
        <label for="color">Color</label>
    </div>
    <div class="col-md-4">
        <input type="text" class="form-control" id="color" name="color" placeholder="Enter Color" >
        <span class="form-msg"></span>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3">
        <label for="unit_price">Unit Price <span class="red-asterisk">*</span></label>
    </div>
    <div class="col-md-4">
        <input type="text" class="form-control" id="unit_price" name="unit_price" placeholder="Enter Unit Price" >
        <span class="form-msg"></span>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3">
        <label for="quantity">Quantity <span class="red-asterisk">*</span></label>
    </div>
    <div class="col-md-4">
        <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Enter Quantity" >
        <span class="form-msg"></span>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3">
        <label for="packaging_type">Packaging Type</label>
    </div>
    <div class="col-md-4">
        <input type="text" class="form-control" id="packaging_type" name="packaging_type" placeholder="Enter Packaging Type" >
        <span class="form-msg"></span>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3">
        <label for="arrival_date">Arrival Date <span class="red-asterisk">*</span></label>
    </div>
    <div class="col-md-4">
        <input type="date" class="form-control" id="arrival_date" name="arrival_date" placeholder="Enter Arrival Date" >
        <span class="form-msg"></span>
    </div>
</div>
