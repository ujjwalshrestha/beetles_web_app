<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">

        <?php
            $action = '#'; // form action will show on change event of entry_method
            $attributes = array(
                "id" => "add_product_form", 
                "name" => "add_product_form",
                "method" => "POST"
            );

            echo form_open($action, $attributes); 
        ?>
        
        <div class="col-md-12 card-body">

            <h4><?php echo $heading ?> <a href="<?php echo base_url('inventory/product') ?>" class="btn btn-success btn-sm float-right" data-toggle="tooltip" data-placement="top" title="Back to List"><span class="fa fa-arrow-left"></span></a></h4><hr>

            <div class="col-md-7">
                <?php if($this->session->flashdata('error_msg')): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                    </div>
                <?php endif; ?>

                <?php if($this->session->flashdata('success_msg')): ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label>Select Entry Medium <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="excel_upload" name="entry_medium" value="excel">
                        <label for="excel_upload" class="custom-control-label">Excel Upload</label>
                    </div>

                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="manual_entry" name="entry_medium"  value="manual">
                        <label for="manual_entry" class="custom-control-label">Manual Entry</label>
                    </div>
                    <?php echo form_error('entry_medium'); ?>
                </div>
            </div>

            <span id="add_product_view">   
            </span>
            
        </div>
        <!-- /.card-body -->

        <div class="card-footer" id="add_btn_section">
            <button type="submit" class="btn btn-success" id="add_btn">Add</button>

            <button class="btn btn-success" type="button" id="loading_btn" disabled>
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                Loading...
            </button>
        </div>

        <?php echo form_close() ?>
    </div>
    <!-- /.card -->

</div>

<script>

    $(document).ready(function(){
        $(document).off("change", "input:radio[name='entry_medium']").on("change", "input:radio[name='entry_medium']", function(e){
            e.preventDefault();

            let obj = $(this), 
                url = "<?php echo base_url('inventory/ajaxGetAddProductForm') ?>",
                entry_medium = obj.val();

            $.ajax({
                url : url,
                dataType: 'json',
                data : {'entry_medium':entry_medium},
                type : "POST",
                success: function(resp) {
                    $('#add_product_view').html('');
                    $('#add_product_view').html(resp.data);
                    
                    //Initialize Select2 Elements
                    $('.select2').select2();
                    
                },
                error: function() {
                    alert('Internal Server Error!');
                }
            });

            if (this.checked && this.value == 'excel') {
                $("#add_btn_section").css("display", "block"); // show Add button
                $('#add_product_form').attr('action', "<?php echo base_url('inventory/addProductByExcel') ?>"); // change form action attribute for excel upload

            } else if(this.checked && this.value == 'manual') {
                $("#add_btn_section").css("display", "block"); // show Add button
                $('#add_product_form').attr('action', "<?php echo base_url('inventory/addProductByManual') ?>"); // change form action attribute for manual entry
            } 
        });


        $(document).off("submit", "#add_product_form").on("submit", "#add_product_form", function(e){
            e.preventDefault();

            let obj = $(this),
                action = obj.attr('action');

            let form_data = new FormData(obj[0]);

            $.ajax({
                url : action,
                dataType: 'json',
                contentType: false,
                processData: false,
                data : form_data,
                type : "POST",

                beforeSend: function(){
                    $('#add_btn').css('display', 'none');
                    $('#loading_btn').css('display', 'block');
                },
                complete: function(){
                    $('#loading_btn').css('display', 'none');
                    $('#add_btn').css('display', 'block');
                },
                success: function(resp) {
                    $("input[name="+resp.csrf_name+"]").val(resp.csrf_value); // replaces the csrf value from the hidden input field with the new one.

                    $.fn.hideError(obj); // hides the validation error

                    if (resp.status === "error") {
                        if (resp.message === 'form_error') {
                            $.each(resp.data, function(index, element) {
                                $.fn.displayError(element.id, element.message);
                            });
                            
                        } else if (resp.message === "file_error") {
                            $("#"+resp.field_id).parent().parent().addClass('has-error');
                            $('#'+resp.field_id).siblings('.form-msg').addClass('text-danger').html(resp.error_msg).show();

                        }  else if (resp.message === "insert_error") {
                            window.location = resp.url;
                        }

                    } else if (resp.status == 'success') {
                        window.location = resp.url;
                    }
                },
                error: function() {
                    alert('Internal Server Error!');
                }
            });
        });
    });

</script>