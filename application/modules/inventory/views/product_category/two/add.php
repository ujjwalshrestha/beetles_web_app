<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">

        <?php
            $action = base_url('inventory/addProductCategoryTwo');
            $attributes = array(
                "id" => "prod_cat_two", 
                "name" => "prod_cat_two",
                "method" => "POST"
            );

            echo form_open($action, $attributes); 
        ?>
        
        <div class="col-md-12 card-body">

            <h4><?php echo $heading ?> <a href="<?php echo base_url('inventory/productCategoryTwo') ?>" class="btn btn-success btn-sm float-right" data-toggle="tooltip" data-placement="top" title="Back to List"><span class="fa fa-arrow-left"></span></a></h4><hr>

            <div class="col-md-6">
                <?php if($this->session->flashdata('error_msg')): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                    </div>
                <?php endif; ?>

                <?php if($this->session->flashdata('success_msg')): ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="prod_category_one">Select Category One <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <select class="form-control select2" name="prod_category_one" id="prod_category_one">
                        <option value="">--- Select Product Category One ---</option>
                        <?php if($productCategoryOne): ?>
                            <?php foreach($productCategoryOne as $key => $category): ?>
                                <option value="<?php echo $category->id ?>" <?php echo set_select('prod_category_one',  $category->id); ?>><?php echo ucfirst($category->category_name) ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <?php echo form_error('prod_category_one'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="category_name">Category Name <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Enter Category Name" value="<?php echo set_value('category_name') ?>">
                    <?php echo form_error('category_name'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="box_no">Box No</label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="box_no" name="box_no" placeholder="Enter Box No" value="<?php echo set_value('box_no') ?>">
                    <?php echo form_error('box_no'); ?>
                </div>
            </div>
            
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-success">Add</button>
        </div>

        <?php echo form_close() ?>
    </div>
    <!-- /.card -->

</div>