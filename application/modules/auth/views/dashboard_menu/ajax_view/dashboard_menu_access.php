<div class="col-md-12">
    <label>Select Access Permission</label>
</div>
<div class="col-md-7">
    <table class="table">
        <thead>
            <tr>
                <th>Menu</th>
                <th class="text-center">View</th>
                <th class="text-center">Add</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
        </thead>

        <tbody>
        <?php 
            if($dashboardMenu): 
            foreach($dashboardMenu as $key => $menu):    
        ?>
            <tr>
                <td><?php echo $menu->menu_name; echo ($menu->menu_type == "p") ? " *" : ""; ?></td>

                <td class="text-center"><input class="permission-checkbox" type="checkbox" name="view_<?php echo $menu->id ?>" id="view_<?php echo $menu->id ?>" value="1" <?php echo (viewAccess($menu->id, $userGroupId) == 1) ? "checked" : ""; ?> ></td>

                <td class="text-center"><?php if ($menu->menu_type == "p"): ?> <span>-</span> <?php else: ?><input class="permission-checkbox" type="checkbox" name="add_<?php echo $menu->id ?>" id="add_<?php echo $menu->id ?>" value="1" <?php echo (addAccess($menu->id, $userGroupId) == 1) ? "checked" : ""; ?> > <?php endif; ?></td>

                <td class="text-center"><?php if ($menu->menu_type == "p"): ?> <span>-</span> <?php else: ?><input class="permission-checkbox" type="checkbox" name="edit_<?php echo $menu->id ?>" id="edit_<?php echo $menu->id ?>" value="1" <?php echo (editAccess($menu->id, $userGroupId) == 1) ? "checked" : ""; ?> > <?php endif; ?></td>

                <td class="text-center"><?php if ($menu->menu_type == "p"): ?> <span>-</span> <?php else: ?><input class="permission-checkbox" type="checkbox" name="delete_<?php echo $menu->id ?>" id="delete_<?php echo $menu->id ?>" value="1" <?php echo (deleteAccess($menu->id, $userGroupId) == 1) ? "checked" : ""; ?> > <?php endif; ?></td>
            </tr>
        <?php 
            endforeach;
            endif;
        ?>
        </tbody>
    </table>
</div>