<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">

        <?php
            $action = base_url('auth/changePassword');
            $attributes = array(
                "id"        => "change_password", 
                "name"      => "change_password",
                "method"    => "POST"
            );
            echo form_open($action, $attributes); 
        ?>
        
        <div class="col-md-6 card-body">

            <?php if($this->session->flashdata('error_msg')): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                </div>
            <?php endif; ?>

            <?php if($this->session->flashdata('success_msg')): ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                </div>
            <?php endif; ?>

            <div class="form-group">
                <label for="oldpassword">Old Password <span class="red-asterisk">*</span></label>
                <input type="password" class="form-control" id="oldpassword" name="oldpassword" placeholder="Enter Old Password" value="<?php echo set_value('oldpassword') ?>">
                <?php echo form_error('oldpassword'); ?>
            </div>

            <div class="form-group">
                <label for="newpassword">New Password <span class="red-asterisk">*</span></label>
                <input type="password" class="form-control" id="newpassword" name="newpassword" placeholder="Enter New Password" value="<?php echo set_value('newpassword') ?>">
                <?php echo form_error('newpassword'); ?>
            </div>
            
            <div class="form-group">
                <label for="confirmpassword">Confirm Password <span class="red-asterisk">*</span></label>
                <input type="password" class="form-control" id="confirmpassword" name="confirmpassword" placeholder="Enter Confirm Password" value="<?php echo set_value('confirmpassword') ?>">
                <?php echo form_error('confirmpassword'); ?>
            </div>
            
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-warning">Update Password</button>
        </div>

        <?php echo form_close() ?>
    </div>
    <!-- /.card -->

</div>