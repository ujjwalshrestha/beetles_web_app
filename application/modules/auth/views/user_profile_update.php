<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">

        <?php
            $action = base_url('auth/userProfileUpdate');
            $attributes = array(
                "id"        => "user_profile", 
                "name"      => "user_profile",
                "method"    => "POST"
            );

            echo form_open($action, $attributes); 
        ?>
        
        <div class="card-body">
            <div class="row">
                <div class="col-md-11">
                    <?php if($this->session->flashdata('error_msg')): ?>
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                        </div>
                    <?php endif; ?>

                    <?php if($this->session->flashdata('success_msg')): ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="col-md-5">
                    <h4>Basic Detail</h4><br>
                    <div class="form-group">
                        <label for="firstname">First Name <span class="red-asterisk">*</span></label>
                        <input type="text" class="form-control" id="firstname" name="firstname" value="<?php echo $user->firstname ?>" placeholder="Enter First Name" required>
                        <?php echo form_error('firstname'); ?>
                    </div>

                    <div class="form-group">
                        <label for="lastname">Last Name <span class="red-asterisk">*</span></label>
                        <input type="lastname" class="form-control" id="lastname" name="lastname" value="<?php echo $user->lastname ?>" placeholder="Enter Last Name" required>
                        <?php echo form_error('lastname'); ?>
                    </div>
                    
                    <div class="form-group">
                        <label for="dob">Date of Birth</label>
                        <input type="date" class="form-control" id="dob" name="dob" value="<?php echo $user->dob ?>" placeholder="Enter Date of Birth">
                        <?php echo form_error('dob'); ?>
                    </div>
                    
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" id="address" name="address" value="<?php echo $user->address ?>" placeholder="Enter Address">
                        <?php echo form_error('address'); ?>
                    </div>
                    
                    <div class="form-group">
                        <label for="gender">Gender</label>
                        <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="male" name="gender" value="m" <?php echo ($user->gender == 'm') ? 'checked' : ''; ?> >
                            <label for="male" class="custom-control-label">Male</label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="female" name="gender"  value="f" <?php echo ($user->gender == 'f') ? 'checked' : ''; ?> >
                            <label for="female" class="custom-control-label">Female</label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="others" name="gender"  value="o" <?php echo ($user->gender == 'o') ? 'checked' : ''; ?> >
                            <label for="others" class="custom-control-label">Others</label>
                        </div>
                        <?php echo form_error('gender'); ?>
                    </div>
                    
                    <div class="form-group">
                        <label for="contact">Contact</label>
                        <input type="text" class="form-control" id="contact" name="contact" value="<?php echo $user->contact ?>" placeholder="Enter Contact">
                        <?php echo form_error('contact'); ?>
                    </div>
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-5">
                    <h4>Authentication Detail</h4><br>
                    <div class="form-group">
                        <label for="username">Username <span class="red-asterisk">*</span></label>
                        <input type="text" class="form-control" id="username" name="username" value="<?php echo $user->username ?>" placeholder="Enter Username" required>
                        <?php echo form_error('username'); ?>
                    </div>

                    <div class="form-group">
                        <label for="email">Email <span class="red-asterisk">*</span></label>
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $user->email ?>" placeholder="Enter Email" required>
                        <?php echo form_error('email'); ?>
                    </div>
                </div>

                <div class="col-md-1">
                    <a href="<?php echo base_url("auth/userProfile") ?>" class="btn btn-success btn-sm float-right" data-toggle="tooltip" data-placement="top" title="Back to Profile"><span class="fa fa-arrow-left"></span></a>
                </div>
            </div>
        </div>
        <!-- /.card-body -->

        <!-- card footer -->
        <div class="card-footer">
            <button type="submit" class="btn btn-warning">Update</button>
        </div>

        <?php echo form_close() ?>
    </div>
    <!-- /.card -->

</div>