<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">
        
        <div class="col-md-12 card-body">
            <table class="table table-responsive-lg" id="user_profile_table">
                <tr>
                    <th class="top-borderless">First Name</th>
                    <td class="top-borderless"><?php echo ucfirst($user->firstname); ?></td>
                </tr>

                <tr>
                    <th>Last Name</th>
                    <td><?php echo ucfirst($user->lastname); ?></td>
                </tr>

                <tr>
                    <th>Date of Birth</th>
                    <td><?php echo ($user->dob) ? $user->dob : '-'; ?></td>
                </tr>

                <tr>
                    <th>Address</th>
                    <td><?php echo ($user->address) ? $user->address : '-'; ?></td>
                </tr>

                <tr>
                    <th>Gender</th>
                    <td><?php echo ($user->gender) ? ( ($user->gender == 'm') ? 'Male' : (($user->gender == 'f') ? 'Female' : 'Others') ) : '-'; ?></td>
                </tr>

                <tr>
                    <th>Contact</th>
                    <td><?php echo ($user->contact) ? $user->contact : '-'; ?></td>
                </tr>

                <tr>
                    <th>Username</th>
                    <td><?php echo $user->username ?></td>
                </tr>

                <tr>
                    <th>Email</th>
                    <td><?php echo $user->email ?></td>
                </tr>

                <?php if ($userGroup->profile_update_access == 1): ?>
                <tr>
                    <td colspan="2"><a class="btn btn-success" href="<?php echo base_url("auth/userProfileUpdate") ?>">Update Profile</a></td>
                </tr>
                <?php endif; ?>
            </table>
        </div>
        <!-- /.card-body -->

        
    </div>
    <!-- /.card -->

</div>