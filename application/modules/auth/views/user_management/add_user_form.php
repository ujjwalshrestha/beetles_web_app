<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">

        <?php
            $action = base_url('auth/addUser');
            $attributes = array(
                "id" => "add_user_form", 
                "name" => "add_user_form",
                "method" => "POST"
            );

            echo form_open($action, $attributes); 
        ?>
        
        <div class="col-md-12 card-body">

            <h4><?php echo $heading ?> <a href="<?php echo base_url('auth/manageUser') ?>" class="btn btn-success btn-sm float-right" data-toggle="tooltip" data-placement="top" title="Back to List"><span class="fa fa-arrow-left"></span></a></h4><hr>

            <?php if($this->session->flashdata('error_msg')): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                </div>
            <?php endif; ?>

            <?php if($this->session->flashdata('success_msg')): ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                </div>
            <?php endif; ?>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="user_group">Select User Group <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <select class="form-control select2" name="user_group" id="user_group">
                        <option value="">--- Select User Group ---</option>
                        <?php if($userGroupList): ?>
                            <?php foreach($userGroupList as $key => $group): ?>
                                <option value="<?php echo $group->id ?>" <?php echo set_select('user_group',  $group->id); ?>><?php echo ucfirst($group->group_name) ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <?php echo form_error('user_group'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="first_name">First Name <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter First Name" value="<?php echo set_value('first_name') ?>">
                    <?php echo form_error('first_name'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="last_name">Last Name <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter Last Name" value="<?php echo set_value('last_name') ?>" >
                    <?php echo form_error('last_name'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label>Gender <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="male" name="gender" value="m" checked>
                        <label for="male" class="custom-control-label">Male</label>
                    </div>

                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="female" name="gender"  value="f">
                        <label for="female" class="custom-control-label">Female</label>
                    </div>

                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="others" name="gender"  value="o">
                        <label for="others" class="custom-control-label">Others</label>
                    </div>
                    <?php echo form_error('gender'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="dob">Date of Birth <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="date" class="form-control" id="dob" name="dob" value="<?php echo set_value('dob') ?>" >
                    <?php echo form_error('dob'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="address">Address <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address" value="<?php echo set_value('address') ?>" >
                    <?php echo form_error('address'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="pan_no">PAN No.</label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="pan_no" name="pan_no" placeholder="Enter PAN No." value="<?php echo set_value('pan_no') ?>" >
                    <?php echo form_error('pan_no'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="blood_group">Blood Group</label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="blood_group" name="blood_group" placeholder="Enter Blood Group" value="<?php echo set_value('blood_group') ?>" >
                    <?php echo form_error('blood_group'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="contact">Contact No.  <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="contact" name="contact" placeholder="Enter Contact No." value="<?php echo set_value('contact') ?>" >
                    <?php echo form_error('contact'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="email">Email <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" value="<?php echo set_value('email') ?>" >
                    <?php echo form_error('email'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="username">Username <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Enter Username" value="<?php echo set_value('username') ?>" >
                    <?php echo form_error('username'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="password">Password <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" >
                    <?php echo form_error('password'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="joined_date">Joined Date <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="date" class="form-control" id="joined_date" name="joined_date" value="<?php echo set_value('joined_date') ?>" >
                    <?php echo form_error('joined_date'); ?>
                </div>
            </div>
            
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-success">Add</button>
        </div>

        <?php echo form_close(); ?>
    </div>
    <!-- /.card -->

</div>