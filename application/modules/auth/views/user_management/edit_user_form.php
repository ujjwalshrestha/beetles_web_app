<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">

        <?php
            $action = base_url('auth/editUser/'.$detail->user_id);
            $attributes = array(
                "id" => "edit_user_form", 
                "name" => "edit_user_form",
                "method" => "POST"
            );

            echo form_open($action, $attributes); 
        ?>
        
        <div class="col-md-12 card-body">

            <h4><?php echo $heading ?> <a href="<?php echo base_url('auth/manageUser') ?>" class="btn btn-success btn-sm float-right" data-toggle="tooltip" data-placement="top" title="Back to List"><span class="fa fa-arrow-left"></span></a></h4><hr>

            <div class="col-md-6">
                <?php if($this->session->flashdata('error_msg')): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                    </div>
                <?php endif; ?>

                <?php if($this->session->flashdata('success_msg')): ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="user_group">Select User Group <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <select class="form-control select2" name="user_group" id="user_group">
                        <option value="">--- Select User Group ---</option>
                        <?php if($userGroupList): ?>
                            <?php foreach($userGroupList as $key => $group): ?>
                                <option value="<?php echo $group->id ?>" <?php echo ($detail->group_id == $group->id) ? 'selected' : ''; ?> ><?php echo ucfirst($group->group_name) ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <?php echo form_error('user_group'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="first_name">First Name <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter First Name" value="<?php echo $detail->firstname ?>" >
                    <?php echo form_error('first_name'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="last_name">Last Name <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter Last Name" value="<?php echo $detail->lastname ?>" >
                    <?php echo form_error('last_name'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label>Gender <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="male" name="gender" value="m" <?php echo ($detail->gender == 'm') ? 'checked' : ''; ?> >
                        <label for="male" class="custom-control-label">Male</label>
                    </div>

                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="female" name="gender"  value="f" <?php echo ($detail->gender == 'f') ? 'checked' : ''; ?> >
                        <label for="female" class="custom-control-label">Female</label>
                    </div>

                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="others" name="gender"  value="o" <?php echo ($detail->gender == 'o') ? 'checked' : ''; ?> >
                        <label for="others" class="custom-control-label">Others</label>
                    </div>
                    <?php echo form_error('gender'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="dob">Date of Birth <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="date" class="form-control" id="dob" name="dob" value="<?php echo $detail->dob ?>" >
                    <?php echo form_error('dob'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="address">Address <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address" value="<?php echo $detail->address ?>" >
                    <?php echo form_error('address'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="pan_no">PAN No.</label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="pan_no" name="pan_no" placeholder="Enter PAN No." value="<?php echo $detail->pan_no ?>" >
                    <?php echo form_error('pan_no'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="blood_group">Blood Group</label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="blood_group" name="blood_group" placeholder="Enter Blood Group" value="<?php echo $detail->blood_group ?>" >
                    <?php echo form_error('blood_group'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="contact">Contact No.  <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="contact" name="contact" placeholder="Enter Contact No." value="<?php echo $detail->contact ?>" >
                    <?php echo form_error('contact'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="email">Email <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" value="<?php echo $detail->email ?>" >
                    <?php echo form_error('email'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="username">Username <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Enter Username" value="<?php echo $detail->username ?>" >
                    <?php echo form_error('username'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="password">Password <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" value="<?php echo decrypt_password($detail->password) ?>" >
                    <?php echo form_error('password'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="joined_date">Joined Date <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="date" class="form-control" id="joined_date" name="joined_date" value="<?php echo $detail->joined_date ?>" >
                    <?php echo form_error('joined_date'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="status">Active Status <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <select class="form-control" name="status" id="status">
                        <option value="1" <?php echo ($detail->active_status == 1) ? 'selected' : ''; ?> >Active</option>
                        <option value="0" <?php echo ($detail->active_status == 0) ? 'selected' : ''; ?> >Inactive</option>
                    </select>
                    <?php echo form_error('status'); ?>
                </div>
            </div>
            
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-warning">Update</button>
        </div>

        <?php echo form_close() ?>
    </div>
    <!-- /.card -->

</div>