<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Through this function, auth user detail from table 'auth_user' is fetched.
	 *
	 * @return array
	 */
	public function getAuthUser()
	{
		$user_id = session_data('user_id'); // userid from session

		$where = array(
			'id' => $user_id
		);

		$result = $this->db->get_where('auth_user', $where);

		if ($result->num_rows() == 1) {
			return $result->row();

		} else {
			return false;
		}
	}
	
	/**
	 * Both auth detail and user detail of logged in user is fetched.
	 * User id is taken from the session.
	 *
	 * @return array
	 */
	public function getAuthUserDetail()
	{
		$user_id = session_data('user_id'); // userid from session

		$where = array(
			'a.id' => $user_id
		);

		$result = $this->db->select('d.firstname, d.lastname, d.dob, d.address, d.gender, d.contact, a.username, a.email')
						   ->from('auth_user as a')
						   ->join('user_detail as d', 'a.id = d.user_id')
						   ->where($where)
						   ->get();

		if ($result->num_rows() == 1) {
			return $result->row();

		} else {
			return false;
		}
	}

	/**
	 * Through this function, login process is carried out. From the username or email, overall session detail of the respective user is fetched. Initially, username/email and delete status of the user is checked to determine if the user exists or not. Then the login status of user group of respective username or email is checked. Then the password is checked, if matched or not. After that, user status is checked, if active or not. After completion of these three procedure, session data is set of the respective user.
	 *
	 * @param string $usernameEmail
	 * @param string $password
	 * @return void
	 */
    public function authLogin($usernameEmail, $password)
	{
		$where = "username='".$usernameEmail."' AND delete_status=0 OR email='".$usernameEmail."' AND delete_status=0";

		// getting user's detail through the username
        $result = $this->db->get_where('auth_user', $where);

		if ($result->num_rows() == 1) {
			$detail = $result->row();
			$user_id = $detail->id; // get user id
			$userGroup = $this->getAuthGroupById($user_id); // get user-group detail by userid

			if ($userGroup->login_status == 1) { // check login status of user group
				$decryptedPassword = decrypt_password($detail->password); // decrypt user's password stored in database

				// Check if the password entered by user and the decrypted password matches or not.
				if ($password == $decryptedPassword) {

					// Check if the user status is active or not
					if ( ($detail->temp_status == 1) && ($detail->active_status == 1) ) {
						$group_id = $userGroup->group_id;
						$group_name = $userGroup->group_name;
						
						// Updates IP Address and Last login datetime of the current logged in user.
						$this->updateIpAddrLastLogin($user_id); 

						// declaring required session data after login
						$session_data = array(
							'user_id' => $user_id, // setting userid in session
							'username' => $username, // setting username in session
							'group_id' => $group_id, // setting groupid in session
							'group_name' => $group_name, // setting groupname in session
							'is_super_admin' => $detail->is_super_admin, // setting is_super_admin in session
						);
						$this->session->set_userdata($session_data); // setting data in session
						return 'success';

					} else {
						return '403'; 
					}

				} else {
					return false;
				}
				
			} else {
				return '403'; 
			}

        } else {
            return '404';
        }
	}	

	/**
	 * Through this function, group ID and and group name of the respective logged in user is fetched through its user id.
	 *
	 * @param int $user_id
	 * @return void
	 */
	public function getAuthGroupById($user_id)
	{
		$where = array(
			'ugm.user_id' => $user_id
		);

		$result = $this->db->select('ag.id as group_id, ag.group_name as group_name, ag.login_status')
						   ->from('auth_user_group_mapping as ugm')
						   ->join('auth_group as ag', 'ugm.group_id = ag.id')
						   ->where($where)
						   ->get();
		
		if ($result->num_rows() == 1) {
			return $result->row();

		} else {
			return false;
		}
	}

	/**
	 * During the login process, IP Address and last login date and time of the respective logged in user is updated through this function.
	 *
	 * @param int $user_id
	 * @return boolean
	 */
	public function updateIpAddrLastLogin($user_id)
	{
		$update_data = array(
			'ip_address' => getUserIpAddr(),
			'last_login' =>	date('Y-m-d H:i:s')	
		);

		$where = array(
			'id' => $user_id
		);

		$this->db->where($where);
		return $this->db->update('auth_user', $update_data);
    }

	public function getDashboardSideMenu()
	{
		$where = array(
			'status' => 1,
			'parent_menu_id' => NULL,
			'delete_status' => 0
		);

		$this->db->order_by('order_by');
		$result = $this->db->get_where('dashboard_menu', $where);

		if ($result->num_rows() > 0) {
			return $result->result();

		} else {
			return false;
		}
	}

	/**
	 * Only parent dashboard menu is fetched.
	 *
	 * @return array
	 */
	public function getParentMenu()
	{
		$where = array(
			'status' => 1,
			'menu_type' => 'p',
			'delete_status' => 0
		);

		$result = $this->db->get_where('dashboard_menu', $where);

		if ($result->num_rows() > 0) {
			return $result->result();

		} else {
			return false;
		}
	}

	/**
	 * All the dashboard menu is fetched.
	 *
	 * @return array
	 */
	public function getDashboardMenuList()
	{
		$where = array(
			'dm1.delete_status' => 0
		);

		$result = $this->db->select('dm1.id, dm1.menu_name, dm2.menu_name as parent_menu_name, dm1.menu_uri, dm1.status, au.username, dm1.created_at, dm1.order_by')
						   ->from('dashboard_menu as dm1')
						   ->join('dashboard_menu as dm2', 'dm1.parent_menu_id = dm2.id', 'left')
						   ->join('auth_user as au', 'dm1.created_by = au.id')
						   ->where($where)
						   ->order_by('dm1.created_at')
						   ->get();

		if ($result->num_rows() > 0) {
			return $result->result();

		} else {
			return false;
		}		
	}

	/**
	 * This function populates all the list of user group except for 'Admin' user group. This is done because superadmin of the system is also mapped with the 'Admin' user group. So, it must not be edited or deleted from the manage user group section.
	 *
	 * @return void
	 */
	public function getUserGroupList()
	{
		$where = array(
			'delete_status' => 0
		);

		$where_not = array(1); // Id of 'Admin' user group
		$this->db->where_not_in('id', $where_not); // except admin group, populates all user group list
		$result = $this->db->get_where('auth_group', $where);

		if ($result->num_rows() > 0) {
			return $result->result();

		} else {
			return false;
		}
	}

	/**
	 * This function populates all the list of user group except for 'Admin' and other external user group. This is done because superadmin of the system is also mapped with the 'Admin' user group. So, it must not be edited or deleted from the manage user group section.
	 *
	 * @return void
	 */
	public function getInternalUserGroupList()
	{
		$where = array(
			'delete_status' => 0
		);

		$where_not = array(1, 8); // Id of 'Admin' user group
		$this->db->where_not_in('id', $where_not); // except admin group, populates all user group list
		$result = $this->db->get_where('auth_group', $where);

		if ($result->num_rows() > 0) {
			return $result->result();

		} else {
			return false;
		}
	}

	/**
	 * User group (auth group) is fetched by group id.
	 *
	 * @param int $group_id
	 * @return array
	 */
	public function getUserGroupById($group_id)
	{
		$where = array(
			'id' => $group_id
		);

		$result = $this->db->get_where('auth_group', $where);

		if ($result->num_rows() == 1) {
			return $result->row();

		} else {
			return false;
		}
	}

	/**
	 * All the user list fetched.
	 *
	 * @return array
	 */
	public function getUsersList()
	{
		$where_not = array(1);
		$where = array(
			'au.delete_status' => 0
		);

		$result = $this->db->select('au.id as user_id, au.username, au.email, au.active_status, ud.firstname, ud.lastname, ag.group_name')
						   ->from('auth_user as au')
						   ->join('user_detail as ud', 'au.id = ud.user_id')
						   ->join('auth_user_group_mapping as ugm', 'au.id = ugm.user_id', 'left')
						   ->join('auth_group as ag', 'ag.id = ugm.group_id', 'left')
						   ->where($where)
						   ->where_not_in('au.is_super_admin', $where_not)
						   ->order_by('ud.firstname')
						   ->get();

		if ($result->num_rows() > 0) {
			return $result->result();

		} else {
			return false;
		}				
	}

	/**
	 * Both auth and detail data of user is fetched by user id.
	 *
	 * @param int $user_id
	 * @return array
	 */
	public function getUserDetailById($user_id)
	{
		$where = array(
			'au.id' => $user_id
		);

		$result = $this->db->select('au.id as user_id, au.username, au.email, au.password, au.active_status, ud.firstname, ud.lastname, ud.gender, ud.dob, ud.address, ud.pan_no, ud.blood_group, ud.contact, ud.joined_date, ag.id as group_id, ag.group_name')
						   ->from('auth_user as au')
						   ->join('user_detail as ud', 'au.id = ud.user_id')
						   ->join('auth_user_group_mapping as ugm', 'au.id = ugm.user_id', 'left')
						   ->join('auth_group as ag', 'ag.id = ugm.group_id', 'left')
						   ->where($where)
						   ->get();

		if ($result->num_rows() == 1) {
			return $result->row();

		} else {
			return false;
		}	
	}

	/**
	 * Auth user data is inserted.
	 * Last insert id from auth_user table is returned.
	 *
	 * @param array $insert_auth_user
	 * @return int
	 */
	public function insertAuthUser($insertAuthUserData)
	{
		$this->db->insert('auth_user', $insertAuthUserData);
		$insert_id = $this->db->insert_id();

   		return $insert_id;
	}

	/**
	 * Email Id is checked to determine if it exists or not.
	 *
	 * @param string $email
	 * @param int $user_id
	 * @return array
	 */
	public function checkEmail($email, $user_id)
	{
		$where = array(
			'email' => $email
		);

		$this->db->where_not_in('id', $user_id);
		$result = $this->db->get_where('auth_user', $where);

		if ($result->num_rows() == 1) {
			return $result->row();

		} else {
			return false;
		}	
	}

	/**
	 * Username is checked to determine if it exists or not.
	 *
	 * @param string $username
	 * @param int $user_id
	 * @return array
	 */
	public function checkUsername($username, $user_id)
	{
		$where = array(
			'username' => $username
		);

		$this->db->where_not_in('id', $user_id);
		$result = $this->db->get_where('auth_user', $where);

		if ($result->num_rows() == 1) {
			return $result->row();
		} else {
			return false;
		}	
	}

	/**
	 * User detail is updated.
	 *
	 * @param array $update_data
	 * @param int $user_id
	 * @return boolean
	 */
	public function updateUserDetail($update_data, $user_id)
	{
		$where = array(
			'user_id' => $user_id
		);

		$this->db->where($where);
		return $this->db->update('user_detail', $update_data);
	}

	/**
	 * User-group Id of the respective user id is updated.
	 *
	 * @param array $update_data
	 * @param int $user_id
	 * @return boolean
	 */
	public function updateUserGroupMap($update_data, $user_id)
	{
		$where = array(
			'user_id' => $user_id
		);

		$this->db->where($where);
		return $this->db->update('auth_user_group_mapping', $update_data);
	}
	
	/**
	 * All the dashboard menu are fetched.
	 *
	 * @return array
	 */
	public function getAllDashMenu()
	{
		$where = array(
			'status' => 1,
			'delete_status' => 0
		);

		$this->db->order_by('menu_name', 'asc');
		$result = $this->db->get_where('dashboard_menu', $where);
		
		if ($result->num_rows() > 0) {
			return $result->result();
		} else {
			return false;
		}
	}

	/**
	 * Checks for existence of menu permission access through user-group id and menu id. 
	 *
	 * @param int $userGroupId
	 * @param int $menuId
	 * @return boolean
	 */
	public function checkMenuAccess($userGroupId, $menuId)
	{
		$where = array(
			'group_id' => $userGroupId,
			'menu_id' => $menuId
		);

		$result = $this->db->get_where('dashboard_menu_access', $where);
		if ($this->db->affected_rows() == 1) {
            return true;
        } else {
            return false;
        }
	}

	/**
	 * Menu access permission data is updated. 
	 *
	 * @param int $userGroupId
	 * @param int $menuId
	 * @param array $updateData
	 * @return boolean
	 */
	public function updateMenuAccessLevel($userGroupId, $menuId, $updateData)
	{
		$where = array(
			'group_id' => $userGroupId,
			'menu_id' => $menuId
		);

		$this->db->where($where);
		return $this->db->update('dashboard_menu_access', $updateData);
	}

	/**
	 * Not in use.
	 *
	 * @param int $userGroupId
	 * @param int $menuId
	 * @return array
	 */
	public function getDashboardMenuAccess_old2($userGroupId, $menuId)
	{
		$where = array(
			"group_id" => $userGroupId,
			"menu_id" => $menuId
		);

		$result = $this->db->get_where("dashboard_menu_access", $where);

		if ($result->num_rows() == 1) {
			return $result->row();
		} else {
			return false;
		}	
	}

	/**
	 * Dashboard menu access permission is fetched.
	 *
	 * @param int $userGroupId
	 * @return array
	 */
	public function getDashboardMenuAccess($userGroupId)
	{
		$where = array(
			"a.group_id" => $userGroupId
		);

		$result = $this->db->select("a.*, m.menu_name, m.menu_type")
						   ->from("dashboard_menu_access as a")
						   ->join("dashboard_menu as m", "a.menu_id = m.id", "inner")
						   ->where($where)
						   ->get();

		if ($result->num_rows() > 0) {
			return $result->result();
		} else {
			return false;
		}	
	}

	/**
	 * Through this function, all the user session is unset and destroyed.
	 *
	 * @param string $redirect_path
	 * @return void
	 */
    public function authLogout($redirect_path)
    {	
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect($redirect_path, 'refresh');
			exit;
		}
		
        $unset = array('user_id', 'user_name', 'group_id', 'group_name', 'is_super_admin');
		$this->session->unset_userdata($unset); // unset login session data
        $this->session->set_flashdata('success_msg', 'Logged Out.'); // set success message
		redirect($redirect_path);
        $this->session->sess_destroy(); // destroy whole session
    }
}

    