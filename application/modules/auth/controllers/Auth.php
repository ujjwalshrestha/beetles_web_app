<?php 

if( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * All the authorization and authentication functionality is carried out within 'Auth' class. This class is for only admin-panel purpose not for main site.
 */
class Auth extends MX_Controller
{
    /*
	 *	variable to hold data for view
	 */
	private static $viewData = array();
	
	public function __construct()
	{
		parent::__construct();

		// To show the username in admin dashboard of the logged in user
		if (isLoggedin()) { 
			self::$viewData['username'] = $this->auth_model->getAuthUser()->username;
		}

		// Dashboard side menu
		self::$viewData['dashboardSideMenu'] = $this->auth_model->getDashboardSideMenu();

		// Checks if the auth active status, auth temp_status and user group login_status is 0 or not. If one of them is 0, then user is forced to logout of the system.
		if (isLoggedin() && (($this->auth_model->getAuthUser()->active_status == 0) || ($this->auth_model->getAuthUser()->temp_status == 0) || ($this->auth_model->getUserGroupById(session_data('group_id'))->login_status == 0)) ) 
		{
			$this->logout(); // force logout
		}
	}

	public function index()
	{
		echo 'Beetles International Traders - Web Application 1.0';
	}

	/**
	 * This function returns login page to start session within the system. Also, login action is carried out in this function which is determined by checking if the respective function is called through post moethod or not.
	 *
	 * @return void
	 */
	public function login()
	{
		// Checks if not logged in
		if (isLoggedin()) {
			redirect('auth/dashboard');
			exit;
		}

		// Check whether the function is called through post method
		if ($this->input->post()) {
			// Form-validation
			$this->form_validation->set_rules('username_email', ' ', 'required');
			$this->form_validation->set_rules('password', ' ', 'required');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if (!$this->form_validation->run()) {
				// If validation is false, then return to page login and show validation error. 
				$this->load->view('login');

			} else {
				$usernameEmail = $this->input->post('username_email');
				$password = $this->input->post('password');

				// Login procedure
				$authLogin = $this->auth_model->authLogin($usernameEmail, $password);

				if ($authLogin == '404') {
					// Checks if user exists or not
					$message = 'User does not exists.';
					$this->session->set_flashdata('error_msg', $message);
					redirect('auth/login'); 

				} else {
					// Check if all the conditions for login are satisfied or not.
					if ($authLogin == false) {
						$message = 'Username or password incorrect.';
						$this->session->set_flashdata('error_msg', $message);
						redirect('auth/login'); 

					} else {
						// Checks if account is active or not
						if ($authLogin == '403') {
							$message = 'Your are not authorized to access this system.';
							$this->session->set_flashdata('error_msg', $message);
							redirect('auth/login'); 

						} elseif ($authLogin == 'success') {
							redirect('dashboard'); 
						}
					}
				}
			}

		} else {
			$this->load->view('login');
		}
	}

	/**
	 * Through this function, change password form view page is loaded.
	 *
	 * @return void
	 */
	public function changePassword()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Change Password";
		self::$viewData['breadcrumb'] = "Change Password"; 
		self::$viewData['page'] = "auth/change_password";

		if ($this->input->post()) {
			// callback function validation
			$this->form_validation->CI =& $this;  
			$this->form_validation->set_rules('oldpassword', ' ', 'required|callback_oldpassword_check');
			$this->form_validation->set_rules('newpassword', ' ', 'required|callback_newpassword_check');
			$this->form_validation->set_rules('confirmpassword', ' ', 'required|matches[newpassword]',
				array('matches' => 'The confirm password field does not match the new password field.')
			);
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				$newpassword = $this->input->post('newpassword');
				$password_encrypted = encrypt_password($newpassword); // encrypt password

				$updateData = array(
					'password' => $password_encrypted,
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s')
				);

				$updatePassword = $this->common_model->update($userId=session_data('user_id'), $updateData, $tableName="auth_user");
				$redirect_url = 'auth/changePassword';
				
				if ($updatePassword) {
					$message = 'Password has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirect_url, 'refresh'); 

				} else {
					$message = 'Password update failed.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirect_url, 'refresh'); 
				}
			}
		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Through this function, the old password is checked if it matches with the one stored in the database or not.
	 *
	 * @param string $oldpassword Old password fetched from user input.
	 * @return void
	 */
	public function oldpassword_check($oldpassword)
	{
		// If oldpassword is empty
		if (!$oldpassword) {
			$this->form_validation->set_message('oldpassword_check', 'The field is required.');
		   	return FALSE;
		}

		$getAuthUser = $this->auth_model->getAuthUser();
		$oldpassword_db = decrypt_password($getAuthUser->password);
	 
		// If old password does not match
		if ($oldpassword != $oldpassword_db) {
		   $this->form_validation->set_message('oldpassword_check', 'The old password did not match.');
		   return FALSE;
		   
		} else {
			return TRUE;
		}
	}

	/**
	 * Through this function, the new password is checked if it matches with the old password or not.
	 *
	 * @param string $newpassword New password fetched from user input.
	 * @return void
	 */
	public function newpassword_check($newpassword)
	{
		// If newpassword is empty
		if (!$newpassword) {
			$this->form_validation->set_message('newpassword_check', 'The field is required.');
		   	return FALSE;
		}

		$getAuthUser = $this->auth_model->getAuthUser();
		$oldpassword = decrypt_password($getAuthUser->password);
	 
		// If old password and new password matches
		if ($newpassword == $oldpassword) {
		   $this->form_validation->set_message('newpassword_check', 'You entered old password. Please enter new different password.');
		   return FALSE;
		   
		} else {
			return TRUE;
		}
	}

	/**
	 * Through this function, user profile page is displayed.
	 *
	 * @return void
	 */
	public function userProfile()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "User Profile";
		self::$viewData['breadcrumb'] = "User Profile"; 
		// gets both auth user data and user details
		self::$viewData['user'] = $this->auth_model->getAuthUserDetail(); 
		// user group data is fetched for the purpose of getting profile update access
		self::$viewData['userGroup'] = $this->auth_model->getUserGroupById(session_data("group_id")); 
		self::$viewData['page'] = "auth/user_profile";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * Through this function, user profile update page is displayed. Also, after the update of user profile, the submitted form will passed in this same function for further validation and update process.
	 *
	 * @return void
	 */
	public function userProfileUpdate()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "User Profile";
		self::$viewData['breadcrumb'] = "User Profile"; 
		self::$viewData['user'] = $this->auth_model->getAuthUserDetail(); 
		self::$viewData['page'] = "auth/user_profile_update";

		if ($this->input->post()) {
			$this->form_validation->set_rules('username', ' ', 'required|trim');
			$this->form_validation->set_rules('email', ' ', 'required|trim|valid_email');
			$this->form_validation->set_rules('firstname', ' ', 'required|trim');
			$this->form_validation->set_rules('lastname', ' ', 'required|trim');

			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				$userId = session_data('user_id'); // user id from session

				// basic user detail
				$firstname = $this->input->post('firstname');
				$lastname = $this->input->post('lastname');
				$dob = $this->input->post('dob');
				$address = $this->input->post('address');
				$gender = $this->input->post('gender');
				$contact = $this->input->post('contact');

				// auth user data
				$username = $this->input->post('username');
				$email = $this->input->post('email');
				
				//redirect url
				$redirectUrl = 'auth/userProfileUpdate'; 

				// Checking if username exists or not. If it does, throw an error message.
				$checkUsername = $this->auth_model->checkUsername($username, $userId);
				if ($checkUsername) {
					$message = 'Username already exists.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}

				// Checking if email exists or not. If it does, throw an error message.
				$checkEmail = $this->auth_model->checkEmail($email, $userId);
				if ($checkEmail) {
					$message = 'Email already exists.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}

				// User detail to be updated
				$updateUserDetailData = array(
					'firstname' => $firstname,
					'lastname' => $lastname,
					'dob' => ($dob) ? $dob : null,
					'address' => $address,
					'gender' => $gender,
					'contact' => $contact,
					'updated_by' => $userId,
					'updated_at' => date('Y-m-d H:i:s')
				);

				// Auth Data to be updated
				$updateAuthUserData = array(
					'username' => $username,
					'email' => $email,
					'updated_by' => $userId,
					'updated_at' => date('Y-m-d H:i:s')
				);
				
				// update auth user data 
				$updateAuthUser = $this->common_model->update($userId, $updateAuthUserData, $tableName="auth_user");
				// update user detail data 
				$updateUserDetail = $this->common_model->update($userId, $updateUserDetailData, $tableName="user_detail");

				// Checks if the update is carried out successfully or not
				if ($updateAuthUser && $updateUserDetail) {
					$message = 'Profile has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Profile update failed.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * All the list of dashboard menu is displayed.
	 * Management of dashboard menu is done in this page.
	 *
	 * @return void
	 */
	public function manageMenu()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 7;
		self::$viewData['title'] = "Dashboard Menu List";
		self::$viewData['breadcrumb'] = "Manage Dashboard Menu"; 
		self::$viewData['heading'] = "Dashboard Menu List";
		self::$viewData['dashMenuList'] = $this->auth_model->getDashboardMenuList();
		self::$viewData['page'] = "auth/dashboard_menu/menu_list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * Form for adding new dashboard menu is loaded.
	 * Validation and insert functionality is also carried out within this function.
	 *
	 * @return void
	 */
	public function dashboardMenuAdd()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Add Dashboard Menu";
		self::$viewData['breadcrumb'] = "Manage Dashboard Menu";
		self::$viewData['heading'] = "Add Dashboard Menu"; 
		self::$viewData['parent_menu'] = $this->auth_model->getParentMenu(); 
		self::$viewData['page'] = "auth/dashboard_menu/add_menu_form";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('menu_type', ' ', 'required|trim');

			if ( ($this->input->post('menu_type') != 'p') && ($this->input->post('menu_type') != 's') ) {
				$this->form_validation->set_rules('parent_menu', ' ', 'required|trim');
			}

			if ($this->input->post('menu_type') != 'p') {
				$this->form_validation->set_rules('menu_uri', ' ', 'required|trim');
			}

			$this->form_validation->set_rules('name', ' ', 'required|trim');
			$this->form_validation->set_rules('slug', ' ', 'required|trim');
			$this->form_validation->set_rules('menu_icon', ' ', 'required|trim');
			$this->form_validation->set_rules('order_by', ' ', 'required|trim');
			$this->form_validation->set_rules('status', ' ', 'required|trim');

			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				$menu_type = $this->input->post('menu_type');
				$parent_menu = ( ($menu_type != 'p') && ($menu_type != 's') ) ? $this->input->post('parent_menu') : NULL;
				$menu_uri = $this->input->post('menu_uri');
				$menu_name = $this->input->post('name');
				$slug = $this->input->post('slug');
				$menu_icon = $this->input->post('menu_icon');
				$order_by = $this->input->post('order_by');
				$status = $this->input->post('status');

				$insertData = array(
					'menu_type' => $menu_type,
					'parent_menu_id' => $parent_menu,
					'menu_name' => $menu_name,
					'slug' => $slug,
					'menu_uri' => $menu_uri,
					'menu_icon' => $menu_icon,
					'order_by' => $order_by,
					'status' => $status,
					'created_by' => session_data('user_id'),
				);

				$insert = $this->common_model->insert($insertData, $tableName="dashboard_menu");
				// $insertDashboardMenu = $this->auth_model->insertDashboardMenu($insert_data);
				$redirectUrl = 'auth/manageMenu'; //redirect url

				if ($insert) {
					$message = 'Dashboard menu has been added.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to add dashboard menu.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Form for updating existing dashboard menu is loaded.
	 * Validation and update functionality is also carried out within this function.
	 *
	 * @param int $menuId
	 * @return void
	 */
	public function dashboardMenuEdit($menuId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Edit Dashboard Menu";
		self::$viewData['breadcrumb'] = "Manage Dashboard Menu";
		self::$viewData['heading'] = "Edit Dashboard Menu"; 
		self::$viewData['parent_menu'] = $this->auth_model->getParentMenu(); 
		// Getting dashboard menu detail by Id
		self::$viewData['detail'] = $this->common_model->getDataById($menuId, $tableName = "dashboard_menu");
		self::$viewData['page'] = "auth/dashboard_menu/edit_menu_form";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('menu_type', ' ', 'required|trim');

			if ($this->input->post('menu_type') != 'p') {
				$this->form_validation->set_rules('menu_uri', ' ', 'required|trim');
			}

			if ($this->input->post('menu_type') == 'c') {
				$this->form_validation->set_rules('parent_menu', ' ', 'required|trim');
			}

			$this->form_validation->set_rules('name', ' ', 'required|trim');
			$this->form_validation->set_rules('slug', ' ', 'required|trim');
			$this->form_validation->set_rules('menu_icon', ' ', 'required|trim');
			$this->form_validation->set_rules('order_by', ' ', 'required|trim');
			$this->form_validation->set_rules('status', ' ', 'required|trim');

			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				$menu_type = $this->input->post('menu_type');
				$parent_menu = ($menu_type == 'c') ? $this->input->post('parent_menu') : NULL;
				$menu_uri = $this->input->post('menu_uri');
				$menu_name = $this->input->post('name');
				$slug = $this->input->post('slug');
				$menu_icon = $this->input->post('menu_icon');
				$order_by = $this->input->post('order_by');
				$status = $this->input->post('status');

				$updateData = array(
					'menu_type' => $menu_type,
					'parent_menu_id' => $parent_menu,
					'menu_name' => $menu_name,
					'slug' => $slug,
					'menu_uri' => $menu_uri,
					'menu_icon' => $menu_icon,
					'order_by' => $order_by,
					'status' => $status,
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s'),
				);

				// update dashbaord menu 
				$update = $this->common_model->update($menuId, $updateData, $tableName="dashboard_menu");
				$redirectUrl = 'auth/dashboardMenuEdit/'.$menuId; //redirect url

				if ($update) {
					$message = 'Dashboard menu has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to update dashboard menu.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Delete status is updated from 0 to 1.
	 * Soft delete concept is implemented.
	 *
	 * @param int $menuId
	 * @return void
	 */
	public function dashboardMenuDelete($menuId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		$softDelete = $this->common_model->softDelete($menuId, $tableName='dashboard_menu');
		$redirectUrl = 'auth/manageMenu'; //redirect url

		if ($softDelete) {
			$message = 'Dashboard Menu has been deleted.';
			$this->session->set_flashdata('success_msg', $message);
			redirect($redirectUrl); 

		} else {
			$message = 'Failed to delete Dashboard Menu.';
			$this->session->set_flashdata('error_msg', $message);
			redirect($redirectUrl); 
		}
	}

	/**
	 * All the list of user is displayed.
	 * Management of user is done in this page.
	 *
	 * @return void
	 */
	public function manageUser()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 6;
		self::$viewData['title'] = "Manage Users";
		self::$viewData['breadcrumb'] = "Manage Users"; 
		self::$viewData['heading'] = "Users List";
		self::$viewData['usersList'] = $this->auth_model->getUsersList(); // join query
		// echo '<pre>';
		// print_r(self::$viewData['usersList']); exit;
		self::$viewData['page'] = "auth/user_management/user_list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * Form for adding new user is loaded.
	 * Validation and insert functionality is also carried out within this function.
	 * Both auth and user detail is added.
	 * User-group mapping is also done within this function.
	 *
	 * @return void
	 */
	public function addUser()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Manage User";
		self::$viewData['breadcrumb'] = "Manage User"; 
		self::$viewData['heading'] = "Add User";
		self::$viewData['userGroupList'] = $this->auth_model->getInternalUserGroupList();
		self::$viewData['page'] = "auth/user_management/add_user_form";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('user_group', ' ', 'required|trim');
			$this->form_validation->set_rules('first_name', ' ', 'required|trim');
			$this->form_validation->set_rules('last_name', ' ', 'required|trim');
			$this->form_validation->set_rules('gender', ' ', 'required|trim');
			$this->form_validation->set_rules('dob', ' ', 'required|trim');
			$this->form_validation->set_rules('address', ' ', 'required|trim');
			$this->form_validation->set_rules('contact', ' ', 'required|trim');
			$this->form_validation->set_rules('joined_date', ' ', 'required|trim');
			$this->form_validation->set_rules('email', ' ', 'required|trim|valid_email|is_unique[auth_user.email]',
				array('is_unique' => 'This email already exists.')
			);
			$this->form_validation->set_rules('username', ' ', 'required|trim|is_unique[auth_user.username]',
				array('is_unique' => 'This username already exists.')
			);
			$this->form_validation->set_rules('password', ' ', 'required|trim');

			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				// This data will be inserted in table 'auth_user'
				$insertAuthUserData = array(
					'email' => $this->input->post('email'),
					'username' => $this->input->post('username'),
					'password' => encrypt_password($this->input->post('password')),
					'created_by' => session_data('user_id'),
				);

				$this->db->trans_begin();

				// Here last inserted user_id is returned.
				$insertAuthUser = $this->auth_model->insertAuthUser($insertAuthUserData); 
				$userId = $insertAuthUser;

				// This data will be inserted in table 'user_detail'
				$insertUserDetailData = array(
					'user_id' => $userId,
					'firstname' => $this->input->post('first_name'),
					'lastname' => $this->input->post('last_name'),
					'gender' => $this->input->post('gender'),
					'dob' => $this->input->post('dob'),
					'address' => $this->input->post('address'),
					'pan_no' => $this->input->post('pan_no'),
					'blood_group' => $this->input->post('blood_group'),
					'contact' => $this->input->post('contact'),
					'joined_date' => $this->input->post('joined_date'),
					'created_by' => session_data('user_id'),
				);

				$insertUserDetail = $this->common_model->insert($insertUserDetailData, $tableName="user_detail");

				// This data will be inserted in table 'auth_user_group_mapping'
				$insertUserGroupMapData = array(
					'user_id' => $userId,
					'group_id' => $this->input->post('user_group'),
					'created_by' => session_data('user_id'),
				);

				$insertUserGroupMapping = $this->common_model->insert($insertUserGroupMapData, $tableName="auth_user_group_mapping");

				if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}

				//redirect url
				$redirectUrl = 'auth/manageUser'; 

				if ($insertAuthUser && $insertUserDetail && $insertUserGroupMapping) {
					$message = 'A user has been added.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to add user.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Form for updating existing user is loaded.
	 * Validation and update functionality is also carried out within this function.
	 * Both auth and user detail is updated.
	 * User-group mapping is also updated within this function.
	 *
	 * @param int $user_id
	 * @return void
	 */
	public function editUser($userId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Manage User";
		self::$viewData['breadcrumb'] = "Manage User"; 
		self::$viewData['heading'] = "Edit User Form";
		self::$viewData['detail'] = $this->auth_model->getUserDetailById($userId); // join query
		// print_r(self::$viewData['detail']); exit;
		self::$viewData['userGroupList'] = $this->auth_model->getInternalUserGroupList();
		self::$viewData['page'] = "auth/user_management/edit_user_form";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('user_group', ' ', 'required|trim');
			$this->form_validation->set_rules('first_name', ' ', 'required|trim');
			$this->form_validation->set_rules('last_name', ' ', 'required|trim');
			$this->form_validation->set_rules('gender', ' ', 'required|trim');
			$this->form_validation->set_rules('dob', ' ', 'required|trim');
			$this->form_validation->set_rules('address', ' ', 'required|trim');
			$this->form_validation->set_rules('contact', ' ', 'required|trim');
			$this->form_validation->set_rules('joined_date', ' ', 'required|trim');
			$this->form_validation->set_rules('email', ' ', 'required|trim|valid_email');
			$this->form_validation->set_rules('username', ' ', 'required|trim');
			$this->form_validation->set_rules('password', ' ', 'required|trim');
			$this->form_validation->set_rules('status', ' ', 'required|trim');

			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				$email = $this->input->post('email');
				$username = $this->input->post('username');

				//redirect url
				$redirectUrl = 'auth/editUser/'.$userId; 

				// Checking if email exists or not. If it does, provide an error message.
				$checkEmail = $this->auth_model->checkEmail($email, $userId);
				if ($checkEmail) {
					$message = 'Email already exists.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}

				// Checking if username exists or not. If it does, provide an error message.
				$checkUsername = $this->auth_model->checkUsername($username, $userId);
				if ($checkUsername) {
					$message = 'Username already exists.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}

				// This data will be updated in table 'auth_user'
				$updateAuthUserData = array(
					'email' => $email,
					'username' => $username,
					'password' => encrypt_password($this->input->post('password')),
					'active_status' => $this->input->post('status'),
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s'),
				);
				
				// This data will be updated in table 'user_detail'
				$updateUserDetailData = array(
					'user_id' => $userId,
					'firstname' => $this->input->post('first_name'),
					'lastname' => $this->input->post('last_name'),
					'gender' => $this->input->post('gender'),
					'dob' => $this->input->post('dob'),
					'address' => $this->input->post('address'),
					'pan_no' => $this->input->post('pan_no'),
					'blood_group' => $this->input->post('blood_group'),
					'contact' => $this->input->post('contact'),
					'joined_date' => $this->input->post('joined_date'),
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s'),
				);
				
				// This data will be updated in table 'auth_user_group_mapping'
				$updateUserGroupMapData = array(
					'group_id' => $this->input->post('user_group'),
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s'),
				);

				// $this->db->trans_begin();

				// updates user's auth data in table 'auth_user'
				$updateAuthUser = $this->common_model->update($userId, $updateAuthUserData, $tableName="auth_user");

				// checks if user main is updated or not
				if ($updateAuthUser) {
					// updates user's detail data in table 'user_detail'
					$updateUserDetail = $this->auth_model->updateUserDetail($updateUserDetailData, $userId);

					// checks if user detail is updated or not
					if ($updateUserDetail) {
						// updates user group mapping data in table 'auth_user_group_mapping'
						$updateUserGroupMap = $this->auth_model->updateUserGroupMap($updateUserGroupMapData, $userId);
						
						// checks if user group mapping is updated or not
						if ($updateUserGroupMap) {
							$message = 'User detail has been updated.';
							$this->session->set_flashdata('success_msg', $message);
							redirect($redirectUrl); 

						} else {
							// This data will be inserted in table 'auth_user_group_mapping'
							$insertUserGroupMapData = array(
								'user_id' => $userId,
								'group_id' => $user_group,
								'created_by' => session_data('user_id'),
							);
							$insertUserGroupMapping = $this->common_model->insert($insertUserGroupMapData, $tableName="auth_user_group_mapping");

							if ($insertUserGroupMapping) {
								$message = 'User detail has been updated.';
								$this->session->set_flashdata('success_msg', $message);
								redirect($redirectUrl); 

							} else {
								$message = 'Failed to update user detail.';
								$this->session->set_flashdata('error_msg', $message);
								redirect($redirectUrl); 
							}
						}

					} else {
						$message = 'Failed to update user detail.';
						$this->session->set_flashdata('error_msg', $message);
						redirect($redirectUrl); 
					}

				} else {
					$message = 'Failed to update user detail.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}

				// if ($this->db->trans_status() === FALSE) {
				// 	$this->db->trans_rollback();
				// } else {
				// 	$this->db->trans_commit();
				// }
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Delete status is updated from 0 to 1.
	 * Soft delete concept is implemented.
	 *
	 * @param int $user_id
	 * @return void
	 */
	public function deleteUser($userId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		$softDelete = $this->common_model->softDelete($userId, $tableName='auth_user');
		$redirectUrl = 'auth/manageUser'; //redirect url

		if ($softDelete) {
			$message = 'User has been deleted.';
			$this->session->set_flashdata('success_msg', $message);
			redirect($redirectUrl); 

		} else {
			$message = 'Failed to delete user.';
			$this->session->set_flashdata('error_msg', $message);
			redirect($redirectUrl); 
		}
	}

	/**
	 * All the list of user-group is displayed.
	 * Management of user-group is done in this page.
	 *
	 * @return void
	 */
	public function manageUserGroup()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 3; // this dashboard menu id is of manage user group menu. it is used for user authorization purpose (crud access)
		self::$viewData['title'] = "Manage User Group";
		self::$viewData['breadcrumb'] = "Manage User Group"; 
		self::$viewData['heading'] = "User Group List";
		self::$viewData['userGroupList'] = $this->auth_model->getUserGroupList();
		self::$viewData['page'] = "auth/user_management/user_group_list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * Form for adding new user-group is loaded.
	 * Validation and insert functionality is also carried out within this function.
	 *
	 * @return void
	 */
	public function addUserGroup()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Manage User Group";
		self::$viewData['breadcrumb'] = "Manage User Group"; 
		self::$viewData['heading'] = "Add User Group";
		self::$viewData['page'] = "auth/user_management/add_user_group_form";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('group_name', ' ', 'required|trim');
			$this->form_validation->set_rules('description', ' ', 'required|trim');

			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {

				$group_name = $this->input->post('group_name');
				$description = $this->input->post('description');

				$insertData = array(
					'group_name' => $group_name,
					'description' => $description,
					'created_by' => session_data('user_id'),
				);

				$insert = $this->common_model->insert($insertData, $tableName="auth_group");
				$redirectUrl = 'auth/manageUserGroup'; //redirect url

				if ($insert) {
					$message = 'User Group has been added.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to add User Group.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Form for updating existing user is loaded.
	 * Validation and update functionality is also carried out within this function.
	 *
	 * @param int $groupId
	 * @return void
	 */
	public function editUserGroup($groupId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Manage User Group";
		self::$viewData['breadcrumb'] = "Manage User Group"; 
		self::$viewData['heading'] = "Edit User Group";
		self::$viewData['detail'] = $this->auth_model->getUserGroupById($groupId);
		self::$viewData['page'] = "auth/user_management/edit_user_group_form";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('group_name', ' ', 'required|trim');
			$this->form_validation->set_rules('description', ' ', 'required|trim');

			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {

				$group_name = $this->input->post('group_name');
				$description = $this->input->post('description');
				$profile_update_access = $this->input->post('profile_update_access');
				$login_status = $this->input->post('login_status');
				// $status = $this->input->post('status');

				$updateData = array(
					'group_name' => $group_name,
					'description' => $description,
					'profile_update_access' => $profile_update_access,
					'login_status' => $login_status,
					// 'status' => $status,
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s'),
				);

				$update = $this->common_model->update($groupId, $updateData, $tableName="auth_group");
				$redirectUrl = 'auth/editUserGroup/'.$groupId; //redirect url

				if ($update) {
					$message = 'User Group has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirectUrl); 

				} else {
					$message = 'Failed to update data.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirectUrl); 
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Delete status is updated from 0 to 1.
	 * Soft delete concept is implemented.
	 *
	 * @param int $groupId
	 * @return void
	 */
	public function deleteUserGroup($groupId)
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		$softDelete = $this->common_model->softDelete($groupId, $tableName='auth_group');
		$redirectUrl = 'auth/manageUserGroup'; //redirect url

		if ($softDelete) {
			$message = 'User Group has been deleted.';
			$this->session->set_flashdata('success_msg', $message);
			redirect($redirectUrl); 

		} else {
			$message = 'Failed to delete User Group.';
			$this->session->set_flashdata('error_msg', $message);
			redirect($redirectUrl); 
		}
	}

	/**
	 * Menu access permission view page is loaded.
	 * Access permission is given based on user-group selected.
	 *
	 * @return void
	 */
	public function manageMenuAccess()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Manage Menu Access";
		self::$viewData['breadcrumb'] = "Manage Menu Access"; 
		self::$viewData['heading'] = "User Group/Menu Access";
		self::$viewData['userGroupList'] = $this->auth_model->getUserGroupList();
		self::$viewData['dashboardMenu'] = $dashboardMenu = $this->auth_model->getAllDashMenu();
		self::$viewData["userGroupId"] = null; // initially userGroupId is defined null
		self::$viewData['page'] = "auth/dashboard_menu/manage_menu_access";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('user_group', ' ', 'required|trim');
			$this->form_validation->set_error_delimiters('<span id="error_msg" style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				$group_id = $this->input->post('user_group');
				// print_r($this->input->post());exit;

				// loop on dashboard menu data to match with each checkbox name from form field.
				foreach ($dashboardMenu as $key => $menu) {
					$view_field = 'view_'.$menu->id;
					$add_field = 'add_'.$menu->id;
					$edit_field = 'edit_'.$menu->id;
					$delete_field = 'delete_'.$menu->id;
					
					$view_access = $this->input->post($view_field);
					$add_access = $this->input->post($add_field);
					$edit_access = $this->input->post($edit_field);
					$delete_access = $this->input->post($delete_field);

					$data[] = array(
						'group_id' => $group_id,
						'menu_id' => $menu->id,
						'view_access' => ($view_access) ? $view_access : 0,
						'add_access' => ($add_access) ? $add_access : 0,
						'edit_access' => ($edit_access) ? $edit_access : 0,
						'delete_access' => ($delete_access) ? $delete_access : 0,
					);
				}
				$count = count($data);

				for ($i=0; $i < $count; $i++) 
				{ 
					$group_id = $data[$i]['group_id'];
					$menu_id = $data[$i]['menu_id'];
					$view_access = $data[$i]['view_access'];
					$add_access = $data[$i]['add_access'];
					$edit_access = $data[$i]['edit_access'];
					$delete_access = $data[$i]['delete_access'];

					$checkMenuAccess = $this->auth_model->checkMenuAccess($group_id, $menu_id);
					if ($checkMenuAccess == false) {
						$insert_data = array(
							'group_id' => $group_id,
							'menu_id' => $menu_id,
							'view_access' => $view_access,
							'add_access' => $add_access,
							'edit_access' => $edit_access,
							'delete_access' => $delete_access,
							'created_by' => session_data('user_id'),
						);
						$this->common_model->insert($insert_data, $tableName="dashboard_menu_access");

					} else {
						$update_data = array(
							'view_access' => $view_access,
							'add_access' => $add_access,
							'edit_access' => $edit_access,
							'delete_access' => $delete_access,
							'updated_by' => session_data('user_id'),
							'updated_at' => date('Y-m-d H:i:s'),
						);
						$this->auth_model->updateMenuAccessLevel($group_id, $menu_id, $update_data);
					}
				}
				$redirect_url = 'auth/manageMenuAccess'; //redirect url
				$message = 'Menu Access Permission has been updated.';
				$this->session->set_flashdata('success_msg', $message);
				redirect($redirect_url); 
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * This function is executed on ajax call while updating menu access permission.
	 * On changing user-group select field, this function is called through ajax request.
	 * Menu access list is then shown accordingly through this function.
	 *
	 * @return void
	 */
	public function ajaxLoadMenuAccess()
	{
		try {
			if ($this->input->is_ajax_request()) {
				$userGroupId = $this->input->post("userGroupId"); 

				if ($userGroupId) {
					// self::$viewData["menuAccess"] = $this->auth_model->getDashboardMenuAccess($userGroupId);
					self::$viewData['dashboardMenu'] = $this->auth_model->getAllDashMenu();
					self::$viewData['userGroupId'] = $userGroupId;
					
					$menuAccessView = $this->load->view("auth/dashboard_menu/ajax_view/dashboard_menu_access", self::$viewData, TRUE);

					$response = array(
						"status" => "success",
						"menu_access_view" => $menuAccessView
					);
					header("Content-type: application/json");
					echo json_encode($response); exit;

				} else {
					$response = array(
						"status" => "error",
						"message" => "User Group field not selected."
					);
					header("Content-type: application/json");
					echo json_encode($response); exit;
				}
				
			} else {
				exit("No direct script allowed!");
			}

		} catch (Exception $e) {
			echo "Caught exception: ". $e->getMessage();
		}
	}

	/**
	 * This function logs out from current user session.
	 *
	 * @return void
	 */
	public function logout()
	{
		$redirect_path = "auth/login";
		$this->auth_model->authLogout($redirect_path);
	}

	







	// public function manage_menu_access_old()
	// {
	// 	// Checks if not logged in 
	// 	if (!isLoggedin()) {
	// 		redirect('auth/login');
	// 		exit;
	// 	}

	// 	self::$viewData['title'] = "Manage Menu Access";
	// 	self::$viewData['breadcrumb'] = "Manage Menu Access"; 
	// 	self::$viewData['heading'] = "User Group/Menu Access";
	// 	self::$viewData['userGroupList'] = $this->auth_model->getUserGroupList();
	// 	self::$viewData['allDashMenu'] = $this->auth_model->getAllDashMenu();
	// 	self::$viewData['page'] = "auth/user_management/manage_menu_access";

	// 	// Check whether the function is called through post method
	// 	if ($this->input->post()) {
	// 		$this->form_validation->set_rules('user_group', ' ', 'required|trim');
	// 		$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

	// 		if ($this->form_validation->run() == FALSE) {
	// 			// If validation is false, then return to the respective view page as defined above and show validation error. 
	// 			$this->load->view(TEMPL_ADMIN, self::$viewData);

	// 		} else {
	// 			$dash_menu = $this->auth_model->getAllDashMenu();
	// 			$group_id = $this->input->post('user_group');
	// 			// print_r($this->input->post());

	// 			foreach ($dash_menu as $key => $menu) {
	// 				$view_field = 'view_'.$menu->id;
	// 				$add_field = 'add_'.$menu->id;
	// 				$edit_field = 'edit_'.$menu->id;
	// 				$delete_field = 'delete_'.$menu->id;
					
	// 				$view_access = $this->input->post($view_field);
	// 				$add_access = $this->input->post($add_field);
	// 				$edit_access = $this->input->post($edit_field);
	// 				$delete_access = $this->input->post($delete_field);

	// 				$data = array(
	// 					'group_id' => $group_id,
	// 					'menu_id' => $menu->id,
	// 					'view_access' => ($view_access) ? $view_access : 0,
	// 					'add_access' => ($add_access) ? $add_access : 0,
	// 					'edit_access' => ($edit_access) ? $edit_access : 0,
	// 					'delete_access' => ($delete_access) ? $delete_access : 0,
	// 				);

	// 				$final_data[] = $data;

	// 			}

	// 			$count = count($final_data);

	// 			for ($i=0; $i < $count; $i++) 
	// 			{ 
	// 				$group_id = $final_data[$i]['group_id'];
	// 				$menu_id = $final_data[$i]['menu_id'];
	// 				$view_access = $final_data[$i]['view_access'];
	// 				$add_access = $final_data[$i]['add_access'];
	// 				$edit_access = $final_data[$i]['edit_access'];
	// 				$delete_access = $final_data[$i]['delete_access'];

	// 				$checkAccessLevel = $this->auth_model->checkAccessLevel($group_id, $menu_id);

	// 				if ($checkAccessLevel == false) {
	// 					$insert_data = array(
	// 						'group_id' => $group_id,
	// 						'menu_id' => $menu_id,
	// 						'view_access' => $view_access,
	// 						'add_access' => $add_access,
	// 						'edit_access' => $edit_access,
	// 						'delete_access' => $delete_access,
	// 						'created_by' => session_data('user_id'),
	// 					);
		
	// 					$this->auth_model->insertMenuAccessLevel($insert_data);

	// 				} else {
	// 					$update_data = array(
	// 						'view_access' => $view_access,
	// 						'add_access' => $add_access,
	// 						'edit_access' => $edit_access,
	// 						'delete_access' => $delete_access,
	// 						'updated_by' => session_data('user_id'),
	// 						'updated_at' => date('Y-m-d H:i:s'),
	// 					);

	// 					$this->auth_model->updateMenuAccessLevel($group_id, $menu_id, $update_data);
	// 				}
	// 			}

	// 			$redirect_url = 'auth/manage-menu-access'; //redirect url

	// 			$message = 'Menu Access Level has been updated.';
	// 			$this->session->set_flashdata('success_msg', $message);
	// 			redirect($redirect_url); 
	// 		}

	// 	} else {
	// 		$this->load->view(TEMPL_ADMIN, self::$viewData);
	// 	}
	// }



	// public function manage_menu_access_old2()
	// {
	// 	// Checks if not logged in 
	// 	if (!isLoggedin()) {
	// 		redirect('auth/login');
	// 		exit;
	// 	}

	// 	self::$viewData['title'] = "Manage Menu Access";
	// 	self::$viewData['breadcrumb'] = "Manage Menu Access"; 
	// 	self::$viewData['heading'] = "User Group/Menu Access";
	// 	self::$viewData['userGroupList'] = $this->auth_model->getUserGroupList();
	// 	self::$viewData['dashboardMenu'] = $this->auth_model->getAllDashMenu();
	// 	self::$viewData['page'] = "auth/user_management/manage_menu_access";

	// 	// Check whether the function is called through post method
	// 	if ($this->input->post()) {
	// 		$this->form_validation->set_rules('user_group', ' ', 'required|trim');
	// 		$this->form_validation->set_rules('menu', ' ', 'required|trim');
	// 		$this->form_validation->set_error_delimiters('<span id="error_msg" style="color: red; font-size: 14px">', '</span>');

	// 		if ($this->form_validation->run() == FALSE) {
	// 			// If validation is false, then return to the respective view page as defined above and show validation error. 
	// 			$this->load->view(TEMPL_ADMIN, self::$viewData);

	// 		} else {
	// 			$userGroupId = $this->input->post("user_group");
	// 			$menuId = $this->input->post("menu");
	// 			$viewAccess = $this->input->post("view");
	// 			$addAccess = $this->input->post("add");
	// 			$editAccess = $this->input->post("edit");
	// 			$deleteAccess = $this->input->post("delete");

	// 			$checkMenuAccess = $this->auth_model->checkMenuAccess($userGroupId, $menuId);
	// 			if ($checkMenuAccess == false) {
	// 				$insertData = array(
	// 					'group_id' => $userGroupId,
	// 					'menu_id' => $menuId,
	// 					'view_access' => $viewAccess ? $viewAccess : 0,
	// 					'add_access' => $addAccess ? $addAccess : 0,
	// 					'edit_access' => $editAccess ? $editAccess : 0,
	// 					'delete_access' => $deleteAccess ? $deleteAccess : 0,
	// 					'created_by' => session_data('user_id'),
	// 				);

	// 				$this->auth_model->insertMenuAccessLevel($insertData);

	// 			} else {
	// 				$updateData = array(
	// 					'view_access' => $viewAccess ? $viewAccess : 0,
	// 					'add_access' => $addAccess ? $addAccess : 0,
	// 					'edit_access' => $editAccess ? $editAccess : 0,
	// 					'delete_access' => $deleteAccess ? $deleteAccess : 0,
	// 					'updated_by' => session_data('user_id'),
	// 					'updated_at' => date('Y-m-d H:i:s'),
	// 				);
	// 				$this->auth_model->updateMenuAccessLevel($userGroupId, $menuId, $updateData);
	// 			}

	// 			$redirect_url = 'auth/manage-menu-access'; //redirect url
	// 			$message = 'Menu Access Level has been updated.';
	// 			$this->session->set_flashdata('success_msg', $message);
	// 			redirect($redirect_url); 
	// 		}

	// 	} else {
	// 		$this->load->view(TEMPL_ADMIN, self::$viewData);
	// 	}
	// }



	// public function ajaxLoadMenuAccess_old2()
	// {
	// 	try {
	// 		if ($this->input->is_ajax_request()) {
	// 			$userGroupId = $this->input->post("userGroupId"); 
	// 			$menuId = $this->input->post("menuId"); 

	// 			if ($userGroupId && $menuId) {
	// 				self::$viewData["menuAccess"] = $this->auth_model->getDashboardMenuAccess($userGroupId, $menuId);

	// 				self::$viewData["menuDetail"] = $this->auth_model->menuDetailById($menuId);
					
	// 				$menuAccessView = $this->load->view("admin/ajax_view/dashboard_menu_access", self::$viewData, TRUE);

	// 				$response = array(
	// 					"status" => "success",
	// 					"menu_access_view" => $menuAccessView
	// 				);
	// 				header("Content-type: application/json");
	// 				echo json_encode($response); exit;

	// 			} else {
	// 				$response = array(
	// 					"status" => "error",
	// 					"message" => "Both user-group and menu needs to be selected."
	// 				);
	// 				header("Content-type: application/json");
	// 				echo json_encode($response); exit;
	// 			}
				
	// 		} else {
	// 			exit("No direct script allowed!");
	// 		}

	// 	} catch (Exception $e) {
	// 		echo "Caught exception: ". $e->getMessage();
	// 	}
	// }

	
}