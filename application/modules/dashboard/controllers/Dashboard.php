<?php 

if( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * All the functionality related CMS, User Management and other backend functions are implemented in this class named 'Admin'.
 */
class Dashboard extends MX_Controller
{
    /*
	 *	variable to hold data for view
	 */
	private static $viewData = array();
	
	public function __construct()
	{
		parent::__construct();

		// To show the username in admin dashboard of the logged in user
		if (isLoggedin()) { // uri_segment(2) && uri_segment(2) != 'login'
			self::$viewData['username'] = $this->auth_model->getAuthUser()->username;
		}

		// Dashboard side menu
		self::$viewData['dashboardSideMenu'] = $this->auth_model->getDashboardSideMenu();

		// Checks if the auth active status, auth temp_status and user group login_status is 0 or not. If one of them is 0, then user is forced to logout of the system.
		if (isLoggedin() && (($this->auth_model->getAuthUser()->active_status == 0) || ($this->auth_model->getAuthUser()->temp_status == 0) || ($this->auth_model->getUserGroupById(session_data('group_id'))->login_status == 0)) ) 
		{
			$this->logout(); // force logout
		}
	}

	public function index()
	{
		// echo date('H:i:s'); exit;
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('auth/login');
			exit;
		}

		self::$viewData['title'] = "Dashboard";
		self::$viewData['breadcrumb'] = "Dashboard"; 
		self::$viewData['page'] = "dashboard";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

}