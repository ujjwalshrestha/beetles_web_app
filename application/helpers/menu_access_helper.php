<?php 

if( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( !function_exists('viewAccess') ) {
    /**
	 * View access of menu (menuId) for respective user-group (groupId).
	 * groupId in second param comes from dashboard_menu_access view page.
	 *
	 * @param int $menuId
	 * @param int $groupId
	 * @return void
	 */
	function viewAccess($menuId, $groupId = null)
	{
        $CI =& get_instance();

		if ($groupId == null) {
			$groupId = session_data('group_id');
		}
        
        $where = array(
			'group_id' => $groupId,
			'menu_id' => $menuId
		);

		$result = $CI->db->get_where('dashboard_menu_access', $where);

		if ($result->num_rows() == 1) {
			return $result->row()->view_access;

		} else {
			return false;
		}
	}
}

if ( !function_exists('addAccess') ) {
    /**
	 * Add access of menu (menuId) for respective user-group (groupId).
	 * groupId in second param comes from dashboard_menu_access view page.
	 *
	 * @param int $menuId
	 * @param int $groupId
	 * @return void
	 */
	function addAccess($menuId, $groupId = null)
	{
        $CI =& get_instance();

		if ($groupId == null) {
			$groupId = session_data('group_id');
		}
        
        $where = array(
			'group_id' => $groupId,
			'menu_id' => $menuId
		);

		$result = $CI->db->get_where('dashboard_menu_access', $where);

		if ($result->num_rows() == 1) {
			return $result->row()->add_access;

		} else {
			return false;
		}
	}
}

if ( !function_exists('editAccess') ) {
    /**
	 * Edit access of menu (menuId) for respective user-group (groupId).
	 * groupId in second param comes from dashboard_menu_access view page.
	 *
	 * @param int $menuId
	 * @param int $groupId
	 * @return void
	 */
	function editAccess($menuId, $groupId = null)
	{
        $CI =& get_instance();

		if ($groupId == null) {
			$groupId = session_data('group_id');
		}
        
        $where = array(
			'group_id' => $groupId,
			'menu_id' => $menuId
		);

		$result = $CI->db->get_where('dashboard_menu_access', $where);

		if ($result->num_rows() == 1) {
			return $result->row()->edit_access;

		} else {
			return false;
		}
	}
}

if ( !function_exists('deleteAccess') ) {
    /**
	 * Delete access of menu (menuId) for respective user-group (groupId).
	 * groupId in second param comes from dashboard_menu_access view page.
	 *
	 * @param int $menuId
	 * @param int $groupId
	 * @return void
	 */
	function deleteAccess($menuId, $groupId = null)
	{
        $CI =& get_instance();

		if ($groupId == null) {
			$groupId = session_data('group_id');
		}
        
        $where = array(
			'group_id' => $groupId,
			'menu_id' => $menuId
		);

		$result = $CI->db->get_where('dashboard_menu_access', $where);

		if ($result->num_rows() == 1) {
			return $result->row()->delete_access;

		} else {
			return false;
		}
	}
}