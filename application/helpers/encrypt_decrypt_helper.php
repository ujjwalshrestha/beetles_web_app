<?php 

if( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( !function_exists('encrypt_password') ) {
    /**
     * This function is mainly used for login password encryption.
     *
     * @param string $string
     * @return string
     */
    function encrypt_password($string)
    {
        $CI =& get_instance();
        $encode = $CI->encrypt->encode($string);
        $encrypt = $CI->encryption->encrypt($encode);
        return $encrypt;
    }
}


if ( !function_exists('decrypt_password') ) {
    /**
     * This function is mainly used for login password decryption.
     *
     * @param string $string
     * @return string
     */
    function decrypt_password($string)
    {
        $CI =& get_instance();
        // $main_string = substr($string, 10);
        $decrypt = $CI->encryption->decrypt($string); // $main_string
		$decode = $CI->encrypt->decode($decrypt);
		return $decode;
    }
}

if ( !function_exists('_encrypt') ) {
    /**
     * Through this function, any normal string is ecrypted. Here, the string is first encoded and then encrypted. This is just for test purpose.
     *
     * @param string $string
     * @return string
     */
    function _encrypt($string)
    {
        $CI =& get_instance();
        $encode = $CI->encrypt->encode($string);
        $encrypt = $CI->encryption->encrypt($encode);
        return $encrypt;
    }
}


if ( !function_exists('_decrypt') ) {
    /**
     * Through this function, any encrypted string is decrypted. Here, the string is first decrypted and then decoded. This is just for test purpose.
     *
     * @param string $string
     * @return string
     */
    function _decrypt($string)
    {
        $CI =& get_instance();
        $decrypt = $CI->encryption->decrypt($string);
		$decode = $CI->encrypt->decode($decrypt);
		return $decode;
    }
}


if ( !function_exists('_encode') ) {
    /**
     * Through this function, any normal string is encoded. This is just for test purpose.
     *
     * @param string $string
     * @return string
     */
    function _encode($string)
    {
        $CI =& get_instance();
        $encode = $CI->encrypt->encode($string);
        return $encode;
    }
}


if ( !function_exists('_decode') ) {
    /**
     * Through this function, any encoded string is decoded. This is just for test purpose.
     *
     * @param string $string
     * @return string
     */
    function _decode($string)
    {
        $CI =& get_instance();
        $decode = $CI->encrypt->decode($string);
        return $decode;
    }
}

if ( !function_exists('hashCode') ) {
    /**
     * This function generates a hashcode. Here, first a random string is generated of length 10. The length of the string can be determined as required by passing the required no. of length through param of this function. The length will then be passed through function generateRandomString() where the string is generated. Then, the generated string is passed and encrypted through function _encrypt(), resulting to a hashcode.
     *
     * @param integer $strlen
     * @return string
     */
    function hashCode($strlen = 10) 
    {
        $randomStr = generateRandomString($strlen); // generates random string of length 10
        $hashCode = _encrypt($randomStr); // encrypts the generated random string as hashcode
        return strtr($hashCode, array('+' => '.', '=' => '-', '/' => '~'));
    }
}

if ( !function_exists('urlEncrypt') ) {
    /**
     * Here, string passed through the param is encrypted. From the encrypted string, some of the characters are replaced to make it applicable for URL.
     *
     * @param string $string
     * @return string
     */
    function urlEncrypt($string) {
        $CI =& get_instance();
        $enc_string = $CI->encryption->encrypt($string);
        return strtr($enc_string, array('+' => '.', '=' => '-', '/' => '~')); // replacing characters
    }
}

if ( !function_exists('urlDecrypt') ) {
    /**
     * Here, encrypted string passed through the param is decrypted. From the encrypted string, some of the characters are replaced and then decrypted.
     *
     * @param string $string
     * @return string
     */
    function urlDecrypt($enc_string) {
        $CI =& get_instance();
        $enc_string = strtr($enc_string, array('.' => '+', '-' => '=', '~' => '/')); // replacing characters
        return $CI->encryption->decrypt($enc_string);
    }
}