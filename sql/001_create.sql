-- auth_user
-- Table for maintaining data related to user session/authentication
-- By Ujjwal Shrestha (2020-12-21)
CREATE TABLE auth_user (
    id int PRIMARY KEY auto_increment,
    ip_address varchar(50),
    username varchar(50) NOT NULL,
    password varchar(255) NOT NULL,
    email varchar(100) NOT NULL,
    activation_code varchar(100) DEFAULT NULL,
    forgot_password_code varchar(100) DEFAULT NULL,
    forgot_password_datetime timestamp NULL DEFAULT NULL,
    temp_status tinyint DEFAULT 1,
    active_status tinyint DEFAULT 1,
    last_login timestamp NULL DEFAULT NULL,
    is_super_admin int(2) DEFAULT 0,
    delete_status int(2) DEFAULT 0,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL,
    deleted_by int DEFAULT NULL,
    deleted_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- user_detail
-- Table for maintaining details of users
-- By Ujjwal Shrestha (2020-12-21)
CREATE TABLE user_detail (
    id int PRIMARY KEY auto_increment,
    user_id int NOT NULL,
    FOREIGN KEY (user_id) REFERENCES auth_user(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    firstname varchar(100),
    lastname varchar(100),
    dob date DEFAULT NULL,
    address varchar(200) DEFAULT NULL,
    gender varchar(2) NOT NULL,
    contact varchar(20),
    pan_no varchar(100) DEFAULT NULL,
    blood_group varchar(10) DEFAULT NULL,
    position varchar(100) DEFAULT NULL,
    joined_date date NULL DEFAULT NULL,
    profile_image varchar(255) DEFAULT NULL,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL  
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- auth_group
-- Table for maintaining user's groups
-- By Ujjwal Shrestha (2020-12-21)
CREATE TABLE auth_group (
    id int PRIMARY KEY auto_increment,
    group_name varchar(100),
    description text DEFAULT NULL, 
    login_status int(2) DEFAULT 1,
    status int(2) DEFAULT 1,
    delete_status int(2) DEFAULT 0,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL,
    deleted_by int DEFAULT NULL,
    deleted_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- auth_user_group_mapping
-- Table for mapping users and its respective group.
-- By Ujjwal Shrestha (2020-12-21)
CREATE TABLE auth_user_group_mapping (
    id int PRIMARY KEY auto_increment,
    user_id int NOT NULL,
    FOREIGN KEY (user_id) REFERENCES auth_user(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    group_id int NOT NULL,
    FOREIGN KEY (group_id) REFERENCES auth_group(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- dashboard_menu
-- Table for maintaining menu of dashboard
-- By Ujjwal Shrestha (2020-12-21)
CREATE TABLE dashboard_menu (
    id int PRIMARY KEY auto_increment,
    menu_type varchar(2) NOT NULL,
    parent_menu_id int DEFAULT NULL,
    FOREIGN KEY (parent_menu_id) REFERENCES dashboard_menu(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    menu_name varchar(100) NOT NULL,
    slug varchar(150) NOT NULL,
    menu_uri varchar(100) DEFAULT '#',
    menu_icon varchar(30) NOT NULL,
    order_by int(5),
    status int(2) DEFAULT 1,
    delete_status int(2) DEFAULT 0,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL,
    deleted_by int DEFAULT NULL,
    deleted_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- dashboard_menu_access
-- Table for determining access level of dashboard menu
-- By Ujjwal Shrestha (2020-12-21)
CREATE TABLE dashboard_menu_access (
    id int PRIMARY KEY auto_increment,
    group_id int NOT NULL,
    FOREIGN KEY (group_id) REFERENCES auth_group(id)
    ON DELETE CASCADE,
    menu_id int NOT NULL,
    FOREIGN KEY (menu_id) REFERENCES dashboard_menu(id)
    ON DELETE CASCADE,
    view_access tinyint DEFAULT NULL,
    add_access tinyint DEFAULT NULL,
    edit_access tinyint DEFAULT NULL,
    delete_access tinyint DEFAULT NULL,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- product_category_one
-- Table for product category one
-- By Ujjwal Shrestha (2021-02-26)
CREATE TABLE product_category_one (
    id int PRIMARY KEY auto_increment,
    category_name varchar(150) NOT NULL,
    delete_status int(2) DEFAULT 0,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL,
    deleted_by int DEFAULT NULL,
    deleted_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- product_category_two
-- Table for product category two
-- By Ujjwal Shrestha (2021-02-26)
CREATE TABLE product_category_two (
    id int PRIMARY KEY auto_increment,
    prod_cat_one_id int NOT NULL,
    FOREIGN KEY (prod_cat_one_id) REFERENCES product_category_one(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    category_name varchar(150) NOT NULL,
    box_no varchar(200) DEFAULT NULL,
    delete_status int(2) DEFAULT 0,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL,
    deleted_by int DEFAULT NULL,
    deleted_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- product_category_three
-- Table for product category three
-- By Ujjwal Shrestha (2021-02-26)
CREATE TABLE product_category_three (
    id int PRIMARY KEY auto_increment,
    prod_cat_two_id int NOT NULL,
    FOREIGN KEY (prod_cat_two_id) REFERENCES product_category_two(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    category_name varchar(150) NOT NULL,
    reorder_level int(5) DEFAULT NULL,
    max_stock_level int(5) DEFAULT NULL,
    delete_status int(2) DEFAULT 0,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL,
    deleted_by int DEFAULT NULL,
    deleted_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- supplier
-- Table for suppliers detail
-- By Ujjwal Shrestha (2021-02-26)
CREATE TABLE supplier (
    id int PRIMARY KEY auto_increment,
    name varchar(200) NOT NULL,
    address varchar(200) NOT NULL,
    pan_vat varchar(200) DEFAULT NULL,
    contact varchar(200) NOT NULL,
    email varchar(200) NOT NULL,
    delete_status int(2) DEFAULT 0,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL,
    deleted_by int DEFAULT NULL,
    deleted_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- product
-- Table for product detail
-- By Ujjwal Shrestha (2021-02-26)
CREATE TABLE product (
    id int PRIMARY KEY auto_increment,
    supplier_id int NOT NULL,
    FOREIGN KEY (supplier_id) REFERENCES supplier(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    product_category_id int NOT NULL,
    FOREIGN KEY (product_category_id) REFERENCES product_category_three(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    sku varchar(200) NOT NULL,
    product_name varchar(200) DEFAULT NULL,
    product_image varchar(255) DEFAULT NULL,
    size varchar(100) DEFAULT NULL,
    color varchar(100) DEFAULT NULL,
    unit_price float(10, 2) NOT NULL,
    stock_quantity int DEFAULT 0,
    packaging_type varchar(200) DEFAULT NULL,
    arrival_date date NOT NULL,
    product_image varchar(255) DEFAULT NULL,
    barcode varchar(255) DEFAULT NULL,
    delete_status int(2) DEFAULT 0,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL,
    deleted_by int DEFAULT NULL,
    deleted_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- company_detail
-- Table for company detail
-- By Ujjwal Shrestha (2021-02-26)
CREATE TABLE company_detail (
    id int PRIMARY KEY auto_increment,
    company_name varchar(200) NOT NULL,
    pan_vat varchar(200) NOT NULL,
    address varchar(200) NOT NULL,
    contact varchar(200) NOT NULL,
    email varchar(200) DEFAULT NULL,
    service_charge float(3, 2) DEFAULT NULL,
    vat_charge float(3, 2) DEFAULT NULL,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- attendance_status
-- Table for attendance status detail
-- By Ujjwal Shrestha (2021-03-08)
CREATE TABLE attendance_status (
    id int PRIMARY KEY auto_increment,
    status_name varchar(200) NOT NULL,
    reason_status int(2) DEFAULT 0,
    bg_color_class varchar(50) NOT NULL,
    delete_status int(2) DEFAULT 0,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL,
    deleted_by int DEFAULT NULL,
    deleted_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- attendance
-- Table for attendance detail
-- By Ujjwal Shrestha (2021-03-08)
CREATE TABLE attendance (
    id int PRIMARY KEY auto_increment,
    user_id int NOT NULL,
    FOREIGN KEY (user_id) REFERENCES auth_user(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    attendance_status_id int NOT NULL,
    FOREIGN KEY (user_id) REFERENCES auth_user(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    reason text DEFAULT NULL,
    attendance_datetime timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- user_bank_detail
-- Table for user bank detail
-- By Ujjwal Shrestha (2021-03-12)
CREATE TABLE user_bank_detail (
    id int PRIMARY KEY auto_increment,
    user_id int NOT NULL,
    FOREIGN KEY (user_id) REFERENCES auth_user(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    bank_name varchar(255) NOT NULL,
    account_name varchar(255) NOT NULL,
    account_number varchar(255) NOT NULL,
    delete_status int(2) DEFAULT 0,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL,
    deleted_by int DEFAULT NULL,
    deleted_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;

-- wholesale_customer
-- Table for maintaining details of wholesale customers
-- By Ujjwal Shrestha (2021-03-12)
CREATE TABLE wholesale_customer (
    id int PRIMARY KEY auto_increment,
    user_id int NOT NULL,
    FOREIGN KEY (user_id) REFERENCES auth_user(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    name varchar(255),
    address varchar(255) DEFAULT NULL,
    pan_vat varchar(255),
    contact varchar(20),
    profile_image varchar(255) DEFAULT NULL,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL  
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;

-- wholesale_order
-- Table for maintaining wholesale customer's orders
-- By Ujjwal Shrestha (2021-03-22)
CREATE TABLE wholesale_order (
    id int PRIMARY KEY auto_increment,
    order_no int(11) NOT NULL,
    customer_id int NOT NULL,
    FOREIGN KEY (customer_id) REFERENCES wholesale_customer(user_id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    order_datetime timestamp NOT NULL,
    gross_amount float(10, 2) NOT NULL,
    service_charge_rate varchar(2) DEFAULT 0,
    service_charge float(10, 2) DEFAULT NULL,
    vat_charge_rate varchar(2) DEFAULT 0,
    vat_charge float(10, 2) DEFAULT NULL,
    delivery_charge varchar(10) DEFAULT NULL,
    discount_rate varchar(2) DEFAULT NULL,
    discount varchar(10) DEFAULT NULL,
    net_amount float(10, 2) NOT NULL,
    special_instruction text DEFAULT NULL,
    delivery_date date DEFAULT NULL,
    dispatched_date date DEFAULT NULL,
    order_status int(2) DEFAULT 0,
    remarks text DEFAULT NULL,
    delete_status int(2) DEFAULT 0,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL,  
    deleted_by int DEFAULT NULL,
    deleted_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;

-- wholesale_order_item
-- Table for maintaining details of wholesale_order_item
-- By Ujjwal Shrestha (2021-03-12)
CREATE TABLE wholesale_order_item (
    id int PRIMARY KEY auto_increment,
    order_id int NOT NULL,
    FOREIGN KEY (order_id) REFERENCES wholesale_order(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    product_id int NOT NULL,
    FOREIGN KEY (product_id) REFERENCES product(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    quantity int(10) NOT NULL,
    rate float(10, 2) NOT NULL,
    total_amount float(10, 2) NOT NULL  
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- retail_customer
-- Table for maintaining details of retail customers
-- By Ujjwal Shrestha (2021-04-07)
CREATE TABLE retail_customer (
    id int PRIMARY KEY auto_increment,
    name varchar(255),
    address varchar(255) DEFAULT NULL,
    contact varchar(20) NOT NULL,
    email varchar(100) NOT NULL,
    delete_status int(2) DEFAULT 0,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL,
    deleted_by int DEFAULT NULL,
    deleted_at timestamp NULL DEFAULT NULL  
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;

-- retail_order
-- Table for maintaining retail customer's orders
-- By Ujjwal Shrestha (2021-04-08)
CREATE TABLE retail_order (
    id int PRIMARY KEY auto_increment,
    order_no int(11) NOT NULL,
    customer_id int NOT NULL,
    FOREIGN KEY (customer_id) REFERENCES retail_customer(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    order_datetime timestamp NOT NULL,
    gross_amount float(10, 2) NOT NULL,
    service_charge_rate varchar(2) DEFAULT 0,
    service_charge float(10, 2) DEFAULT NULL,
    vat_charge_rate varchar(2) DEFAULT 0,
    vat_charge float(10, 2) DEFAULT NULL,
    delivery_charge varchar(10) DEFAULT NULL,
    discount_rate varchar(2) DEFAULT NULL,
    discount varchar(10) DEFAULT NULL,
    net_amount float(10, 2) NOT NULL,
    special_instruction text DEFAULT NULL,
    delivery_date date DEFAULT NULL,
    dispatched_date date DEFAULT NULL,
    order_status int(2) DEFAULT 0,
    remarks text DEFAULT NULL,
    delete_status int(2) DEFAULT 0,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL,  
    deleted_by int DEFAULT NULL,
    deleted_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;

-- retail_order_item
-- Table for maintaining details of retail_order_item
-- By Ujjwal Shrestha (2021-03-12)
CREATE TABLE retail_order_item (
    id int PRIMARY KEY auto_increment,
    order_id int NOT NULL,
    FOREIGN KEY (order_id) REFERENCES retail_order(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    product_id int NOT NULL,
    FOREIGN KEY (product_id) REFERENCES product(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    quantity int(10) NOT NULL,
    rate float(10, 2) NOT NULL,
    total_amount float(10, 2) NOT NULL  
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;

-- user_subscription
-- Table for user subscription email
-- By Ujjwal Shrestha (2021-01-20)
CREATE TABLE user_subscription (
    id int PRIMARY KEY auto_increment,
    email varchar(150) NOT NULL,
    hash_code varchar(255) NOT NULL,
    status tinyint DEFAULT 1,
    subscribed_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    unsubscribed_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;