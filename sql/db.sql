-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 31, 2021 at 01:17 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beetles_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `attendance_status_id` int(11) NOT NULL,
  `reason` text DEFAULT NULL,
  `attendance_datetime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `user_id`, `attendance_status_id`, `reason`, `attendance_datetime`) VALUES
(6, 14, 1, NULL, '2021-03-30 16:53:22'),
(7, 15, 1, NULL, '2021-03-30 09:06:09'),
(8, 15, 2, 'sick', '2021-03-29 09:07:02'),
(9, 15, 1, NULL, '2021-04-12 11:56:42');

-- --------------------------------------------------------

--
-- Table structure for table `attendance_status`
--

CREATE TABLE `attendance_status` (
  `id` int(11) NOT NULL,
  `status_name` varchar(200) NOT NULL,
  `reason_status` int(2) DEFAULT 0,
  `bg_color_class` varchar(50) NOT NULL,
  `time_limit` smallint(6) DEFAULT NULL,
  `delete_status` int(2) DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attendance_status`
--

INSERT INTO `attendance_status` (`id`, `status_name`, `reason_status`, `bg_color_class`, `time_limit`, `delete_status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES
(1, 'In Work', 0, 'bg-success', 1, 0, 1, '2021-03-08 08:07:01', NULL, NULL, NULL, NULL),
(2, 'On Leave', 1, 'bg-danger', 0, 0, 1, '2021-03-08 08:08:31', NULL, NULL, NULL, NULL),
(3, 'Late', 1, 'bg-warning', 0, 0, 1, '2021-03-08 08:09:04', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `group_name` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `profile_update_access` int(2) DEFAULT 1,
  `login_status` int(2) DEFAULT 1,
  `status` int(2) DEFAULT 1,
  `delete_status` int(2) DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_group`
--

INSERT INTO `auth_group` (`id`, `group_name`, `description`, `profile_update_access`, `login_status`, `status`, `delete_status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES
(1, 'admin', 'Administration', 1, 1, 1, 0, 0, '2021-02-17 07:22:12', NULL, NULL, NULL, NULL),
(8, 'customer', 'Customer', 1, 1, 1, 0, 1, '2021-03-16 08:58:42', 1, '2021-03-16 14:13:26', 1, '2021-03-30 13:49:23'),
(9, 'supervisors', 'Supervisor', 0, 0, 1, 0, 1, '2021-03-30 14:46:03', 1, '2021-03-31 08:08:33', NULL, NULL),
(10, 'editor', 'Editor', 1, 1, 1, 0, 1, '2021-03-31 08:09:02', NULL, NULL, 1, '2021-03-31 08:09:24');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `activation_code` varchar(100) DEFAULT NULL,
  `forgot_password_code` varchar(100) DEFAULT NULL,
  `forgot_password_datetime` timestamp NULL DEFAULT NULL,
  `temp_status` tinyint(4) DEFAULT 1,
  `active_status` tinyint(4) DEFAULT 1,
  `last_login` timestamp NULL DEFAULT NULL,
  `is_super_admin` int(2) DEFAULT 0,
  `delete_status` int(2) DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `ip_address`, `username`, `password`, `email`, `activation_code`, `forgot_password_code`, `forgot_password_datetime`, `temp_status`, `active_status`, `last_login`, `is_super_admin`, `delete_status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES
(1, '::1', 'superadmin', '44a58c0ca17b2fe281a62136875b0bc3220b61bac2e87e2b442a4a3b6d9c8f6607405759a0fbb6572ddc39d0191084c4f5b29151518dfbd5a994cc1a167a08der9ZdIi1PHdyh92MHnyVz70Sf+97Chg89UP4zgiFpvHLdP8MWwtqirD9bTK8170ye', 'admin@gmail.com', NULL, NULL, NULL, 1, 1, '2021-10-31 04:55:28', 1, 0, 1, '2019-07-24 04:08:02', 1, '2021-03-16 06:57:34', NULL, NULL),
(14, '::1', 'ujjwal', '5b0d4e13bbd5d4b8f625231657a5601e5ece47945903a0575123b65e42a113fa2637966a733adb57936afee723aab4b6b1edcc19dec0c4d4c09dfe70951046c9gNSLd0CicotF6BhV0YKaNa4+A+bV+wXXutK8Br1R22+B8/elaXBkOuATv9WTyP94', 'ujjwalshrestha1996@gmail.com', NULL, NULL, NULL, 1, 1, '2021-03-30 16:52:39', 0, 0, 1, '2021-03-30 16:34:58', 1, '2021-03-30 16:48:35', NULL, NULL),
(15, '::1', 'savson', '61124adaf95ebfc6153be97f2e4fc2bc2d56dfd88b27bd27b18e74dd3a3a2e2818d0d195e5655c4afa8a93c3dfa1d0be414b8d95e5e9be034f5a99b8977f55d7E68kUzBEpleyLJbLjyyW0ztr4xqgiE4NcU9vXvY0Scy9NvH3cpzwll7ZS0kS1nLP', 'a@gmail.com', NULL, NULL, NULL, 1, 1, '2021-04-12 10:50:11', 0, 0, 1, '2021-03-31 08:12:32', 1, '2021-03-31 08:13:21', NULL, NULL),
(16, '::1', 'nike', 'c451d23dc977953faf03353d6bcece6e19fab2be4e377d0ff39b2f46d48ff19188a557cdc95b09c8171db1df101917303945b0137862550def3cc72949c49968lXUdVkSpNwXsYgHkfu/jTdwCH+lJZOb2BxRq+8rdr3Oy/ZJRtafjKzLJxF57mjis', 'n@gmail.com', NULL, NULL, NULL, 1, 1, '2021-03-31 08:18:06', 0, 0, 1, '2021-03-31 08:17:52', NULL, NULL, NULL, NULL),
(18, NULL, 'adidas', 'cfd3b86fb50a0d2b9e3750dc53630603c3b217145264c5fbef3782ef6f49d1f11f92e09c82852ac75919598bd32fb5fd239fffa1d97e4925f4ce205d3a8167e8n0SWUgqtvXelsFfgPoOjpHDhgzrRDJRTy3NZm3QExB+05+7aRGqhoGh97PCoBb6/', 'ad@gmail.com', NULL, NULL, NULL, 1, 1, NULL, 0, 0, 1, '2021-04-05 14:29:16', 1, '2021-04-06 00:54:19', 1, '2021-04-07 16:56:58');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_group_mapping`
--

CREATE TABLE `auth_user_group_mapping` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_user_group_mapping`
--

INSERT INTO `auth_user_group_mapping` (`id`, `user_id`, `group_id`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 1, 1, '2019-10-17 12:01:38', NULL, NULL),
(13, 14, 9, 1, '2021-03-30 16:34:58', 1, '2021-03-30 16:48:35'),
(14, 15, 10, 1, '2021-03-31 08:12:32', 1, '2021-03-31 08:13:21'),
(15, 16, 8, 1, '2021-03-31 08:17:52', NULL, NULL),
(16, 18, 8, 1, '2021-04-05 14:29:17', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company_detail`
--

CREATE TABLE `company_detail` (
  `id` int(11) NOT NULL,
  `company_name` varchar(200) NOT NULL,
  `pan_vat` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `contact` varchar(200) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `service_charge` varchar(10) DEFAULT NULL,
  `vat_charge` varchar(10) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_detail`
--

INSERT INTO `company_detail` (`id`, `company_name`, `pan_vat`, `address`, `contact`, `email`, `service_charge`, `vat_charge`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'Beetles International Traders', '-', '-', '-', '-', '0', '13', 1, '2021-03-01 09:31:11', 1, '2021-03-31 09:16:13');

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_menu`
--

CREATE TABLE `dashboard_menu` (
  `id` int(11) NOT NULL,
  `menu_type` varchar(2) NOT NULL,
  `parent_menu_id` int(11) DEFAULT NULL,
  `menu_name` varchar(100) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `menu_uri` varchar(100) DEFAULT '#',
  `menu_icon` varchar(30) NOT NULL,
  `order_by` int(5) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `delete_status` int(2) DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dashboard_menu`
--

INSERT INTO `dashboard_menu` (`id`, `menu_type`, `parent_menu_id`, `menu_name`, `slug`, `menu_uri`, `menu_icon`, `order_by`, `status`, `delete_status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES
(1, 's', NULL, 'Dashboard', 'dashboard', 'dashboard', 'fas fa-tachometer-alt', 1, 1, 0, 1, '2019-09-06 06:31:06', 1, '2021-03-31 08:16:31', NULL, NULL),
(2, 'p', NULL, 'User Management', 'user-management', '#', 'fas fa-users', 2, 1, 0, 1, '2019-09-06 08:40:20', NULL, NULL, NULL, NULL),
(3, 'c', 2, 'Manage User Group', 'manage-user-group', 'auth/manageUserGroup', 'far fa-circle', 1, 1, 0, 1, '2019-09-06 10:42:15', 1, '2021-02-06 14:23:12', NULL, NULL),
(4, 'p', NULL, 'Dashboard Menu', 'dashboard-menu', '#', 'fas fa-bars', 3, 1, 0, 1, '2019-09-07 21:43:46', 1, '2021-01-28 00:56:21', NULL, NULL),
(5, 'c', 4, 'Manage Menu Access', 'manage-menu-access', 'auth/manageMenuAccess', 'far fa-circle', 2, 1, 0, 1, '2019-09-07 21:45:05', 1, '2021-02-06 14:23:33', NULL, NULL),
(6, 'c', 2, 'Manage Users', 'manage-users', 'auth/manageUser', 'far fa-circle', 2, 1, 0, 1, '2019-09-24 03:44:10', 1, '2021-02-06 14:23:46', NULL, NULL),
(7, 'c', 4, 'Manage Menu', 'manage-menu', 'auth/manageMenu', 'far fa-circle', 1, 1, 0, 1, '2019-09-30 04:20:58', 1, '2021-02-06 16:46:16', NULL, NULL),
(12, 'p', NULL, 'Product Category', 'product-category', '#', 'fas fa-shopping-bag', 6, 1, 0, 1, '2021-02-26 09:12:46', 1, '2021-03-20 11:07:48', NULL, NULL),
(13, 'c', 12, 'Product Category One', 'product-category-one', 'inventory/productCategoryOne', 'far fa-circle', 1, 1, 0, 1, '2021-02-26 09:16:22', 1, '2021-02-28 07:43:25', NULL, NULL),
(14, 'c', 12, 'Product Category Two', 'product-category-two', 'inventory/productCategoryTwo', 'far fa-circle', 2, 1, 0, 1, '2021-02-26 09:18:50', 1, '2021-02-28 07:42:57', NULL, NULL),
(15, 'c', 12, 'Product Category Three', 'product-category-three', 'inventory/productCategoryThree', 'far fa-circle', 3, 1, 0, 1, '2021-02-26 09:19:19', 1, '2021-02-28 11:56:18', NULL, NULL),
(16, 's', NULL, 'Products', 'products', 'inventory/product', 'fas fa-barcode', 7, 1, 0, 1, '2021-02-26 09:19:57', 1, '2021-03-20 11:07:59', NULL, NULL),
(17, 's', NULL, 'Company Detail', 'company-detail', 'inventory/companyDetail', 'fas fa-building', 999, 1, 0, 1, '2021-02-26 09:20:45', 1, '2021-03-01 08:18:12', NULL, NULL),
(18, 's', NULL, 'Suppliers', 'suppliers', 'inventory/supplier', 'fas fa-truck', 4, 1, 0, 1, '2021-03-01 12:09:21', 1, '2021-03-20 11:07:20', NULL, NULL),
(19, 'p', NULL, 'Attendance', 'attendance', '#', 'fas fa-clipboard', 10, 1, 0, 1, '2021-03-08 08:13:22', 1, '2021-03-20 11:04:58', NULL, NULL),
(20, 'c', 19, 'Daily Attendance', 'daily-attendance', 'hr/dailyAttendance', 'far fa-circle', 1, 1, 0, 1, '2021-03-08 08:15:39', 1, '2021-03-08 09:01:36', NULL, NULL),
(21, 'c', 19, 'Attendance Report', 'attendance-report', 'hr/attendanceReport', 'far fa-circle', 2, 1, 0, 1, '2021-03-08 08:16:05', 1, '2021-03-09 11:11:42', NULL, NULL),
(23, 's', NULL, 'User Bank Detail', 'user-bank-detail', 'hr/userBankDetail', 'fas fa-rupee-sign', 9, 1, 0, 1, '2021-03-12 07:54:35', 1, '2021-03-20 11:05:42', NULL, NULL),
(25, 'c', 31, 'Wholesale Customers', 'wholesale-customers', 'customer/wholesaleCustomer', 'far fa-circle', 1, 1, 0, 1, '2021-03-16 12:10:59', 1, '2021-04-04 14:21:01', NULL, NULL),
(26, 'c', 33, 'Wholesale Orders', 'wholesale-orders', 'inventory/wholesaleOrders', 'far fa-circle', 1, 1, 0, 1, '2021-03-18 06:16:00', 1, '2021-04-07 16:47:58', NULL, NULL),
(31, 'p', NULL, 'Customers', 'customers', '#', 'fas fa-user-friends', 5, 1, 0, 1, '2021-03-31 09:19:14', NULL, NULL, NULL, NULL),
(32, 'c', 31, 'Retail Customer', 'retail-customer', 'customer/retailCustomer', 'far fa-circle', 2, 1, 0, 1, '2021-03-31 09:20:37', 1, '2021-04-07 16:55:30', NULL, NULL),
(33, 'p', NULL, 'Orders', 'orders', '#', 'fas fa-shopping-cart', 8, 1, 0, 1, '2021-04-07 16:47:35', NULL, NULL, NULL, NULL),
(34, 'c', 33, 'Retail Orders', 'retail-orders', 'inventory/retailOrders', 'far fa-circle', 2, 1, 0, 1, '2021-04-07 17:23:28', 1, '2021-04-08 05:33:55', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_menu_access`
--

CREATE TABLE `dashboard_menu_access` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `view_access` tinyint(4) DEFAULT NULL,
  `add_access` tinyint(4) DEFAULT NULL,
  `edit_access` tinyint(4) DEFAULT NULL,
  `delete_access` tinyint(4) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dashboard_menu_access`
--

INSERT INTO `dashboard_menu_access` (`id`, `group_id`, `menu_id`, `view_access`, `add_access`, `edit_access`, `delete_access`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(37, 8, 19, 0, 0, 0, 0, 1, '2021-03-17 11:19:38', 1, '2021-03-31 08:19:02'),
(38, 8, 21, 0, 0, 0, 0, 1, '2021-03-17 11:19:38', 1, '2021-03-31 08:19:02'),
(39, 8, 17, 0, 0, 0, 0, 1, '2021-03-17 11:19:38', 1, '2021-03-31 08:19:02'),
(40, 8, 25, 0, 0, 0, 0, 1, '2021-03-17 11:19:38', 1, '2021-03-31 08:19:02'),
(41, 8, 20, 0, 0, 0, 0, 1, '2021-03-17 11:19:39', 1, '2021-03-31 08:19:02'),
(42, 8, 1, 0, 0, 0, 0, 1, '2021-03-17 11:19:39', 1, '2021-03-31 08:19:02'),
(43, 8, 4, 0, 0, 0, 0, 1, '2021-03-17 11:19:39', 1, '2021-03-31 08:19:02'),
(44, 8, 7, 0, 0, 0, 0, 1, '2021-03-17 11:19:39', 1, '2021-03-31 08:19:02'),
(45, 8, 5, 0, 0, 0, 0, 1, '2021-03-17 11:19:39', 1, '2021-03-31 08:19:02'),
(46, 8, 3, 0, 0, 0, 0, 1, '2021-03-17 11:19:39', 1, '2021-03-31 08:19:02'),
(47, 8, 6, 0, 0, 0, 0, 1, '2021-03-17 11:19:39', 1, '2021-03-31 08:19:03'),
(48, 8, 12, 0, 0, 0, 0, 1, '2021-03-17 11:19:39', 1, '2021-03-31 08:19:03'),
(49, 8, 13, 0, 0, 0, 0, 1, '2021-03-17 11:19:39', 1, '2021-03-31 08:19:03'),
(50, 8, 15, 0, 0, 0, 0, 1, '2021-03-17 11:19:39', 1, '2021-03-31 08:19:03'),
(51, 8, 14, 0, 0, 0, 0, 1, '2021-03-17 11:19:39', 1, '2021-03-31 08:19:03'),
(52, 8, 16, 0, 0, 0, 0, 1, '2021-03-17 11:19:39', 1, '2021-03-31 08:19:03'),
(53, 8, 18, 0, 0, 0, 0, 1, '2021-03-17 11:19:39', 1, '2021-03-31 08:19:03'),
(54, 8, 23, 0, 0, 0, 0, 1, '2021-03-17 11:19:39', 1, '2021-03-31 08:19:03'),
(55, 8, 2, 0, 0, 0, 0, 1, '2021-03-17 11:19:39', 1, '2021-03-31 08:19:03'),
(57, 9, 19, 1, 0, 0, 0, 1, '2021-03-30 16:51:27', 1, '2021-03-30 16:52:30'),
(58, 9, 21, 0, 0, 0, 0, 1, '2021-03-30 16:51:27', 1, '2021-03-30 16:52:30'),
(59, 9, 17, 0, 0, 0, 0, 1, '2021-03-30 16:51:27', 1, '2021-03-30 16:52:30'),
(60, 9, 25, 0, 0, 0, 0, 1, '2021-03-30 16:51:27', 1, '2021-03-30 16:52:30'),
(61, 9, 20, 1, 1, 1, 1, 1, '2021-03-30 16:51:27', 1, '2021-03-30 16:52:30'),
(62, 9, 1, 0, 0, 0, 0, 1, '2021-03-30 16:51:27', 1, '2021-03-30 16:52:30'),
(63, 9, 4, 0, 0, 0, 0, 1, '2021-03-30 16:51:27', 1, '2021-03-30 16:52:30'),
(64, 9, 7, 0, 0, 0, 0, 1, '2021-03-30 16:51:27', 1, '2021-03-30 16:52:30'),
(65, 9, 5, 0, 0, 0, 0, 1, '2021-03-30 16:51:27', 1, '2021-03-30 16:52:31'),
(67, 9, 3, 0, 0, 0, 0, 1, '2021-03-30 16:51:27', 1, '2021-03-30 16:52:31'),
(68, 9, 6, 0, 0, 0, 0, 1, '2021-03-30 16:51:27', 1, '2021-03-30 16:52:31'),
(69, 9, 26, 0, 0, 0, 0, 1, '2021-03-30 16:51:27', 1, '2021-03-30 16:52:31'),
(70, 9, 12, 0, 0, 0, 0, 1, '2021-03-30 16:51:27', 1, '2021-03-30 16:52:31'),
(71, 9, 13, 0, 0, 0, 0, 1, '2021-03-30 16:51:28', 1, '2021-03-30 16:52:31'),
(72, 9, 15, 0, 0, 0, 0, 1, '2021-03-30 16:51:28', 1, '2021-03-30 16:52:31'),
(73, 9, 14, 0, 0, 0, 0, 1, '2021-03-30 16:51:28', 1, '2021-03-30 16:52:31'),
(74, 9, 16, 0, 0, 0, 0, 1, '2021-03-30 16:51:28', 1, '2021-03-30 16:52:31'),
(75, 9, 18, 0, 0, 0, 0, 1, '2021-03-30 16:51:28', 1, '2021-03-30 16:52:31'),
(76, 9, 23, 0, 0, 0, 0, 1, '2021-03-30 16:51:28', 1, '2021-03-30 16:52:31'),
(77, 9, 2, 0, 0, 0, 0, 1, '2021-03-30 16:51:28', 1, '2021-03-30 16:52:31'),
(80, 8, 26, 0, 0, 0, 0, 1, '2021-03-31 08:18:37', 1, '2021-03-31 08:19:03'),
(84, 10, 19, 1, 0, 0, 0, 1, '2021-03-31 08:20:09', 1, '2021-03-31 08:59:12'),
(85, 10, 21, 1, 0, 0, 0, 1, '2021-03-31 08:20:09', 1, '2021-03-31 08:59:12'),
(86, 10, 17, 0, 0, 0, 0, 1, '2021-03-31 08:20:10', 1, '2021-03-31 08:59:13'),
(87, 10, 25, 1, 1, 1, 1, 1, '2021-03-31 08:20:10', 1, '2021-03-31 08:59:13'),
(88, 10, 20, 1, 1, 1, 0, 1, '2021-03-31 08:20:10', 1, '2021-03-31 08:59:13'),
(89, 10, 1, 0, 0, 0, 0, 1, '2021-03-31 08:20:10', 1, '2021-03-31 08:59:13'),
(90, 10, 4, 0, 0, 0, 0, 1, '2021-03-31 08:20:10', 1, '2021-03-31 08:59:13'),
(91, 10, 7, 0, 0, 0, 0, 1, '2021-03-31 08:20:10', 1, '2021-03-31 08:59:13'),
(92, 10, 5, 0, 0, 0, 0, 1, '2021-03-31 08:20:10', 1, '2021-03-31 08:59:13'),
(94, 10, 3, 0, 0, 0, 0, 1, '2021-03-31 08:20:10', 1, '2021-03-31 08:59:13'),
(95, 10, 6, 0, 0, 0, 0, 1, '2021-03-31 08:20:10', 1, '2021-03-31 08:59:13'),
(96, 10, 26, 0, 0, 0, 0, 1, '2021-03-31 08:20:10', 1, '2021-03-31 08:59:13'),
(97, 10, 12, 0, 0, 0, 0, 1, '2021-03-31 08:20:10', 1, '2021-03-31 08:59:13'),
(98, 10, 13, 0, 0, 0, 0, 1, '2021-03-31 08:20:10', 1, '2021-03-31 08:59:13'),
(99, 10, 15, 0, 0, 0, 0, 1, '2021-03-31 08:20:10', 1, '2021-03-31 08:59:13'),
(100, 10, 14, 0, 0, 0, 0, 1, '2021-03-31 08:20:10', 1, '2021-03-31 08:59:13'),
(101, 10, 16, 0, 0, 0, 0, 1, '2021-03-31 08:20:10', 1, '2021-03-31 08:59:13'),
(102, 10, 18, 0, 0, 0, 0, 1, '2021-03-31 08:20:11', 1, '2021-03-31 08:59:13'),
(105, 10, 23, 0, 0, 0, 0, 1, '2021-03-31 08:20:11', 1, '2021-03-31 08:59:14'),
(106, 10, 2, 0, 0, 0, 0, 1, '2021-03-31 08:20:11', 1, '2021-03-31 08:59:14');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `product_category_id` int(11) NOT NULL,
  `sku` varchar(200) NOT NULL,
  `product_name` varchar(200) DEFAULT NULL,
  `product_image` varchar(255) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL,
  `color` varchar(100) DEFAULT NULL,
  `unit_price` float(10,2) NOT NULL,
  `stock_quantity` int(11) DEFAULT 0,
  `packaging_type` varchar(200) DEFAULT NULL,
  `arrival_date` date NOT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `entry_medium` varchar(10) NOT NULL,
  `delete_status` int(2) DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `supplier_id`, `product_category_id`, `sku`, `product_name`, `product_image`, `size`, `color`, `unit_price`, `stock_quantity`, `packaging_type`, `arrival_date`, `barcode`, `entry_medium`, `delete_status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES
(11, 4, 6, 'test', NULL, 'http://img.ltwebstatic.com/images2_pi/2018/07/04/15306940234071242929.jpg', 'test', 'test', 625.00, 10, 'test', '2021-03-31', NULL, 'excel', 0, 1, '2021-03-31 08:33:43', NULL, NULL, NULL, NULL),
(12, 4, 6, 'test1', '', 'http://img.ltwebstatic.com/images2_pi/2018/07/04/15306940234071242929.jpg', 'test4', 'test5', 554.00, 50, 'test9', '2021-03-31', NULL, '', 0, 1, '2021-03-31 08:33:43', 1, '2021-03-31 08:37:16', NULL, NULL),
(13, 4, 6, 'test2', NULL, 'http://img.ltwebstatic.com/images2_pi/2018/07/04/15306940234071242929.jpg', 'test5', 'test6', 878.00, 17, 'test10', '2021-03-31', NULL, 'excel', 0, 1, '2021-03-31 08:33:43', NULL, NULL, NULL, NULL),
(14, 4, 6, 'test3', NULL, 'http://img.ltwebstatic.com/images2_pi/2018/07/04/15306940234071242929.jpg', 'test6', 'test7', 23.00, 3, 'test11', '2021-03-31', NULL, 'excel', 0, 1, '2021-03-31 08:33:43', NULL, NULL, NULL, NULL),
(15, 4, 6, 'test', 'test42', NULL, 'test', 'test', 120.00, 3, 'test', '2021-03-31', NULL, 'excel', 0, 1, '2021-03-31 08:33:43', NULL, NULL, NULL, NULL),
(16, 4, 6, 'test13', 'test43', NULL, 'test16', 'test17', 1200.00, 3, 'test21', '2021-03-31', NULL, 'excel', 0, 1, '2021-03-31 08:33:43', NULL, NULL, NULL, NULL),
(17, 4, 6, 'test14', 'test44', NULL, 'test17', 'test18', 1500.00, 0, 'test22', '2021-03-31', NULL, 'excel', 0, 1, '2021-03-31 08:33:43', NULL, NULL, NULL, NULL),
(18, 4, 6, 'test15', 'test45', NULL, 'test18', 'test19', 2000.00, 0, 'test23', '2021-03-31', NULL, '', 0, 1, '2021-03-31 08:33:43', 1, '2021-04-07 11:47:00', NULL, NULL),
(19, 4, 6, 'test16', 'test46', NULL, 'test19', 'test20', 750.00, 0, 'test24', '2021-03-31', NULL, 'excel', 0, 1, '2021-03-31 08:33:43', NULL, NULL, NULL, NULL),
(20, 4, 6, 'weqrwqe', 'jordans', './assets/uploads/images/products/computer_table3.jpg', '42', 'red', 235.00, 3, 'box', '2021-03-26', NULL, 'manual', 0, 1, '2021-03-31 08:38:57', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_category_one`
--

CREATE TABLE `product_category_one` (
  `id` int(11) NOT NULL,
  `category_name` varchar(150) NOT NULL,
  `delete_status` int(2) DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_category_one`
--

INSERT INTO `product_category_one` (`id`, `category_name`, `delete_status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES
(3, 'bag', 0, 1, '2021-03-31 08:26:49', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_category_three`
--

CREATE TABLE `product_category_three` (
  `id` int(11) NOT NULL,
  `prod_cat_two_id` int(11) NOT NULL,
  `category_name` varchar(150) NOT NULL,
  `reorder_level` int(5) DEFAULT NULL,
  `max_stock_level` int(5) DEFAULT NULL,
  `delete_status` int(2) DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_category_three`
--

INSERT INTO `product_category_three` (`id`, `prod_cat_two_id`, `category_name`, `reorder_level`, `max_stock_level`, `delete_status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES
(6, 5, 'laptop bag', 10, 5, 0, 1, '2021-03-31 08:30:05', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_category_two`
--

CREATE TABLE `product_category_two` (
  `id` int(11) NOT NULL,
  `prod_cat_one_id` int(11) NOT NULL,
  `category_name` varchar(150) NOT NULL,
  `box_no` varchar(200) DEFAULT NULL,
  `delete_status` int(2) DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_category_two`
--

INSERT INTO `product_category_two` (`id`, `prod_cat_one_id`, `category_name`, `box_no`, `delete_status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES
(5, 3, 'side bag', '23', 0, 1, '2021-03-31 08:28:06', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `retail_customer`
--

CREATE TABLE `retail_customer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact` varchar(20) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `delete_status` int(2) DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `retail_customer`
--

INSERT INTO `retail_customer` (`id`, `name`, `address`, `contact`, `email`, `delete_status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES
(1, 'Rita raiiiii', 'sanepaaa', '9841454545', 'rita@gmail.com', 0, 1, '2021-04-07 17:05:41', 1, '2021-04-10 10:42:14', 1, '2021-04-07 17:09:36'),
(2, 'Hari bahadur', 'lazimpat', '9841787878', 'hari@gmail.com', 0, 1, '2021-04-07 17:06:58', 1, '2021-04-08 03:29:51', NULL, NULL),
(3, 'Luke Thomas', 'Baluwatar - 4, Nayabasti Marg', '9841739097', 'luke@yahoo.com', 0, 1, '2021-04-12 10:42:20', NULL, NULL, 1, '2021-04-12 10:42:33');

-- --------------------------------------------------------

--
-- Table structure for table `retail_order`
--

CREATE TABLE `retail_order` (
  `id` int(11) NOT NULL,
  `order_no` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_datetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `gross_amount` float(10,2) NOT NULL,
  `service_charge_rate` varchar(2) DEFAULT '0',
  `service_charge` float(10,2) DEFAULT NULL,
  `vat_charge_rate` varchar(2) DEFAULT '0',
  `vat_charge` float(10,2) DEFAULT NULL,
  `delivery_charge` varchar(10) DEFAULT NULL,
  `discount_rate` varchar(2) DEFAULT NULL,
  `discount` varchar(10) DEFAULT NULL,
  `net_amount` float(10,2) NOT NULL,
  `special_instruction` text DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `dispatched_date` date DEFAULT NULL,
  `order_status` int(2) DEFAULT 0,
  `remarks` text DEFAULT NULL,
  `delete_status` int(2) DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `retail_order`
--

INSERT INTO `retail_order` (`id`, `order_no`, `customer_id`, `order_datetime`, `gross_amount`, `service_charge_rate`, `service_charge`, `vat_charge_rate`, `vat_charge`, `delivery_charge`, `discount_rate`, `discount`, `net_amount`, `special_instruction`, `delivery_date`, `dispatched_date`, `order_status`, `remarks`, `delete_status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES
(1, 1, 2, '2021-04-09 14:32:00', 18400.00, NULL, 0.00, '13', 2392.00, NULL, '15', '2760.00', 18032.00, 'Nothing.\r\n', '2021-04-13', '0000-00-00', 0, NULL, 0, 1, '2021-04-09 00:14:43', 1, '2021-04-09 00:59:56', NULL, NULL),
(2, 2, 1, '2021-04-09 01:07:39', 7986.00, NULL, 0.00, '13', 1038.18, NULL, '10', '798.60', 8225.58, 'Iure illo sunt harum', '2021-04-03', NULL, 0, NULL, 0, 1, '2021-04-09 01:04:26', NULL, NULL, 1, '2021-04-09 01:07:24');

-- --------------------------------------------------------

--
-- Table structure for table `retail_order_item`
--

CREATE TABLE `retail_order_item` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(10) NOT NULL,
  `rate` float(10,2) NOT NULL,
  `total_amount` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `retail_order_item`
--

INSERT INTO `retail_order_item` (`id`, `order_id`, `product_id`, `quantity`, `rate`, `total_amount`) VALUES
(10, 1, 16, 2, 1200.00, 2400.00),
(11, 1, 17, 4, 1500.00, 6000.00),
(12, 1, 18, 5, 2000.00, 10000.00),
(13, 2, 12, 9, 554.00, 4986.00),
(14, 2, 17, 2, 1500.00, 3000.00);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `pan_vat` varchar(200) DEFAULT NULL,
  `contact` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `delete_status` int(2) DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `name`, `address`, `pan_vat`, `contact`, `email`, `delete_status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES
(4, 'Cecilia Vazquez', 'China', '23425435436', '875675656', 'gddg@gmail.com', 0, 1, '2021-03-31 08:23:36', 1, '2021-04-12 10:41:12', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_bank_detail`
--

CREATE TABLE `user_bank_detail` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `delete_status` int(2) DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_bank_detail`
--

INSERT INTO `user_bank_detail` (`id`, `user_id`, `bank_name`, `account_name`, `account_number`, `delete_status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES
(3, 15, 'prime bank', 'Savon', '23452345453253245', 0, 1, '2021-03-31 09:12:56', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_detail`
--

CREATE TABLE `user_detail` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `gender` varchar(2) NOT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `pan_no` varchar(100) DEFAULT NULL,
  `blood_group` varchar(10) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  `joined_date` date DEFAULT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_detail`
--

INSERT INTO `user_detail` (`id`, `user_id`, `firstname`, `lastname`, `dob`, `address`, `gender`, `contact`, `pan_no`, `blood_group`, `position`, `joined_date`, `profile_image`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 'Admin', 'istrator', NULL, '', 'm', '', NULL, NULL, NULL, NULL, NULL, 1, '2019-07-24 04:14:35', 1, '2021-03-16 06:57:34'),
(12, 14, 'Ujjwal', 'Shrestha', '1996-04-17', 'Baluwatar - 4, Nayabasti Marg, Kathmandu', 'm', '+977 9841739097', '1234567890ab', 'O+ve', NULL, '2021-03-29', NULL, 1, '2021-03-30 16:34:58', 1, '2021-03-30 16:48:35'),
(13, 15, 'Savson', 'Maharjan', '1995-12-27', 'Baluwatar - 4, Nayabasti Marg', 'm', '9841739097', '1234567890', 'O+ve', NULL, '2021-03-31', NULL, 1, '2021-03-31 08:12:32', 1, '2021-03-31 08:13:21');

-- --------------------------------------------------------

--
-- Table structure for table `wholesale_customer`
--

CREATE TABLE `wholesale_customer` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `pan_vat` varchar(255) DEFAULT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wholesale_customer`
--

INSERT INTO `wholesale_customer` (`id`, `user_id`, `name`, `address`, `pan_vat`, `contact`, `profile_image`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(2, 16, 'Nike', 'Baluwatar - 4, Nayabasti Marg', '34254325234523', '9841739097', NULL, 1, '2021-03-31 08:17:52', NULL, NULL),
(3, 18, 'adidas', 'Baluwatar - 4, Nayabasti Marg', '34254325234523', '9841739097', NULL, 1, '2021-04-05 14:29:16', 1, '2021-04-06 00:54:19');

-- --------------------------------------------------------

--
-- Table structure for table `wholesale_order`
--

CREATE TABLE `wholesale_order` (
  `id` int(11) NOT NULL,
  `order_no` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_datetime` timestamp NULL DEFAULT NULL,
  `gross_amount` float(10,2) NOT NULL,
  `service_charge_rate` varchar(2) DEFAULT '0',
  `service_charge` float(10,2) DEFAULT NULL,
  `vat_charge_rate` varchar(2) DEFAULT '0',
  `vat_charge` float(10,2) DEFAULT NULL,
  `delivery_charge` varchar(10) DEFAULT NULL,
  `discount_rate` varchar(5) DEFAULT NULL,
  `discount` varchar(10) DEFAULT NULL,
  `net_amount` float(10,2) NOT NULL,
  `special_instruction` text DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `dispatched_date` date DEFAULT NULL,
  `order_status` int(2) DEFAULT 0,
  `remarks` text DEFAULT NULL,
  `delete_status` int(2) DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wholesale_order`
--

INSERT INTO `wholesale_order` (`id`, `order_no`, `customer_id`, `order_datetime`, `gross_amount`, `service_charge_rate`, `service_charge`, `vat_charge_rate`, `vat_charge`, `delivery_charge`, `discount_rate`, `discount`, `net_amount`, `special_instruction`, `delivery_date`, `dispatched_date`, `order_status`, `remarks`, `delete_status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES
(9, 1, 16, '2021-06-04 20:20:00', 29680.00, NULL, 0.00, '13', 3858.40, NULL, '', '0.00', 33538.40, 'Quaerat quos aut sit', '2012-06-21', '0000-00-00', 0, NULL, 0, 1, '2021-04-07 11:53:10', 1, '2021-04-07 13:52:30', NULL, NULL),
(10, 2, 18, '2021-04-07 15:51:00', 554.00, NULL, 0.00, '13', 72.02, NULL, '15', '83.10', 542.92, 'Dolorem doloremque i', '2021-04-08', '0000-00-00', 0, 'qwerty', 0, 1, '2021-04-07 14:06:10', 1, '2021-04-19 08:58:32', 1, '2021-04-07 14:09:49'),
(12, 3, 16, '2021-04-07 17:40:00', 13941.00, NULL, 0.00, '13', 1812.33, NULL, '14', '1951.74', 13801.59, 'Vel do non magnam sa', '2021-04-10', '2021-04-09', 0, NULL, 0, 1, '2021-04-07 14:55:16', 1, '2021-04-07 14:59:58', NULL, NULL),
(13, 4, 18, '2021-04-12 10:43:40', 50750.00, NULL, 0.00, '13', 6597.50, NULL, '20', '10150.00', 47197.50, '', '2021-04-16', NULL, 0, NULL, 0, 1, '2021-04-12 10:48:03', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wholesale_order_item`
--

CREATE TABLE `wholesale_order_item` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(10) NOT NULL,
  `rate` float(10,2) NOT NULL,
  `total_amount` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wholesale_order_item`
--

INSERT INTO `wholesale_order_item` (`id`, `order_id`, `product_id`, `quantity`, `rate`, `total_amount`) VALUES
(51, 9, 16, 3, 1200.00, 3600.00),
(52, 9, 17, 10, 1500.00, 15000.00),
(53, 9, 12, 20, 554.00, 11080.00),
(68, 12, 14, 1, 23.00, 23.00),
(69, 12, 17, 5, 1500.00, 7500.00),
(70, 12, 12, 10, 554.00, 5540.00),
(71, 12, 13, 1, 878.00, 878.00),
(72, 13, 19, 5, 750.00, 3750.00),
(73, 13, 18, 10, 2000.00, 20000.00),
(74, 13, 17, 18, 1500.00, 27000.00),
(77, 10, 12, 1, 554.00, 554.00);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `attendance_status`
--
ALTER TABLE `attendance_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_user_group_mapping`
--
ALTER TABLE `auth_user_group_mapping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `company_detail`
--
ALTER TABLE `company_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dashboard_menu`
--
ALTER TABLE `dashboard_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_menu_id` (`parent_menu_id`);

--
-- Indexes for table `dashboard_menu_access`
--
ALTER TABLE `dashboard_menu_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `product_category_id` (`product_category_id`);

--
-- Indexes for table `product_category_one`
--
ALTER TABLE `product_category_one`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_category_three`
--
ALTER TABLE `product_category_three`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prod_cat_two_id` (`prod_cat_two_id`);

--
-- Indexes for table `product_category_two`
--
ALTER TABLE `product_category_two`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prod_cat_one_id` (`prod_cat_one_id`);

--
-- Indexes for table `retail_customer`
--
ALTER TABLE `retail_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `retail_order`
--
ALTER TABLE `retail_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `retail_order_item`
--
ALTER TABLE `retail_order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_bank_detail`
--
ALTER TABLE `user_bank_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wholesale_customer`
--
ALTER TABLE `wholesale_customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wholesale_order`
--
ALTER TABLE `wholesale_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `wholesale_order_item`
--
ALTER TABLE `wholesale_order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `attendance_status`
--
ALTER TABLE `attendance_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `auth_user_group_mapping`
--
ALTER TABLE `auth_user_group_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `company_detail`
--
ALTER TABLE `company_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dashboard_menu`
--
ALTER TABLE `dashboard_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `dashboard_menu_access`
--
ALTER TABLE `dashboard_menu_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `product_category_one`
--
ALTER TABLE `product_category_one`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_category_three`
--
ALTER TABLE `product_category_three`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product_category_two`
--
ALTER TABLE `product_category_two`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `retail_customer`
--
ALTER TABLE `retail_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `retail_order`
--
ALTER TABLE `retail_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `retail_order_item`
--
ALTER TABLE `retail_order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_bank_detail`
--
ALTER TABLE `user_bank_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_detail`
--
ALTER TABLE `user_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `wholesale_customer`
--
ALTER TABLE `wholesale_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wholesale_order`
--
ALTER TABLE `wholesale_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `wholesale_order_item`
--
ALTER TABLE `wholesale_order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `attendance`
--
ALTER TABLE `attendance`
  ADD CONSTRAINT `attendance_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `attendance_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_user_group_mapping`
--
ALTER TABLE `auth_user_group_mapping`
  ADD CONSTRAINT `auth_user_group_mapping_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_user_group_mapping_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `dashboard_menu`
--
ALTER TABLE `dashboard_menu`
  ADD CONSTRAINT `dashboard_menu_ibfk_1` FOREIGN KEY (`parent_menu_id`) REFERENCES `dashboard_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `dashboard_menu_access`
--
ALTER TABLE `dashboard_menu_access`
  ADD CONSTRAINT `dashboard_menu_access_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `dashboard_menu_access_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `dashboard_menu` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`product_category_id`) REFERENCES `product_category_three` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_category_three`
--
ALTER TABLE `product_category_three`
  ADD CONSTRAINT `product_category_three_ibfk_1` FOREIGN KEY (`prod_cat_two_id`) REFERENCES `product_category_two` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_category_two`
--
ALTER TABLE `product_category_two`
  ADD CONSTRAINT `product_category_two_ibfk_1` FOREIGN KEY (`prod_cat_one_id`) REFERENCES `product_category_one` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `retail_order`
--
ALTER TABLE `retail_order`
  ADD CONSTRAINT `retail_order_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `retail_customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `retail_order_item`
--
ALTER TABLE `retail_order_item`
  ADD CONSTRAINT `retail_order_item_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `retail_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `retail_order_item_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_bank_detail`
--
ALTER TABLE `user_bank_detail`
  ADD CONSTRAINT `user_bank_detail_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD CONSTRAINT `user_detail_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wholesale_customer`
--
ALTER TABLE `wholesale_customer`
  ADD CONSTRAINT `wholesale_customer_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wholesale_order`
--
ALTER TABLE `wholesale_order`
  ADD CONSTRAINT `wholesale_order_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `wholesale_customer` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wholesale_order_item`
--
ALTER TABLE `wholesale_order_item`
  ADD CONSTRAINT `wholesale_order_item_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `wholesale_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wholesale_order_item_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
