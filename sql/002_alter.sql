-- tbl_name
-- Description
-- By Author_name (Date)


-- tbl_users_main
-- To add a column named LAST_LOGIN after ACTIVE_STATUS
-- By Ujjwal Shrestha (2019-07-27)
ALTER TABLE tbl_users_main ADD LAST_LOGIN TIMESTAMP DEFAULT NULL AFTER ACTIVE_STATUS;


-- tbl_user_groups
-- To add a column named LOGIN_STATUS, STATUS after DESCRIPTION, LOGIN_STATUS
-- By Ujjwal Shrestha (2019-09-29)
ALTER TABLE tbl_user_groups ADD LOGIN_STATUS INT(2) DEFAULT 1 AFTER DESCRIPTION;
ALTER TABLE tbl_user_groups ADD STATUS INT(2) DEFAULT 1 AFTER LOGIN_STATUS;


-- tbl_users_main
-- To add a column named IS_SUPER_ADMIN after LAST_LOGIN
-- By Ujjwal Shrestha (2019-09-29)
ALTER TABLE tbl_users_main ADD IS_SUPER_ADMIN INT(2) DEFAULT 0 AFTER LAST_LOGIN;


-- tbl_users_detail
-- To add a column named GENDER after DOB
-- By Ujjwal Shrestha (2019-09-29)
ALTER TABLE tbl_users_detail ADD GENDER VARCHAR(2) DEFAULT NOT NULL AFTER DOB;


-- tbl_user_group_mapping
-- To add a column named GENDER after DOB
-- By Ujjwal Shrestha (2019-09-29)
ALTER TABLE tbl_user_group_mapping ADD CREATED_BY INT NOT NULL AFTER GROUP_ID;
ALTER TABLE tbl_user_group_mapping ADD CREATED_AT TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL AFTER CREATED_BY;
ALTER TABLE tbl_user_group_mapping ADD UPDATED_BY INT NULL AFTER CREATED_AT;
ALTER TABLE tbl_user_group_mapping ADD UPDATED_AT TIMESTAMP NULL AFTER UPDATED_BY;


ALTER TABLE user_subscription ADD hash_code varchar(255) NOT NULL AFTER email;

ALTER TABLE user_detail ADD contact varchar(20) DEFAULT NULL AFTER gender;

ALTER TABLE auth_group ADD profile_update_access int(2) DEFAULT 1 AFTER description;

ALTER TABLE auth_group ADD delete_status int(2) DEFAULT 0 AFTER status;
ALTER TABLE auth_group ADD created_by int NOT NULL AFTER delete_status;
ALTER TABLE auth_group ADD created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL AFTER created_by;
ALTER TABLE auth_group ADD updated_by int DEFAULT NULL AFTER created_at;
ALTER TABLE auth_group ADD updated_at timestamp NULL DEFAULT NULL AFTER updated_by;
ALTER TABLE auth_group ADD deleted_by int DEFAULT NULL AFTER updated_at;
ALTER TABLE auth_group ADD deleted_at timestamp NULL DEFAULT NULL AFTER deleted_by;

ALTER TABLE auth_user ADD delete_status int(2) DEFAULT 0 AFTER is_super_admin;
ALTER TABLE auth_user ADD deleted_by int DEFAULT NULL AFTER updated_at;
ALTER TABLE auth_user ADD deleted_at timestamp NULL DEFAULT NULL AFTER deleted_by;

ALTER TABLE dashboard_menu ADD delete_status int(2) DEFAULT 0 AFTER status;
ALTER TABLE dashboard_menu ADD deleted_by int DEFAULT NULL AFTER updated_at;
ALTER TABLE dashboard_menu ADD deleted_at timestamp NULL DEFAULT NULL AFTER deleted_by;

ALTER TABLE user_detail ADD pan_no varchar(100) DEFAULT NULL AFTER contact;
ALTER TABLE user_detail ADD blood_group varchar(10) DEFAULT NULL AFTER pan_no;
ALTER TABLE user_detail ADD position varchar(100) DEFAULT NULL AFTER blood_group;
ALTER TABLE user_detail ADD joined_date date NULL DEFAULT NULL AFTER position;


ALTER TABLE product ADD entry_medium varchar(15) NOT NULL AFTER barcode;


ALTER TABLE orders ADD delivery_charge varchar(10) DEFAULT NULL AFTER vat_charge;

ALTER TABLE attendance_status ADD time_limit time DEFAULT NULL AFTER bg_color_class;

ALTER TABLE orders ADD discount_rate varchar(5) DEFAULT NULL AFTER delivery_charge;
ALTER TABLE orders ADD order_no int(11) NOT NULL AFTER id;

